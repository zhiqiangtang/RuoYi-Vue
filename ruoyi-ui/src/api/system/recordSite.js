import request from '@/utils/request'

// 查询校车行驶站点记录列表
export function listRecordSite(query) {
  return request({
    url: '/system/recordSite/list',
    method: 'get',
    params: query
  })
}

// 查询校车行驶站点记录详细
export function getRecordSite(id) {
  return request({
    url: '/system/recordSite/' + id,
    method: 'get'
  })
}

// 新增校车行驶站点记录
export function addRecordSite(data) {
  return request({
    url: '/system/recordSite',
    method: 'post',
    data: data
  })
}

// 修改校车行驶站点记录
export function updateRecordSite(data) {
  return request({
    url: '/system/recordSite',
    method: 'put',
    data: data
  })
}

// 删除校车行驶站点记录
export function delRecordSite(id) {
  return request({
    url: '/system/recordSite/' + id,
    method: 'delete'
  })
}
