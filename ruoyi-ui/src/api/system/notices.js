import request from "@/utils/request";

// 查询【请填写功能名称】列表
export function listNotice(query) {
  return request({
    url: "/system/notices/list",
    method: "get",
    params: query,
  });
}

// 查询搜索
export function getNotice(id) {
  return request({
    url: "/system/notices/" + id,
    method: "get",
  });
}

// 新增公告
export function addNotice(data) {
  return request({
    url: "/system/notices",
    method: "get",
    params: data,
  });
}

// 修改功能
export function updateNotice(data) {
  return request({
    url: "/system/notices/edit",
    method: "get",
    params: data,
  });
}

// 删除功能
export function delNotice(id) {
  return request({
    url: "/system/notices/" + id,
    method: "delete",
  });
}

export function noticesRelease(id) {
  return request({
    url: "/system/notices/noticesRelease/" + id,
    method: "get",
  });
}
export function noticesOffShelf(id) {
  return request({
    url: "/system/notices/noticesOffShelf/" + id,
    method: "get",
  });
}
