import request from '@/utils/request'

// 查询校车列表
export function listBus(query) {
  return request({
    url: '/system/bus/list',
    method: 'get',
    params: query
  })
}

// 查询校车详细
export function getBus(id) {
  return request({
    url: '/system/bus/' + id,
    method: 'get'
  })
}

// 新增校车
export function addBus(data) {
  return request({
    url: '/system/bus',
    method: 'post',
    data: data
  })
}

// 修改校车
export function updateBus(data) {
  return request({
    url: '/system/bus',
    method: 'put',
    data: data
  })
}

// 删除校车
export function delBus(id) {
  return request({
    url: '/system/bus/' + id,
    method: 'delete'
  })
}

