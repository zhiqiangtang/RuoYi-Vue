import request from '@/utils/request'

// 查询校车行驶记录实时站点记录列表
export function listRealtime(query) {
  return request({
    url: '/system/realtime/list',
    method: 'get',
    params: query
  })
}

// 查询校车行驶记录实时站点记录详细
export function getRealtime(id) {
  return request({
    url: '/system/realtime/' + id,
    method: 'get'
  })
}

// 新增校车行驶记录实时站点记录
export function addRealtime(data) {
  return request({
    url: '/system/realtime',
    method: 'post',
    data: data
  })
}

// 修改校车行驶记录实时站点记录
export function updateRealtime(data) {
  return request({
    url: '/system/realtime',
    method: 'put',
    data: data
  })
}

// 删除校车行驶记录实时站点记录
export function delRealtime(id) {
  return request({
    url: '/system/realtime/' + id,
    method: 'delete'
  })
}
