import request from '@/utils/request'

// 查询乘车预约记录列表
export function listReservation(query) {
  return request({
    url: '/system/reservation/list',
    method: 'get',
    params: query
  })
}

// 查询乘车预约记录详细
export function getReservation(id) {
  return request({
    url: '/system/reservation/' + id,
    method: 'get'
  })
}

// 新增乘车预约记录
export function addReservation(data) {
  return request({
    url: '/system/reservation',
    method: 'post',
    data: data
  })
}

// 修改乘车预约记录
export function updateReservation(data) {
  return request({
    url: '/system/reservation',
    method: 'put',
    data: data
  })
}

// 删除乘车预约记录
export function delReservation(id) {
  return request({
    url: '/system/reservation/' + id,
    method: 'delete'
  })
}
