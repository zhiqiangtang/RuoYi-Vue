import request from "@/utils/request";

/**分页查询*/
export function selectSubsidyPage(paramPage){
  return request(
    {
      url:'/subsidy/selectPage',
      method:'get',
      params:paramPage
    }
  )
}

/**插入数据*或修改数据*/
export function addSubsidyData(subsidyData){
  return request(
    {
      url:'/subsidy/insertUpdateSubsidyData',
      method:'post',
      data:subsidyData
    }
  )
}
/**批量删除或多个删除*/

export function deleteBeachIds(ids){
  return request(
    {
      url:'/subsidy/deleteBeachIds/'+ids,
      method:'delete'
    }
  )
}
export function exportSubsidy(data){
  return request(
    {
      url:'/subsidy/export',
      method:'post',
      params:data,
    }
  )
}

/*验证人编号*/
export function checkBehaviourCodeRepeat(value){
  return request(
    {
      url:'/subsidy/checkBehaviourCodeRepeat/'+value,
      method:'get'
    }
  )
}
export function selectQuery(priam){
  return request(
    {
      url:'/subsidy/selectQuery',
      method:'get',
      params:priam

    }
  )

}

