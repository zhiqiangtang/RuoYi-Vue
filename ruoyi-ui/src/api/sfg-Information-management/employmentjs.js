import request from "@/utils/request";
/*分页查询*/
export function selectEmploymentPage(param){
  return request(
    {
      url:'/employment/slectEmploymentDataPage',
      method:'get',
      params:param
    }
  )
}
/*新增数据或修改数据*/
export function addEmploymentData(data){
  return request(
    {
      url:'/employment/insertOrUpdateEmploymentData',
      method:'post',
      data:data
    }

  )
}
/**人编号验证*/
export function checkBehaviourCodeRepeat(employmentPersonId){
  return request(
    {
      url:'/employment/checkBehaviourCodeRepeat/'+employmentPersonId,
      method:'get',
    }
  )
}
/**批量删除*/
export function deleteBeachIds(ids){
  return request(
    {
      url:'/employment/deleteBeachIds/'+ids,
      method:'delete',
    }
  )
}
