import request from "@/utils/request";

//教育管理

export function selectEducationPage(query){
  return request(
    {
    url: '/education/query',
    method: 'get',
    params:query
    }
  )
}

//新增数据或修改数据
export function updateOrInsertDate(date){
  return request(
    {
      url:'/education/insertOrUpdateData',
      method:'post',
      data:date
    }
  )
}

//批量或单个删除数据
export function deleteBeachIds(ids){
  return request(
    {
      url:'/education/deleteBeachIds/'+ids,
      method:'post'
    }
  )
}

