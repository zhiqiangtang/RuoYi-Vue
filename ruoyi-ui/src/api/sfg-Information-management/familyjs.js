import request from "@/utils/request";
import * as url from "url";

//分页查询
export function selectFamilyPage(query){
  return request(
    {
      url: '/family/selectFamilyData',
      method:'get',
      params:query
    }
)
}
//新增家庭基本数据
export function addFamilyData(data){
  return request(
    {
      url:'/family/insertFamilyData',
      method:'post',
      data:data,
    }
  )
}

/*批量删除*/
export function deleteBeachIds(infoId){
  return request(
    {
      url:'/family/deleteBeachIds/'+infoId,
      method:'delete'
    }
  )
}
//人编号验证
export function checkBehaviourCodeRepeat(value){
  return request(
    {
      url:'/family/checkBehaviourCodeRepeat/'+value,
      method:'post',
    }
  )
}

/*数据的导出*/
export function exportFamily(data){
  return request(
    {
      url:'/family/exportData',
      method:'post',
      params:data
    }
  )
}
