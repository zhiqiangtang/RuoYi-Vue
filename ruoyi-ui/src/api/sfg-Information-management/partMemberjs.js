import request from "@/utils/request";

export function partMemberData(partMemberParam){
  return request(
    {
      url:'/PartMember/selectPartMemberDataPage',
      method:'get',
      params:partMemberParam
    }
  )
}
//新增数据或修改数据
export function addPartMemberData(partMemberdata){
  return request(
    {
      url:'/PartMember/insertOrUpdataPartMemberData',
      method:'post',
      data:partMemberdata
    }
  )
}
//批量删除
export  function deleteBeachIds(ids){
  return request(
    {
      url:'/PartMember/deleteBeachPartMemberData/'+ids,
      method:'delete'
    }
  )
}

/*人编号的验证*/
export function checkBehaviourCodeRepeat(value){
  return request(
    {
      url:'/PartMember/checkBehaviourCodeRepeat/'+value,
      method:'post'
    }
  )
}

export function exportPartMember(pram){
  return request(
    {
      url:'PartMember/exportData',
      method:'post',
      params:pram
    }
  )
}
