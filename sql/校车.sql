



-- ----------------------------
-- 公告
-- ----------------------------
drop table if exists bus_notice;
create table bus_notice (
  id              bigint(20)      not null auto_increment    comment 'ID',
  title            varchar(200)     default ''                 comment '标题',
  content            longtext     default ''                 comment '内容',
  release_time          datetime                                   comment '发布时间',
  release_by            bigint(20)     not null                 comment '发布人',
  release_role            bigint(20)     default null                 comment '发布可见角色',
  
  status               char(1)         not null                   comment '状态（0正常 1停用）',
  del_flag             char(1)         default '0'                comment '删除标志（0代表存在 2代表删除）',
  create_by            varchar(64)     default ''                 comment '创建者',
  create_time          datetime                                   comment '创建时间',
  update_by            varchar(64)     default ''                 comment '更新者',
  update_time          datetime                                   comment '更新时间',
  primary key (id)
) engine=innodb auto_increment=100 comment = '公告';

-- ----------------------------
-- 公告阅读
-- ----------------------------
drop table if exists bus_notice_read;
create table bus_notice_read (
  id              bigint(20)      not null auto_increment    comment 'ID',
  notice_id            bigint(20)     not null                 comment '公告',
  user_id            bigint(20)     not null                 comment '接收人',
  is_read          datetime                                   comment '是否阅读',
  read_time          datetime                                   comment '阅读时间',
  
  status               char(1)         not null                   comment '状态（0正常 1停用）',
  del_flag             char(1)         default '0'                comment '删除标志（0代表存在 2代表删除）',
  create_by            varchar(64)     default ''                 comment '创建者',
  create_time          datetime                                   comment '创建时间',
  update_by            varchar(64)     default ''                 comment '更新者',
  update_time          datetime                                   comment '更新时间',
  primary key (id)
) engine=innodb auto_increment=100 comment = '公告阅读';

-- ----------------------------
-- 校车
-- ----------------------------
drop table if exists bus_bus;
create table bus_bus (
  id              bigint(20)      not null auto_increment    comment 'ID',
  bus_name            varchar(64)     default ''                 comment '校车名',
  plate            varchar(64)     default ''                 comment '车牌号',
  driver            bigint(20)     default null                 comment '司机',
  seating            int(3)     default 0                 comment '座位数',
  
  status               char(1)         not null                   comment '状态（0正常 1停用）',
  del_flag             char(1)         default '0'                comment '删除标志（0代表存在 2代表删除）',
  create_by            varchar(64)     default ''                 comment '创建者',
  create_time          datetime                                   comment '创建时间',
  update_by            varchar(64)     default ''                 comment '更新者',
  update_time          datetime                                   comment '更新时间',
  primary key (id)
) engine=innodb auto_increment=100 comment = '校车';


-- ----------------------------
-- 站点
-- ----------------------------
drop table if exists bus_route_site;
create table bus_route_site (
  id              bigint(20)      not null auto_increment    comment 'ID',
  site_name            varchar(64)     default ''                 comment '站点名',
  longitude            varchar(64)     default ''                 comment '经度',
  latitude            varchar(64)     default ''                 comment '纬度',
  address            longtext     default null                 comment '详细地址',
  
  status               char(1)         not null                   comment '状态（0正常 1停用）',
  del_flag             char(1)         default '0'                comment '删除标志（0代表存在 2代表删除）',
  create_by            varchar(64)     default ''                 comment '创建者',
  create_time          datetime                                   comment '创建时间',
  update_by            varchar(64)     default ''                 comment '更新者',
  update_time          datetime                                   comment '更新时间',
  primary key (id)
) engine=innodb auto_increment=100 comment = '站点';

-- ----------------------------
-- 路线
-- ----------------------------
drop table if exists bus_route;
create table bus_route (
  id              bigint(20)      not null auto_increment    comment 'ID',
  route_name            varchar(64)     default ''                 comment '路线名',
  start_site_id            bigint(20)     not null                 comment '起点',
  end_site_id            bigint(20)     not null                 comment '终点',
  forward_route            longtext     default null                 comment '正向路线，json',
  reverse_route            longtext     default null                 comment '反向路线，json',
  
  status               char(1)         not null                   comment '状态（0正常 1停用）',
  del_flag             char(1)         default '0'                comment '删除标志（0代表存在 2代表删除）',
  create_by            varchar(64)     default ''                 comment '创建者',
  create_time          datetime                                   comment '创建时间',
  update_by            varchar(64)     default ''                 comment '更新者',
  update_time          datetime                                   comment '更新时间',
  primary key (id)
) engine=innodb auto_increment=100 comment = '路线';


-- ----------------------------
-- 校车行驶记录
-- ----------------------------
drop table if exists bus_driving_record;
create table bus_driving_record (
  id              bigint(20)      not null auto_increment    comment 'ID',
  bus_id            bigint(20)     not null                 comment '校车',
  driver            bigint(20)     not null                 comment '司机',
  route_id            bigint(20)     not null                 comment '路线',
  record_date            varchar(20)     not null                 comment '运行日期',
  departure_time          datetime                                   comment '发车时间',
  
  status               char(1)         not null                   comment '状态（0正常 1停用）',
  del_flag             char(1)         default '0'                comment '删除标志（0代表存在 2代表删除）',
  create_by            varchar(64)     default ''                 comment '创建者',
  create_time          datetime                                   comment '创建时间',
  update_by            varchar(64)     default ''                 comment '更新者',
  update_time          datetime                                   comment '更新时间',
  primary key (id)
) engine=innodb auto_increment=100 comment = '校车行驶记录';

-- ----------------------------
-- 校车行驶站点记录，产生行驶记录时生成站点记录
-- ----------------------------
drop table if exists bus_record_site;
create table bus_record_site (
  id              bigint(20)      not null auto_increment    comment 'ID',
  record_id            bigint(20)     not null                 comment '行驶记录',
  route_id            bigint(20)     not null                 comment '路线',
  direction            int(20)     default 0                 comment '方向，0正向、1反向',
  site_id            bigint(20)     not null                 comment '站点',
  is_in            int(1)     default 0                 comment '是否到站，0未到、1已到、2已出站',
  in_time          datetime                                   comment '到站时间，精确到分',
  out_time          datetime                                   comment '出站时间，精确到分',
  
  status               char(1)         not null                   comment '状态（0正常 1停用）',
  del_flag             char(1)         default '0'                comment '删除标志（0代表存在 2代表删除）',
  create_by            varchar(64)     default ''                 comment '创建者',
  create_time          datetime                                   comment '创建时间',
  update_by            varchar(64)     default ''                 comment '更新者',
  update_time          datetime                                   comment '更新时间',
  primary key (id)
) engine=innodb auto_increment=100 comment = '校车行驶站点记录';


-- ----------------------------
-- 校车行驶记录实时站点之间距离和时间
-- ----------------------------
drop table if exists bus_record_realtime;
create table bus_record_realtime (
  id              bigint(20)      not null auto_increment    comment 'ID',
  record_id            bigint(20)     not null                 comment '行驶记录',
  route_id            bigint(20)     not null                 comment '路线',
  start_site_id            bigint(20)     default null                 comment '起始站点',
  start_coordinate         varchar(100)     default null           comment '起始坐标，车辆实时坐标',
  end_site_id            bigint(20)     not null                 comment '结束站点',
  search_time          varchar(30)        default ''                 comment '查询时间',
  distance            varchar(10)     default '0'                 comment '距离',
  in_time          varchar(10)        default '0'                 comment '时间',
  other_msg          longtext        default null                 comment '其它',
  
  status               char(1)         not null                   comment '状态（0正常 1停用）',
  del_flag             char(1)         default '0'                comment '删除标志（0代表存在 2代表删除）',
  create_by            varchar(64)     default ''                 comment '创建者',
  create_time          datetime                                   comment '创建时间',
  update_by            varchar(64)     default ''                 comment '更新者',
  update_time          datetime                                   comment '更新时间',
  primary key (id)
) engine=innodb auto_increment=1 comment = '校车行驶记录实时站点记录';

-- ----------------------------
-- 乘车预约记录
-- ----------------------------
drop table if exists bus_reservation;
create table bus_reservation (
  id              bigint(20)      not null auto_increment    comment 'ID',
  record_id            bigint(20)     not null                 comment '行驶记录',
  user_id            bigint(20)     not null                 comment '预约人',
  reservation_time          datetime                                   comment '预约时间',
  in_site_id            bigint(20)     not null                 comment '上车站点',
  out_site_id            bigint(20)     default null                 comment '下车站点',
  diding_status               char(1)         not null                   comment '乘车状态（0已预约 1已上车 2已下车 3已取消），未取消的记录在预约时间3小时候后默认调整为已乘坐并已下车',
  
  status               char(1)         not null                   comment '状态（0正常 1停用）',
  del_flag             char(1)         default '0'                comment '删除标志（0代表存在 2代表删除）',
  create_by            varchar(64)     default ''                 comment '创建者',
  create_time          datetime                                   comment '创建时间',
  update_by            varchar(64)     default ''                 comment '更新者',
  update_time          datetime                                   comment '更新时间',
  primary key (id)
) engine=innodb auto_increment=100 comment = '乘车预约记录';


