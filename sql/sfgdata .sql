-- MySQL dump 10.13  Distrib 8.0.30, for Win64 (x86_64)
--
-- Host: localhost    Database: ry-vue
-- ------------------------------------------------------
-- Server version	8.0.30

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `sfg_education`
--

DROP TABLE IF EXISTS `sfg_education`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `sfg_education` (
  `education_id` int NOT NULL AUTO_INCREMENT,
  `education_person_id` int DEFAULT NULL COMMENT '人编号',
  `education_school_name` varchar(100) DEFAULT NULL COMMENT '学校名',
  `education_educational_level` varchar(100) DEFAULT NULL COMMENT '教育程度',
  `education_admission_time` datetime DEFAULT NULL COMMENT '入学时间',
  `education_current_grade` varchar(100) DEFAULT NULL COMMENT '现在年级',
  `education_graduation_time` date DEFAULT NULL COMMENT '毕业时间',
  `education_quit_school` varchar(2) DEFAULT NULL COMMENT '是否辍学',
  PRIMARY KEY (`education_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sfg_education`
--

LOCK TABLES `sfg_education` WRITE;
/*!40000 ALTER TABLE `sfg_education` DISABLE KEYS */;
INSERT INTO `sfg_education` VALUES (1,12,'贵州轻工','大学','2021-09-08 00:00:00','大三','2024-06-05','否'),(2,13,'贵州轻工1','大学','2022-09-08 00:00:00','大二','2024-06-05','否'),(3,14,'贵州轻工2','大学','2022-09-08 00:00:00','大二','2024-06-05','否'),(4,15,'贵州轻工3','大学','2022-09-08 00:00:00','大二','2024-06-05','否'),(5,16,'贵州轻工4','大学','2022-09-08 00:00:00','大二','2024-06-05','否'),(6,NULL,NULL,NULL,NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `sfg_education` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sfg_employment`
--

DROP TABLE IF EXISTS `sfg_employment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `sfg_employment` (
  `employment_id` int NOT NULL AUTO_INCREMENT,
  `employment_person_id` int DEFAULT NULL COMMENT '人编号',
  `employment_or_working` varchar(2) DEFAULT NULL COMMENT '是否外出打工',
  `employmente_aidpost` varchar(2) DEFAULT NULL COMMENT '援助岗位',
  `employmente_nurse` varchar(2) DEFAULT NULL COMMENT '护林员',
  `employmente_cleaner` varchar(2) DEFAULT NULL COMMENT '清洁员',
  `employmente_plumber` varchar(2) DEFAULT NULL COMMENT '管水员',
  `employmente_village_cadre` varchar(50) DEFAULT NULL COMMENT '村干部',
  `employment_salary` int DEFAULT NULL COMMENT '薪资',
  `employment_work_unit` varchar(255) DEFAULT NULL COMMENT '工作单位',
  `employment_work_time` datetime DEFAULT NULL COMMENT '工作时间',
  `employment_phone` varchar(12) DEFAULT NULL COMMENT '手机号',
  PRIMARY KEY (`employment_id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sfg_employment`
--

LOCK TABLES `sfg_employment` WRITE;
/*!40000 ALTER TABLE `sfg_employment` DISABLE KEYS */;
INSERT INTO `sfg_employment` VALUES (1,24,'否','是','是','否','是','村支书',3000,'村干部','2023-04-28 12:00:00','123'),(5,15,'是','否','是','否','是','村支书',3000,'村干部','2022-06-12 00:00:00','123'),(9,23,'上海','是','是','是','是','副书记',123,'qw','2023-04-23 12:00:00','123123'),(12,2,'北京','是','是','否','否','党支部委员',23,'23','2023-04-23 12:00:00','2323'),(13,98,'上海','是','否','否','是','副主任',3600,'78','2023-04-23 12:00:00','18798443981'),(14,30,'北京','否','否','否','是','无',2500,'管水员','2023-04-28 12:00:00','18798443984');
/*!40000 ALTER TABLE `sfg_employment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sfg_family_member`
--

DROP TABLE IF EXISTS `sfg_family_member`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `sfg_family_member` (
  `family_id` int NOT NULL AUTO_INCREMENT,
  `family_person_id` int DEFAULT NULL COMMENT '人编号',
  `family_account_id` int DEFAULT NULL COMMENT '户编号',
  `family_name` varchar(255) DEFAULT NULL COMMENT '姓名',
  `family_age` int DEFAULT NULL COMMENT '年龄',
  `family_sex` varchar(2) DEFAULT NULL COMMENT '性别',
  `family_birth_time` datetime DEFAULT NULL COMMENT '出生时间',
  `family_number_id` varchar(255) DEFAULT NULL COMMENT '身份证',
  `family_householder_relation` varchar(255) DEFAULT NULL COMMENT '户主关系',
  `family_or_partymember` varchar(2) DEFAULT NULL COMMENT '是否党员',
  `family_theparty` datetime DEFAULT NULL COMMENT '入党时间',
  `family_count` int DEFAULT NULL COMMENT '家庭人数',
  `family_group_name` varchar(255) DEFAULT NULL COMMENT '组名',
  `family_village_name` varchar(255) DEFAULT NULL COMMENT '村名',
  PRIMARY KEY (`family_id`)
) ENGINE=InnoDB AUTO_INCREMENT=40 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sfg_family_member`
--

LOCK TABLES `sfg_family_member` WRITE;
/*!40000 ALTER TABLE `sfg_family_member` DISABLE KEYS */;
INSERT INTO `sfg_family_member` VALUES (9,45,34,'青蛙王子',23,'男','2023-04-20 00:00:00','456125465464561324','父子','是','2023-04-21 00:00:00',12,'唐池','唐池'),(38,30,34,'spring redis',20,'男','2023-05-03 00:00:00','522731200009054878','父子','是','2023-05-25 00:00:00',4,'第三组','大唐'),(39,66,45,'张宏',12,'男','2023-05-11 00:00:00','522131245231464564','父子','是','2023-05-11 00:00:00',30,'第4组','蛋汤');
/*!40000 ALTER TABLE `sfg_family_member` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sfg_partmember`
--

DROP TABLE IF EXISTS `sfg_partmember`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `sfg_partmember` (
  `partMember_id` int NOT NULL AUTO_INCREMENT,
  `partMember_person_id` int DEFAULT NULL COMMENT '人编号',
  `partMember_theparty` date DEFAULT NULL COMMENT '入党时间',
  `partMember_phone` varchar(12) DEFAULT NULL COMMENT '手机号',
  `partMember_nationality` varchar(50) DEFAULT NULL COMMENT '民族',
  PRIMARY KEY (`partMember_id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sfg_partmember`
--

LOCK TABLES `sfg_partmember` WRITE;
/*!40000 ALTER TABLE `sfg_partmember` DISABLE KEYS */;
INSERT INTO `sfg_partmember` VALUES (1,30,'2021-02-01','18798443844','汉族'),(2,45,'2021-05-01','18298443844','苗族'),(3,50,'2021-09-01','18398443844','布依族'),(4,51,'2021-07-01','18798003844','汉族'),(5,12,'2023-04-28','18798443981','布依族'),(6,13,'2023-05-11','18798443981','苗族'),(7,30,'2021-02-01','18798443844','汉族'),(8,45,'2021-05-01','18298443844','苗族'),(9,50,'2021-09-01','18398443844','布依族'),(10,51,'2021-07-01','18798003844','汉族');
/*!40000 ALTER TABLE `sfg_partmember` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sfg_subsidy`
--

DROP TABLE IF EXISTS `sfg_subsidy`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `sfg_subsidy` (
  `subsidy_id` int NOT NULL AUTO_INCREMENT,
  `subsidy_person_id` int DEFAULT NULL COMMENT '人编号',
  `subsidy_disability_allowance` int DEFAULT NULL COMMENT '残疾补贴',
  `subsidy_old_age_allowance` int DEFAULT NULL COMMENT '高龄补贴',
  `subsidy_orphan_allowance` int DEFAULT NULL COMMENT '孤儿补助',
  `subsidy_temporary_rescue` int DEFAULT NULL COMMENT '临时救助',
  `subsidy_subsistence_allowance` int DEFAULT NULL COMMENT '低保金',
  `subsidy_emergency_allowance` int DEFAULT NULL COMMENT '特困供养金',
  `subsidy_retired_servicemen` int DEFAULT NULL COMMENT '退役军人补助金',
  `subsidy_account_id` int DEFAULT NULL,
  `subsidy_number` int DEFAULT '0' COMMENT '人数',
  `subsidy_solve` varchar(10) DEFAULT NULL COMMENT '是否解决',
  `subsidy_type_or_grade` varchar(40) DEFAULT NULL COMMENT '类型或等级',
  `subsidy_time` date DEFAULT NULL COMMENT '时间',
  PRIMARY KEY (`subsidy_id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sfg_subsidy`
--

LOCK TABLES `sfg_subsidy` WRITE;
/*!40000 ALTER TABLE `sfg_subsidy` DISABLE KEYS */;
INSERT INTO `sfg_subsidy` VALUES (1,12,260,3000,1200,1200,1230,211,1200,112,4,'是','三等级','2023-04-25'),(2,14,260,3000,1200,1000,1230,211,1200,112,4,'是','三等级','2023-04-25'),(3,20,260,3000,1200,2000,1230,211,1200,112,4,'是','三等级','2023-04-25'),(4,30,260,3010,1200,1211,1230,1211,3000,112,3,'是','三等级','2023-04-25'),(6,23,260,3000,1200,1211,1230,211,1200,0,4,'是','三等级','2023-04-25'),(7,34,260,3000,1200,1211,1230,211,1200,34,4,'是','三等级','2023-04-25'),(8,27,260,3000,1200,1211,1230,211,1200,NULL,4,'是','三等级','2023-04-25'),(13,13,129,3400,1233,1200,300,2330,1200,NULL,5,'是','二等级','2023-05-06'),(14,15,123,234,123,1234,234,123,123,NULL,4,'是','二等级','2023-05-18'),(15,45,245,123,345,123,123,343,123,NULL,4,'是','一等级','2023-05-03'),(16,2,456,125,456,434,234,45,234,NULL,6,'是','二等级','2023-05-09'),(17,66,56,434,456,56,345,56,435,NULL,5,'是','二等级','2023-05-31');
/*!40000 ALTER TABLE `sfg_subsidy` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2023-05-09 20:43:10
