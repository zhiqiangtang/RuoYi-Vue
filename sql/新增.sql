/*
 Navicat Premium Data Transfer

 Source Server         : mysql
 Source Server Type    : MySQL
 Source Server Version : 80030
 Source Host           : localhost:3306
 Source Schema         : ry_vue

 Target Server Type    : MySQL
 Target Server Version : 80030
 File Encoding         : 65001

 Date: 12/10/2023 08:43:42
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for bus_notice
-- ----------------------------
DROP TABLE IF EXISTS `bus_notice`;
CREATE TABLE `bus_notice`  (
  `id` int(0) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `title_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_nn_0900_ai_ci NULL DEFAULT NULL COMMENT '标题名',
  `type` int(0) NULL DEFAULT NULL COMMENT '1表示通知，0表示公告',
  `notice_content` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_nn_0900_ai_ci NULL COMMENT '文章',
  `status` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_nn_0900_ai_ci NULL DEFAULT '0' COMMENT '1表示已经发布，0表示下架',
  `operate` int(0) NULL DEFAULT NULL COMMENT '1表示发布，0下架',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `create_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_nn_0900_ai_ci NULL DEFAULT NULL COMMENT '创建者',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_nn_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of bus_notice
-- ----------------------------
INSERT INTO `bus_notice` VALUES (1, '路线详情', 2, '<p>路线详情</p>', '0', 1, '2023-10-11 12:23:50', NULL);

SET FOREIGN_KEY_CHECKS = 1;
