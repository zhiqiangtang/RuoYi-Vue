/*
 Navicat Premium Data Transfer

 Source Server         : qyxc
 Source Server Type    : MySQL
 Source Server Version : 80019
 Source Host           : 10.255.3.20:3306
 Source Schema         : ry_vue

 Target Server Type    : MySQL
 Target Server Version : 80019
 File Encoding         : 65001

 Date: 14/12/2023 11:04:37
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for bus_route_site
-- ----------------------------
DROP TABLE IF EXISTS `bus_route_site`;
CREATE TABLE `bus_route_site`  (
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `site_name` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '站点名',
  `longitude` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '经度',
  `latitude` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '纬度',
  `address` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL COMMENT '详细地址',
  `status` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '状态（0正常 1停用）',
  `del_flag` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '0' COMMENT '删除标志（0代表存在 2代表删除）',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 141 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '站点' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of bus_route_site
-- ----------------------------
INSERT INTO `bus_route_site` VALUES (115, '贵轻工', '106.478882', '26.341022', '贵轻工', '0', '0', '', '2023-09-25 09:20:46', '', NULL);
INSERT INTO `bus_route_site` VALUES (121, '玉米地', '106.46793240017361', '26.345334743923612', '玉米地', '0', '0', '', '2023-09-26 10:18:21', '', NULL);
INSERT INTO `bus_route_site` VALUES (122, '第一个红绿灯路口', '106.49023898654514', '26.341166178385418', '红路灯直走10米', '0', '0', '', '2023-09-26 10:18:31', '', NULL);
INSERT INTO `bus_route_site` VALUES (123, '中铁八局', '106.47498996310765', '26.3445556640625', '中铁八局', '0', '0', '', '2023-09-26 10:21:18', '', NULL);
INSERT INTO `bus_route_site` VALUES (124, '阳谷路', '106.49660400390626', '26.340757378472222', '阳谷路十字路口', '0', '0', '', '2023-09-26 10:23:26', '', NULL);
INSERT INTO `bus_route_site` VALUES (125, '贵州轻工职业技术学院-北门', '106.4813560655382', '26.34149169921875', '贵州轻工职业技术学院-北门', '0', '0', '', '2023-09-26 10:24:57', '', NULL);
INSERT INTO `bus_route_site` VALUES (126, '阳谷路2', '106.49658989800348', '26.340844455295137', '阳谷路', '0', '0', '', '2023-09-26 11:24:14', '', NULL);
INSERT INTO `bus_route_site` VALUES (127, '7号电缆分支箱', '106.49668267144098', '26.340850965711805', '7号电缆', '0', '0', '', '2023-09-26 11:28:43', '', NULL);
INSERT INTO `bus_route_site` VALUES (128, '轻工侧门-测试', '106.48133572048611', '26.339704318576388', '安顺市', '0', '0', '', '2023-12-13 11:27:39', '', NULL);
INSERT INTO `bus_route_site` VALUES (129, '碧桂园学府一号', '106.61409152560763', '26.395948079427082', '贵阳', '0', '0', '', '2023-12-14 09:28:28', '', NULL);
INSERT INTO `bus_route_site` VALUES (130, '师大侧门', '106.61469428168402', '26.395513780381943', '贵阳', '0', '0', '', '2023-12-14 09:32:26', '', NULL);
INSERT INTO `bus_route_site` VALUES (131, '师大西门', '106.47511935763889', '26.339065755208335', '贵阳', '0', '0', '', '2023-12-14 09:34:48', '', NULL);
INSERT INTO `bus_route_site` VALUES (132, '群生小区', '106.63021158854167', '26.386683213975694', '贵阳', '0', '0', '', '2023-12-14 09:39:11', '', NULL);
INSERT INTO `bus_route_site` VALUES (133, '恒大观山学府南门', '106.63386610243056', '26.401278211805554', '贵阳', '0', '0', '', '2023-12-14 09:41:48', '', NULL);
INSERT INTO `bus_route_site` VALUES (134, '学府青藤', '106.63392605251737', '26.401290147569444', '贵阳', '0', '0', '', '2023-12-14 09:43:48', '', NULL);
INSERT INTO `bus_route_site` VALUES (135, '学府青藤', '106.64369357638888', '26.400655924479167', '贵阳', '0', '0', '', '2023-12-14 09:44:48', '', NULL);
INSERT INTO `bus_route_site` VALUES (136, '保利西湖', '106.64481146918403', '26.40065375434028', '贵阳', '0', '0', '', '2023-12-14 09:50:56', '', NULL);
INSERT INTO `bus_route_site` VALUES (137, '花溪行政中心', '106.66521294487848', '26.40932807074653', '贵阳', '0', '0', '', '2023-12-14 09:54:23', '', NULL);
INSERT INTO `bus_route_site` VALUES (138, '花溪交警七大队', '106.66969997829861', '26.408515625', '贵阳', '0', '0', '', '2023-12-14 10:02:11', '', NULL);
INSERT INTO `bus_route_site` VALUES (139, '花溪车站对面', '106.6769691297743', '26.437808159722223', '贵阳', '0', '0', '', '2023-12-14 10:10:07', '', NULL);
INSERT INTO `bus_route_site` VALUES (140, '花溪交警七大队_回', '106.676845703125', '26.437423231336805', '贵阳', '0', '0', '', '2023-12-14 10:14:58', '', NULL);

SET FOREIGN_KEY_CHECKS = 1;
