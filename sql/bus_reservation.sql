/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MySQL
 Source Server Version : 80012
 Source Host           : localhost:3306
 Source Schema         : ry_vue

 Target Server Type    : MySQL
 Target Server Version : 80012
 File Encoding         : 65001

 Date: 26/12/2023 13:07:49
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for bus_reservation
-- ----------------------------
DROP TABLE IF EXISTS `bus_reservation`;
CREATE TABLE `bus_reservation`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `record_id` bigint(20) NOT NULL COMMENT '行驶记录',
  `user_id` bigint(20) NOT NULL COMMENT '预约人',
  `reservation_time` datetime NULL DEFAULT NULL COMMENT '预约时间',
  `in_site_id` bigint(20) NULL DEFAULT NULL COMMENT '上车站点',
  `out_site_id` bigint(20) NULL DEFAULT NULL COMMENT '下车站点',
  `diding_status` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '乘车状态（0已预约 1已上车 2已下车 3已取消），未取消的记录在预约时间3小时候后默认调整为已乘坐并已下车',
  `status` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '状态（0正常 1停用）',
  `del_flag` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '0' COMMENT '删除标志（0代表存在 2代表删除）',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  `record_date` datetime NULL DEFAULT NULL COMMENT '行驶记录日期',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 326 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '乘车预约记录' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of bus_reservation
-- ----------------------------
INSERT INTO `bus_reservation` VALUES (331, 105, 1, '2023-12-26 12:49:45', 123, NULL, '0', '0', '0', '', '2023-12-26 12:49:45', '', NULL, '2023-12-26 00:00:00');

SET FOREIGN_KEY_CHECKS = 1;
