-- MySQL dump 10.13  Distrib 5.7.37, for Win64 (x86_64)
--
-- Host: localhost    Database: ry-vue
-- ------------------------------------------------------
-- Server version	5.7.37

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `bus_bus`
--

DROP TABLE IF EXISTS `bus_bus`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bus_bus` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `bus_name` varchar(64) DEFAULT '' COMMENT '校车名',
  `plate` varchar(64) DEFAULT '' COMMENT '车牌号',
  `driver` bigint(20) DEFAULT NULL COMMENT '司机',
  `seating` int(3) DEFAULT '0' COMMENT '座位数',
  `status` char(1) NOT NULL COMMENT '状态（0正常 1停用）',
  `del_flag` char(1) DEFAULT '0' COMMENT '删除标志（0代表存在 2代表删除）',
  `create_by` varchar(64) DEFAULT '' COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) DEFAULT '' COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=102 DEFAULT CHARSET=utf8mb4 COMMENT='校车';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `bus_bus`
--

LOCK TABLES `bus_bus` WRITE;
/*!40000 ALTER TABLE `bus_bus` DISABLE KEYS */;
INSERT INTO `bus_bus` VALUES (100,'校车11','贵A654321',100,30,'0','0','','2023-09-16 11:18:35','','2023-09-16 15:51:43'),(101,'校车22','贵A123456',100,20,'0','0','','2023-09-16 17:10:40','',NULL);
/*!40000 ALTER TABLE `bus_bus` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `bus_driving_record`
--

DROP TABLE IF EXISTS `bus_driving_record`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bus_driving_record` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `bus_id` bigint(20) NOT NULL COMMENT '校车',
  `driver` bigint(20) NOT NULL COMMENT '司机',
  `route_id` bigint(20) NOT NULL COMMENT '路线',
  `departure_time` datetime DEFAULT NULL COMMENT '发车时间',
  `status` char(1) NOT NULL COMMENT '状态（0正常 1停用）',
  `del_flag` char(1) DEFAULT '0' COMMENT '删除标志（0代表存在 2代表删除）',
  `create_by` varchar(64) DEFAULT '' COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) DEFAULT '' COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `record_date` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=102 DEFAULT CHARSET=utf8mb4 COMMENT='校车行驶记录';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `bus_driving_record`
--

LOCK TABLES `bus_driving_record` WRITE;
/*!40000 ALTER TABLE `bus_driving_record` DISABLE KEYS */;
INSERT INTO `bus_driving_record` VALUES (100,101,100,100,'2023-09-17 00:00:00','0','0','','2023-09-16 11:40:52','','2023-09-16 17:12:09','2023-09-17'),(101,100,100,100,'2023-09-22 00:00:00','0','0','','2023-09-16 17:22:39','','2023-09-16 17:22:47','2023-09-22');
/*!40000 ALTER TABLE `bus_driving_record` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `bus_record_site`
--

DROP TABLE IF EXISTS `bus_record_site`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bus_record_site` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `record_id` bigint(20) NOT NULL COMMENT '行驶记录',
  `route_id` bigint(20) NOT NULL COMMENT '路线',
  `site_id` bigint(20) NOT NULL COMMENT '站点',
  `is_in` int(1) NOT NULL COMMENT '是否到站',
  `in_time` datetime DEFAULT NULL COMMENT '到站时间',
  `out_time` datetime DEFAULT NULL COMMENT '出站时间',
  `status` char(1) NOT NULL COMMENT '状态（0正常 1停用）',
  `del_flag` char(1) DEFAULT '0' COMMENT '删除标志（0代表存在 2代表删除）',
  `create_by` varchar(64) DEFAULT '' COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) DEFAULT '' COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='校车行驶站点记录';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `bus_record_site`
--

LOCK TABLES `bus_record_site` WRITE;
/*!40000 ALTER TABLE `bus_record_site` DISABLE KEYS */;
/*!40000 ALTER TABLE `bus_record_site` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `bus_reservation`
--

DROP TABLE IF EXISTS `bus_reservation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bus_reservation` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `record_id` bigint(20) NOT NULL COMMENT '行驶记录',
  `user_id` bigint(20) NOT NULL COMMENT '预约人',
  `reservation_time` datetime DEFAULT NULL COMMENT '预约时间',
  `in_site_id` bigint(20) NOT NULL COMMENT '上车站点',
  `out_site_id` bigint(20) DEFAULT NULL COMMENT '下车站点',
  `diding_status` char(1) NOT NULL COMMENT '乘车状态（0已预约 1已上车 2已下车 3已取消），未取消的记录在预约时间3小时候后默认调整为已乘坐并已下车',
  `status` char(1) NOT NULL COMMENT '状态（0正常 1停用）',
  `del_flag` char(1) DEFAULT '0' COMMENT '删除标志（0代表存在 2代表删除）',
  `create_by` varchar(64) DEFAULT '' COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) DEFAULT '' COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=101 DEFAULT CHARSET=utf8mb4 COMMENT='乘车预约记录';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `bus_reservation`
--

LOCK TABLES `bus_reservation` WRITE;
/*!40000 ALTER TABLE `bus_reservation` DISABLE KEYS */;
INSERT INTO `bus_reservation` VALUES (100,100,1,'2023-09-17 00:00:00',104,112,'0','0','0','','2023-09-17 16:09:22','','2023-09-17 20:16:21');
/*!40000 ALTER TABLE `bus_reservation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `bus_route`
--

DROP TABLE IF EXISTS `bus_route`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bus_route` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `route_name` varchar(64) DEFAULT '' COMMENT '路线名',
  `start_site_id` bigint(20) NOT NULL COMMENT '起点',
  `end_site_id` bigint(20) NOT NULL COMMENT '终点',
  `forward_route` longtext COMMENT '正向路线，json',
  `reverse_route` longtext COMMENT '反向路线，json',
  `status` char(1) NOT NULL COMMENT '状态（0正常 1停用）',
  `del_flag` char(1) DEFAULT '0' COMMENT '删除标志（0代表存在 2代表删除）',
  `create_by` varchar(64) DEFAULT '' COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) DEFAULT '' COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=101 DEFAULT CHARSET=utf8mb4 COMMENT='路线';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `bus_route`
--

LOCK TABLES `bus_route` WRITE;
/*!40000 ALTER TABLE `bus_route` DISABLE KEYS */;
INSERT INTO `bus_route` VALUES (100,'一号车线路',100,114,'[100,101,102,103,104,105,106,107,108,109,110,111,112,113,114]','[114,113,112,111,110,109,108,107,106,105,104,103,102,101,100]','0','0','','2023-09-16 15:47:19','','2023-09-16 22:46:43');
/*!40000 ALTER TABLE `bus_route` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `bus_route_site`
--

DROP TABLE IF EXISTS `bus_route_site`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bus_route_site` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `site_name` varchar(64) DEFAULT '' COMMENT '站点名',
  `longitude` varchar(64) DEFAULT '' COMMENT '经度',
  `latitude` varchar(64) DEFAULT '' COMMENT '纬度',
  `address` longtext COMMENT '详细地址',
  `status` char(1) NOT NULL COMMENT '状态（0正常 1停用）',
  `del_flag` char(1) DEFAULT '0' COMMENT '删除标志（0代表存在 2代表删除）',
  `create_by` varchar(64) DEFAULT '' COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) DEFAULT '' COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=115 DEFAULT CHARSET=utf8mb4 COMMENT='站点';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `bus_route_site`
--

LOCK TABLES `bus_route_site` WRITE;
/*!40000 ALTER TABLE `bus_route_site` DISABLE KEYS */;
INSERT INTO `bus_route_site` VALUES (100,'白云区党委','66°33\' 38\" N','66°33\' 38\" N','白云区党委','0','0','','2023-09-16 11:39:30','','2023-09-16 15:41:21'),(101,'金阳新世界','1','1','金阳新世界','0','0','','2023-09-16 15:42:28','',NULL),(102,'省教育厅','1','1','省教育厅','0','0','','2023-09-16 15:44:51','',NULL),(103,'金融城','1','1','金融城','0','0','','2023-09-16 15:45:04','',NULL),(104,'金融101大厦（数博大道与观山东路交叉口）','1','1','金融101大厦（数博大道与观山东路交叉口）','0','0','','2023-09-16 15:45:14','',NULL),(105,'兴筑东路口','1','1','兴筑东路口','0','0','','2023-09-16 15:45:22','',NULL),(106,'长岭南路口（北京西路）','1','1','长岭南路口（北京西路）','0','0','','2023-09-16 15:45:31','',NULL),(107,'小湾河大桥','1','1','小湾河大桥','0','0','','2023-09-16 15:45:39','',NULL),(108,'北京西路公交站（金阳南路地铁站B出口旁）','1','1','北京西路公交站（金阳南路地铁站B出口旁）','0','0','','2023-09-16 15:45:49','',NULL),(109,'金清大道口（中铁逸都国际）','1','1','金清大道口（中铁逸都国际）','0','0','','2023-09-16 15:45:59','',NULL),(110,'二铺公交站','1','1','二铺公交站','0','0','','2023-09-16 15:46:07','',NULL),(111,'宾阳大道（南）公交站','1','1','宾阳大道（南）公交站','0','0','','2023-09-16 15:46:16','',NULL),(112,'华丰食品城公交站','1','1','华丰食品城公交站','0','0','','2023-09-16 15:46:24','',NULL),(113,'经宾阳大道（百马大道-兴安大道）','1','1','经宾阳大道（百马大道-兴安大道）','0','0','','2023-09-16 15:46:33','',NULL),(114,'富贵安康小镇（新校区公租房）','1','1','富贵安康小镇（新校区公租房）','0','0','','2023-09-16 15:46:51','',NULL);
/*!40000 ALTER TABLE `bus_route_site` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gen_table`
--

DROP TABLE IF EXISTS `gen_table`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gen_table` (
  `table_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `table_name` varchar(200) DEFAULT '' COMMENT '表名称',
  `table_comment` varchar(500) DEFAULT '' COMMENT '表描述',
  `sub_table_name` varchar(64) DEFAULT NULL COMMENT '关联子表的表名',
  `sub_table_fk_name` varchar(64) DEFAULT NULL COMMENT '子表关联的外键名',
  `class_name` varchar(100) DEFAULT '' COMMENT '实体类名称',
  `tpl_category` varchar(200) DEFAULT 'crud' COMMENT '使用的模板（crud单表操作 tree树表操作）',
  `package_name` varchar(100) DEFAULT NULL COMMENT '生成包路径',
  `module_name` varchar(30) DEFAULT NULL COMMENT '生成模块名',
  `business_name` varchar(30) DEFAULT NULL COMMENT '生成业务名',
  `function_name` varchar(50) DEFAULT NULL COMMENT '生成功能名',
  `function_author` varchar(50) DEFAULT NULL COMMENT '生成功能作者',
  `gen_type` char(1) DEFAULT '0' COMMENT '生成代码方式（0zip压缩包 1自定义路径）',
  `gen_path` varchar(200) DEFAULT '/' COMMENT '生成路径（不填默认项目路径）',
  `options` varchar(1000) DEFAULT NULL COMMENT '其它生成选项',
  `create_by` varchar(64) DEFAULT '' COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) DEFAULT '' COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`table_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COMMENT='代码生成业务表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gen_table`
--

LOCK TABLES `gen_table` WRITE;
/*!40000 ALTER TABLE `gen_table` DISABLE KEYS */;
INSERT INTO `gen_table` VALUES (1,'bus_bus','校车',NULL,NULL,'BusBus','crud','com.ruoyi.system','system','bus','校车','ruoyi','0','/','{\"parentMenuId\":\"1\"}','admin','2023-09-16 11:11:02','','2023-09-16 11:20:34',NULL),(2,'bus_route_site','站点',NULL,NULL,'BusRouteSite','crud','com.ruoyi.system','system','site','站点','ruoyi','0','/','{\"parentMenuId\":1}','admin','2023-09-16 11:26:10','','2023-09-16 11:27:02',NULL),(3,'bus_route','路线',NULL,NULL,'BusRoute','crud','com.ruoyi.system','system','route','路线','ruoyi','0','/','{\"parentMenuId\":1}','admin','2023-09-16 11:29:00','','2023-09-16 11:30:00',NULL),(4,'bus_driving_record','校车行驶记录',NULL,NULL,'BusDrivingRecord','crud','com.ruoyi.system','system','record','校车行驶记录','ruoyi','0','/','{\"parentMenuId\":1}','admin','2023-09-16 11:31:40','','2023-09-16 11:32:14',NULL),(5,'bus_record_site','校车行驶站点记录',NULL,NULL,'BusRecordSite','crud','com.ruoyi.system','system','recordSite','校车行驶站点记录','ruoyi','0','/','{\"parentMenuId\":1}','admin','2023-09-16 11:33:53','','2023-09-16 11:34:58',NULL),(6,'bus_reservation','乘车预约记录',NULL,NULL,'BusReservation','crud','com.ruoyi.system','system','reservation','乘车预约记录','ruoyi','0','/','{\"parentMenuId\":1}','admin','2023-09-16 11:37:23','','2023-09-16 11:38:09',NULL);
/*!40000 ALTER TABLE `gen_table` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gen_table_column`
--

DROP TABLE IF EXISTS `gen_table_column`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gen_table_column` (
  `column_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `table_id` bigint(20) DEFAULT NULL COMMENT '归属表编号',
  `column_name` varchar(200) DEFAULT NULL COMMENT '列名称',
  `column_comment` varchar(500) DEFAULT NULL COMMENT '列描述',
  `column_type` varchar(100) DEFAULT NULL COMMENT '列类型',
  `java_type` varchar(500) DEFAULT NULL COMMENT 'JAVA类型',
  `java_field` varchar(200) DEFAULT NULL COMMENT 'JAVA字段名',
  `is_pk` char(1) DEFAULT NULL COMMENT '是否主键（1是）',
  `is_increment` char(1) DEFAULT NULL COMMENT '是否自增（1是）',
  `is_required` char(1) DEFAULT NULL COMMENT '是否必填（1是）',
  `is_insert` char(1) DEFAULT NULL COMMENT '是否为插入字段（1是）',
  `is_edit` char(1) DEFAULT NULL COMMENT '是否编辑字段（1是）',
  `is_list` char(1) DEFAULT NULL COMMENT '是否列表字段（1是）',
  `is_query` char(1) DEFAULT NULL COMMENT '是否查询字段（1是）',
  `query_type` varchar(200) DEFAULT 'EQ' COMMENT '查询方式（等于、不等于、大于、小于、范围）',
  `html_type` varchar(200) DEFAULT NULL COMMENT '显示类型（文本框、文本域、下拉框、复选框、单选框、日期控件）',
  `dict_type` varchar(200) DEFAULT '' COMMENT '字典类型',
  `sort` int(11) DEFAULT NULL COMMENT '排序',
  `create_by` varchar(64) DEFAULT '' COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) DEFAULT '' COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`column_id`)
) ENGINE=InnoDB AUTO_INCREMENT=71 DEFAULT CHARSET=utf8mb4 COMMENT='代码生成业务表字段';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gen_table_column`
--

LOCK TABLES `gen_table_column` WRITE;
/*!40000 ALTER TABLE `gen_table_column` DISABLE KEYS */;
INSERT INTO `gen_table_column` VALUES (1,1,'id','ID','bigint(20)','Long','id','1','1',NULL,'1',NULL,NULL,NULL,'EQ','input','',1,'admin','2023-09-16 11:11:02','','2023-09-16 11:20:34'),(2,1,'bus_name','校车名','varchar(64)','String','busName','0','0','1','1','1','1','1','LIKE','input','',2,'admin','2023-09-16 11:11:02','','2023-09-16 11:20:34'),(3,1,'plate','车牌号','varchar(64)','String','plate','0','0','1','1','1','1','1','EQ','input','',3,'admin','2023-09-16 11:11:02','','2023-09-16 11:20:34'),(4,1,'driver','司机','bigint(20)','Long','driver','0','0','1','1','1','1','1','EQ','input','',4,'admin','2023-09-16 11:11:02','','2023-09-16 11:20:34'),(5,1,'seating','座位数','int(3)','Integer','seating','0','0','1','1','1','1','1','EQ','input','',5,'admin','2023-09-16 11:11:02','','2023-09-16 11:20:34'),(6,1,'status','状态（0正常 1停用）','char(1)','String','status','0','0','1','1','1','1','1','EQ','radio','sys_data_status',6,'admin','2023-09-16 11:11:02','','2023-09-16 11:20:34'),(7,1,'del_flag','删除标志（0代表存在 2代表删除）','char(1)','String','delFlag','0','0',NULL,'1',NULL,NULL,NULL,'EQ','input','',7,'admin','2023-09-16 11:11:02','','2023-09-16 11:20:34'),(8,1,'create_by','创建者','varchar(64)','String','createBy','0','0',NULL,'1',NULL,NULL,NULL,'EQ','input','',8,'admin','2023-09-16 11:11:02','','2023-09-16 11:20:34'),(9,1,'create_time','创建时间','datetime','Date','createTime','0','0',NULL,'1',NULL,NULL,NULL,'EQ','datetime','',9,'admin','2023-09-16 11:11:02','','2023-09-16 11:20:34'),(10,1,'update_by','更新者','varchar(64)','String','updateBy','0','0',NULL,'1','1',NULL,NULL,'EQ','input','',10,'admin','2023-09-16 11:11:02','','2023-09-16 11:20:34'),(11,1,'update_time','更新时间','datetime','Date','updateTime','0','0',NULL,'1','1',NULL,NULL,'EQ','datetime','',11,'admin','2023-09-16 11:11:02','','2023-09-16 11:20:34'),(12,2,'id','ID','bigint(20)','Long','id','1','1',NULL,'1',NULL,NULL,NULL,'EQ','input','',1,'admin','2023-09-16 11:26:10','','2023-09-16 11:27:02'),(13,2,'site_name','站点名','varchar(64)','String','siteName','0','0','1','1','1','1','1','LIKE','input','',2,'admin','2023-09-16 11:26:10','','2023-09-16 11:27:02'),(14,2,'longitude','经度','varchar(64)','String','longitude','0','0','1','1','1','1','1','EQ','input','',3,'admin','2023-09-16 11:26:10','','2023-09-16 11:27:02'),(15,2,'latitude','纬度','varchar(64)','String','latitude','0','0','1','1','1','1','1','EQ','input','',4,'admin','2023-09-16 11:26:10','','2023-09-16 11:27:02'),(16,2,'address','详细地址','longtext','String','address','0','0','1','1','1','1','1','EQ','textarea','',5,'admin','2023-09-16 11:26:10','','2023-09-16 11:27:02'),(17,2,'status','状态（0正常 1停用）','char(1)','String','status','0','0','1','1','1','1','1','EQ','radio','sys_data_status',6,'admin','2023-09-16 11:26:10','','2023-09-16 11:27:02'),(18,2,'del_flag','删除标志（0代表存在 2代表删除）','char(1)','String','delFlag','0','0',NULL,'1',NULL,NULL,NULL,'EQ','input','',7,'admin','2023-09-16 11:26:10','','2023-09-16 11:27:02'),(19,2,'create_by','创建者','varchar(64)','String','createBy','0','0',NULL,'1',NULL,NULL,NULL,'EQ','input','',8,'admin','2023-09-16 11:26:10','','2023-09-16 11:27:02'),(20,2,'create_time','创建时间','datetime','Date','createTime','0','0',NULL,'1',NULL,NULL,NULL,'EQ','datetime','',9,'admin','2023-09-16 11:26:10','','2023-09-16 11:27:02'),(21,2,'update_by','更新者','varchar(64)','String','updateBy','0','0',NULL,'1','1',NULL,NULL,'EQ','input','',10,'admin','2023-09-16 11:26:10','','2023-09-16 11:27:02'),(22,2,'update_time','更新时间','datetime','Date','updateTime','0','0',NULL,'1','1',NULL,NULL,'EQ','datetime','',11,'admin','2023-09-16 11:26:10','','2023-09-16 11:27:02'),(23,3,'id','ID','bigint(20)','Long','id','1','1',NULL,'1',NULL,NULL,NULL,'EQ','input','',1,'admin','2023-09-16 11:29:00','','2023-09-16 11:30:00'),(24,3,'route_name','路线名','varchar(64)','String','routeName','0','0','1','1','1','1','1','LIKE','input','',2,'admin','2023-09-16 11:29:00','','2023-09-16 11:30:00'),(25,3,'start_site_id','起点','bigint(20)','Long','startSiteId','0','0','1','1','1','1','1','EQ','input','',3,'admin','2023-09-16 11:29:00','','2023-09-16 11:30:00'),(26,3,'end_site_id','终点','bigint(20)','Long','endSiteId','0','0','1','1','1','1','1','EQ','input','',4,'admin','2023-09-16 11:29:00','','2023-09-16 11:30:00'),(27,3,'forward_route','正向路线，json','longtext','String','forwardRoute','0','0',NULL,'1','1','1','0','EQ','textarea','',5,'admin','2023-09-16 11:29:00','','2023-09-16 11:30:00'),(28,3,'reverse_route','反向路线，json','longtext','String','reverseRoute','0','0',NULL,'1','1','1','0','EQ','textarea','',6,'admin','2023-09-16 11:29:00','','2023-09-16 11:30:00'),(29,3,'status','状态（0正常 1停用）','char(1)','String','status','0','0','1','1','1','1','1','EQ','radio','sys_data_status',7,'admin','2023-09-16 11:29:00','','2023-09-16 11:30:00'),(30,3,'del_flag','删除标志（0代表存在 2代表删除）','char(1)','String','delFlag','0','0',NULL,'1',NULL,NULL,NULL,'EQ','input','',8,'admin','2023-09-16 11:29:00','','2023-09-16 11:30:00'),(31,3,'create_by','创建者','varchar(64)','String','createBy','0','0',NULL,'1',NULL,NULL,NULL,'EQ','input','',9,'admin','2023-09-16 11:29:00','','2023-09-16 11:30:00'),(32,3,'create_time','创建时间','datetime','Date','createTime','0','0',NULL,'1',NULL,NULL,NULL,'EQ','datetime','',10,'admin','2023-09-16 11:29:00','','2023-09-16 11:30:00'),(33,3,'update_by','更新者','varchar(64)','String','updateBy','0','0',NULL,'1','1',NULL,NULL,'EQ','input','',11,'admin','2023-09-16 11:29:00','','2023-09-16 11:30:00'),(34,3,'update_time','更新时间','datetime','Date','updateTime','0','0',NULL,'1','1',NULL,NULL,'EQ','datetime','',12,'admin','2023-09-16 11:29:00','','2023-09-16 11:30:00'),(35,4,'id','ID','bigint(20)','Long','id','1','1',NULL,'1',NULL,NULL,NULL,'EQ','input','',1,'admin','2023-09-16 11:31:40','','2023-09-16 11:32:14'),(36,4,'bus_id','校车','bigint(20)','Long','busId','0','0','1','1','1','1','1','EQ','input','',2,'admin','2023-09-16 11:31:40','','2023-09-16 11:32:14'),(37,4,'driver','司机','bigint(20)','Long','driver','0','0','1','1','1','1','1','EQ','input','',3,'admin','2023-09-16 11:31:40','','2023-09-16 11:32:14'),(38,4,'route_id','路线','bigint(20)','Long','routeId','0','0','1','1','1','1','1','EQ','input','',4,'admin','2023-09-16 11:31:40','','2023-09-16 11:32:14'),(39,4,'departure_time','发车时间','datetime','Date','departureTime','0','0',NULL,'1','1','1','1','EQ','datetime','',5,'admin','2023-09-16 11:31:40','','2023-09-16 11:32:14'),(40,4,'status','状态（0正常 1停用）','char(1)','String','status','0','0','1','1','1','1','1','EQ','radio','sys_data_status',6,'admin','2023-09-16 11:31:40','','2023-09-16 11:32:14'),(41,4,'del_flag','删除标志（0代表存在 2代表删除）','char(1)','String','delFlag','0','0',NULL,'1',NULL,NULL,NULL,'EQ','input','',7,'admin','2023-09-16 11:31:40','','2023-09-16 11:32:14'),(42,4,'create_by','创建者','varchar(64)','String','createBy','0','0',NULL,'1',NULL,NULL,NULL,'EQ','input','',8,'admin','2023-09-16 11:31:40','','2023-09-16 11:32:14'),(43,4,'create_time','创建时间','datetime','Date','createTime','0','0',NULL,'1',NULL,NULL,NULL,'EQ','datetime','',9,'admin','2023-09-16 11:31:40','','2023-09-16 11:32:14'),(44,4,'update_by','更新者','varchar(64)','String','updateBy','0','0',NULL,'1','1',NULL,NULL,'EQ','input','',10,'admin','2023-09-16 11:31:40','','2023-09-16 11:32:14'),(45,4,'update_time','更新时间','datetime','Date','updateTime','0','0',NULL,'1','1',NULL,NULL,'EQ','datetime','',11,'admin','2023-09-16 11:31:40','','2023-09-16 11:32:14'),(46,5,'id','ID','bigint(20)','Long','id','1','1',NULL,'1',NULL,NULL,NULL,'EQ','input','',1,'admin','2023-09-16 11:33:53','','2023-09-16 11:34:58'),(47,5,'record_id','行驶记录','bigint(20)','Long','recordId','0','0','1','1','1','1','1','EQ','input','',2,'admin','2023-09-16 11:33:53','','2023-09-16 11:34:58'),(48,5,'route_id','路线','bigint(20)','Long','routeId','0','0','1','1','1','1','1','EQ','input','',3,'admin','2023-09-16 11:33:53','','2023-09-16 11:34:58'),(49,5,'site_id','站点','bigint(20)','Long','siteId','0','0','1','1','1','1','1','EQ','input','',4,'admin','2023-09-16 11:33:53','','2023-09-16 11:34:58'),(50,5,'is_in','是否到站','int(1)','Integer','isIn','0','0','1','1','1','1','1','EQ','radio','sys_yes_no',5,'admin','2023-09-16 11:33:53','','2023-09-16 11:34:58'),(51,5,'in_time','到站时间','datetime','Date','inTime','0','0',NULL,'1','1','1','1','EQ','datetime','',6,'admin','2023-09-16 11:33:53','','2023-09-16 11:34:58'),(52,5,'out_time','出站时间','datetime','Date','outTime','0','0',NULL,'1','1','1','1','EQ','datetime','',7,'admin','2023-09-16 11:33:53','','2023-09-16 11:34:58'),(53,5,'status','状态（0正常 1停用）','char(1)','String','status','0','0','1','1','1','1','1','EQ','radio','sys_data_status',8,'admin','2023-09-16 11:33:53','','2023-09-16 11:34:58'),(54,5,'del_flag','删除标志（0代表存在 2代表删除）','char(1)','String','delFlag','0','0',NULL,'1',NULL,NULL,NULL,'EQ','input','',9,'admin','2023-09-16 11:33:53','','2023-09-16 11:34:58'),(55,5,'create_by','创建者','varchar(64)','String','createBy','0','0',NULL,'1',NULL,NULL,NULL,'EQ','input','',10,'admin','2023-09-16 11:33:53','','2023-09-16 11:34:58'),(56,5,'create_time','创建时间','datetime','Date','createTime','0','0',NULL,'1',NULL,NULL,NULL,'EQ','datetime','',11,'admin','2023-09-16 11:33:53','','2023-09-16 11:34:58'),(57,5,'update_by','更新者','varchar(64)','String','updateBy','0','0',NULL,'1','1',NULL,NULL,'EQ','input','',12,'admin','2023-09-16 11:33:53','','2023-09-16 11:34:58'),(58,5,'update_time','更新时间','datetime','Date','updateTime','0','0',NULL,'1','1',NULL,NULL,'EQ','datetime','',13,'admin','2023-09-16 11:33:53','','2023-09-16 11:34:58'),(59,6,'id','ID','bigint(20)','Long','id','1','1',NULL,'1',NULL,NULL,NULL,'EQ','input','',1,'admin','2023-09-16 11:37:23','','2023-09-16 11:38:09'),(60,6,'user_id','预约人','bigint(20)','Long','userId','0','0','1','1','1','1','1','EQ','input','',2,'admin','2023-09-16 11:37:23','','2023-09-16 11:38:09'),(61,6,'reservation_time','预约时间','datetime','Date','reservationTime','0','0','1','1','1','1','1','EQ','datetime','',3,'admin','2023-09-16 11:37:23','','2023-09-16 11:38:09'),(62,6,'in_site_id','上车站点','bigint(20)','Long','inSiteId','0','0','1','1','1','1','1','EQ','input','',4,'admin','2023-09-16 11:37:23','','2023-09-16 11:38:09'),(63,6,'out_site_id','下车站点','bigint(20)','Long','outSiteId','0','0','1','1','1','1','1','EQ','input','',5,'admin','2023-09-16 11:37:23','','2023-09-16 11:38:09'),(64,6,'diding_status','乘车状态（0已预约 1已上车 2已下车 3已取消），未取消的记录在预约时间3小时候后默认调整为已乘坐并已下车','char(1)','String','didingStatus','0','0','1','1','1','1','1','EQ','radio','bus_diding_status',6,'admin','2023-09-16 11:37:23','','2023-09-16 11:38:09'),(65,6,'status','状态（0正常 1停用）','char(1)','String','status','0','0','1','1','1','1','1','EQ','radio','sys_data_status',7,'admin','2023-09-16 11:37:23','','2023-09-16 11:38:09'),(66,6,'del_flag','删除标志（0代表存在 2代表删除）','char(1)','String','delFlag','0','0',NULL,'1',NULL,NULL,NULL,'EQ','input','',8,'admin','2023-09-16 11:37:23','','2023-09-16 11:38:09'),(67,6,'create_by','创建者','varchar(64)','String','createBy','0','0',NULL,'1',NULL,NULL,NULL,'EQ','input','',9,'admin','2023-09-16 11:37:23','','2023-09-16 11:38:09'),(68,6,'create_time','创建时间','datetime','Date','createTime','0','0',NULL,'1',NULL,NULL,NULL,'EQ','datetime','',10,'admin','2023-09-16 11:37:23','','2023-09-16 11:38:09'),(69,6,'update_by','更新者','varchar(64)','String','updateBy','0','0',NULL,'1','1',NULL,NULL,'EQ','input','',11,'admin','2023-09-16 11:37:23','','2023-09-16 11:38:09'),(70,6,'update_time','更新时间','datetime','Date','updateTime','0','0',NULL,'1','1',NULL,NULL,'EQ','datetime','',12,'admin','2023-09-16 11:37:23','','2023-09-16 11:38:09');
/*!40000 ALTER TABLE `gen_table_column` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_config`
--

DROP TABLE IF EXISTS `sys_config`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sys_config` (
  `config_id` int(5) NOT NULL AUTO_INCREMENT COMMENT '参数主键',
  `config_name` varchar(100) DEFAULT '' COMMENT '参数名称',
  `config_key` varchar(100) DEFAULT '' COMMENT '参数键名',
  `config_value` varchar(500) DEFAULT '' COMMENT '参数键值',
  `config_type` char(1) DEFAULT 'N' COMMENT '系统内置（Y是 N否）',
  `create_by` varchar(64) DEFAULT '' COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) DEFAULT '' COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`config_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COMMENT='参数配置表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_config`
--

LOCK TABLES `sys_config` WRITE;
/*!40000 ALTER TABLE `sys_config` DISABLE KEYS */;
INSERT INTO `sys_config` VALUES (1,'主框架页-默认皮肤样式名称','sys.index.skinName','skin-blue','Y','admin','2023-09-16 10:17:55','',NULL,'蓝色 skin-blue、绿色 skin-green、紫色 skin-purple、红色 skin-red、黄色 skin-yellow'),(2,'用户管理-账号初始密码','sys.user.initPassword','123456','Y','admin','2023-09-16 10:17:55','',NULL,'初始化密码 123456'),(3,'主框架页-侧边栏主题','sys.index.sideTheme','theme-dark','Y','admin','2023-09-16 10:17:55','',NULL,'深色主题theme-dark，浅色主题theme-light'),(4,'账号自助-验证码开关','sys.account.captchaEnabled','true','Y','admin','2023-09-16 10:17:55','',NULL,'是否开启验证码功能（true开启，false关闭）'),(5,'账号自助-是否开启用户注册功能','sys.account.registerUser','false','Y','admin','2023-09-16 10:17:55','',NULL,'是否开启注册用户功能（true开启，false关闭）'),(6,'用户登录-黑名单列表','sys.login.blackIPList','','Y','admin','2023-09-16 10:17:55','',NULL,'设置登录IP黑名单限制，多个匹配项以;分隔，支持匹配（*通配、网段）');
/*!40000 ALTER TABLE `sys_config` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_dept`
--

DROP TABLE IF EXISTS `sys_dept`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sys_dept` (
  `dept_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '部门id',
  `parent_id` bigint(20) DEFAULT '0' COMMENT '父部门id',
  `ancestors` varchar(50) DEFAULT '' COMMENT '祖级列表',
  `dept_name` varchar(30) DEFAULT '' COMMENT '部门名称',
  `order_num` int(4) DEFAULT '0' COMMENT '显示顺序',
  `leader` varchar(20) DEFAULT NULL COMMENT '负责人',
  `phone` varchar(11) DEFAULT NULL COMMENT '联系电话',
  `email` varchar(50) DEFAULT NULL COMMENT '邮箱',
  `status` char(1) DEFAULT '0' COMMENT '部门状态（0正常 1停用）',
  `del_flag` char(1) DEFAULT '0' COMMENT '删除标志（0代表存在 2代表删除）',
  `create_by` varchar(64) DEFAULT '' COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) DEFAULT '' COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`dept_id`)
) ENGINE=InnoDB AUTO_INCREMENT=110 DEFAULT CHARSET=utf8mb4 COMMENT='部门表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_dept`
--

LOCK TABLES `sys_dept` WRITE;
/*!40000 ALTER TABLE `sys_dept` DISABLE KEYS */;
INSERT INTO `sys_dept` VALUES (100,0,'0','若依科技',0,'若依','15888888888','ry@qq.com','0','0','admin','2023-09-16 10:17:55','',NULL),(101,100,'0,100','深圳总公司',1,'若依','15888888888','ry@qq.com','0','0','admin','2023-09-16 10:17:55','',NULL),(102,100,'0,100','长沙分公司',2,'若依','15888888888','ry@qq.com','0','0','admin','2023-09-16 10:17:55','',NULL),(103,101,'0,100,101','研发部门',1,'若依','15888888888','ry@qq.com','0','0','admin','2023-09-16 10:17:55','',NULL),(104,101,'0,100,101','市场部门',2,'若依','15888888888','ry@qq.com','0','0','admin','2023-09-16 10:17:55','',NULL),(105,101,'0,100,101','测试部门',3,'若依','15888888888','ry@qq.com','0','0','admin','2023-09-16 10:17:55','',NULL),(106,101,'0,100,101','财务部门',4,'若依','15888888888','ry@qq.com','0','0','admin','2023-09-16 10:17:55','',NULL),(107,101,'0,100,101','运维部门',5,'若依','15888888888','ry@qq.com','0','0','admin','2023-09-16 10:17:55','',NULL),(108,102,'0,100,102','市场部门',1,'若依','15888888888','ry@qq.com','0','0','admin','2023-09-16 10:17:55','',NULL),(109,102,'0,100,102','财务部门',2,'若依','15888888888','ry@qq.com','0','0','admin','2023-09-16 10:17:55','',NULL);
/*!40000 ALTER TABLE `sys_dept` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_dict_data`
--

DROP TABLE IF EXISTS `sys_dict_data`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sys_dict_data` (
  `dict_code` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '字典编码',
  `dict_sort` int(4) DEFAULT '0' COMMENT '字典排序',
  `dict_label` varchar(100) DEFAULT '' COMMENT '字典标签',
  `dict_value` varchar(100) DEFAULT '' COMMENT '字典键值',
  `dict_type` varchar(100) DEFAULT '' COMMENT '字典类型',
  `css_class` varchar(100) DEFAULT NULL COMMENT '样式属性（其他样式扩展）',
  `list_class` varchar(100) DEFAULT NULL COMMENT '表格回显样式',
  `is_default` char(1) DEFAULT 'N' COMMENT '是否默认（Y是 N否）',
  `status` char(1) DEFAULT '0' COMMENT '状态（0正常 1停用）',
  `create_by` varchar(64) DEFAULT '' COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) DEFAULT '' COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`dict_code`)
) ENGINE=InnoDB AUTO_INCREMENT=106 DEFAULT CHARSET=utf8mb4 COMMENT='字典数据表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_dict_data`
--

LOCK TABLES `sys_dict_data` WRITE;
/*!40000 ALTER TABLE `sys_dict_data` DISABLE KEYS */;
INSERT INTO `sys_dict_data` VALUES (1,1,'男','0','sys_user_sex','','','Y','0','admin','2023-09-16 10:17:55','',NULL,'性别男'),(2,2,'女','1','sys_user_sex','','','N','0','admin','2023-09-16 10:17:55','',NULL,'性别女'),(3,3,'未知','2','sys_user_sex','','','N','0','admin','2023-09-16 10:17:55','',NULL,'性别未知'),(4,1,'显示','0','sys_show_hide','','primary','Y','0','admin','2023-09-16 10:17:55','',NULL,'显示菜单'),(5,2,'隐藏','1','sys_show_hide','','danger','N','0','admin','2023-09-16 10:17:55','',NULL,'隐藏菜单'),(6,1,'正常','0','sys_normal_disable','','primary','Y','0','admin','2023-09-16 10:17:55','',NULL,'正常状态'),(7,2,'停用','1','sys_normal_disable','','danger','N','0','admin','2023-09-16 10:17:55','',NULL,'停用状态'),(8,1,'正常','0','sys_job_status','','primary','Y','0','admin','2023-09-16 10:17:55','',NULL,'正常状态'),(9,2,'暂停','1','sys_job_status','','danger','N','0','admin','2023-09-16 10:17:55','',NULL,'停用状态'),(10,1,'默认','DEFAULT','sys_job_group','','','Y','0','admin','2023-09-16 10:17:55','',NULL,'默认分组'),(11,2,'系统','SYSTEM','sys_job_group','','','N','0','admin','2023-09-16 10:17:55','',NULL,'系统分组'),(12,1,'是','Y','sys_yes_no','','primary','Y','0','admin','2023-09-16 10:17:55','',NULL,'系统默认是'),(13,2,'否','N','sys_yes_no','','danger','N','0','admin','2023-09-16 10:17:55','',NULL,'系统默认否'),(14,1,'通知','1','sys_notice_type','','warning','Y','0','admin','2023-09-16 10:17:55','',NULL,'通知'),(15,2,'公告','2','sys_notice_type','','success','N','0','admin','2023-09-16 10:17:55','',NULL,'公告'),(16,1,'正常','0','sys_notice_status','','primary','Y','0','admin','2023-09-16 10:17:55','',NULL,'正常状态'),(17,2,'关闭','1','sys_notice_status','','danger','N','0','admin','2023-09-16 10:17:55','',NULL,'关闭状态'),(18,99,'其他','0','sys_oper_type','','info','N','0','admin','2023-09-16 10:17:55','',NULL,'其他操作'),(19,1,'新增','1','sys_oper_type','','info','N','0','admin','2023-09-16 10:17:55','',NULL,'新增操作'),(20,2,'修改','2','sys_oper_type','','info','N','0','admin','2023-09-16 10:17:55','',NULL,'修改操作'),(21,3,'删除','3','sys_oper_type','','danger','N','0','admin','2023-09-16 10:17:55','',NULL,'删除操作'),(22,4,'授权','4','sys_oper_type','','primary','N','0','admin','2023-09-16 10:17:55','',NULL,'授权操作'),(23,5,'导出','5','sys_oper_type','','warning','N','0','admin','2023-09-16 10:17:55','',NULL,'导出操作'),(24,6,'导入','6','sys_oper_type','','warning','N','0','admin','2023-09-16 10:17:55','',NULL,'导入操作'),(25,7,'强退','7','sys_oper_type','','danger','N','0','admin','2023-09-16 10:17:55','',NULL,'强退操作'),(26,8,'生成代码','8','sys_oper_type','','warning','N','0','admin','2023-09-16 10:17:55','',NULL,'生成操作'),(27,9,'清空数据','9','sys_oper_type','','danger','N','0','admin','2023-09-16 10:17:55','',NULL,'清空操作'),(28,1,'成功','0','sys_common_status','','primary','N','0','admin','2023-09-16 10:17:55','',NULL,'正常状态'),(29,2,'失败','1','sys_common_status','','danger','N','0','admin','2023-09-16 10:17:55','',NULL,'停用状态'),(100,0,'正常','0','sys_data_status',NULL,'default','N','0','admin','2023-09-16 11:13:35','',NULL,NULL),(101,1,'停用','1','sys_data_status',NULL,'warning','N','0','admin','2023-09-16 11:13:47','',NULL,NULL),(102,0,'已预约','0','bus_diding_status',NULL,'default','N','0','admin','2023-09-16 11:36:33','',NULL,NULL),(103,1,'已上车','1','bus_diding_status',NULL,'default','N','0','admin','2023-09-16 11:36:44','',NULL,NULL),(104,2,'已下车','2','bus_diding_status',NULL,'default','N','0','admin','2023-09-16 11:36:53','',NULL,NULL),(105,3,'已取消','3','bus_diding_status',NULL,'default','N','0','admin','2023-09-16 11:37:02','',NULL,NULL);
/*!40000 ALTER TABLE `sys_dict_data` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_dict_type`
--

DROP TABLE IF EXISTS `sys_dict_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sys_dict_type` (
  `dict_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '字典主键',
  `dict_name` varchar(100) DEFAULT '' COMMENT '字典名称',
  `dict_type` varchar(100) DEFAULT '' COMMENT '字典类型',
  `status` char(1) DEFAULT '0' COMMENT '状态（0正常 1停用）',
  `create_by` varchar(64) DEFAULT '' COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) DEFAULT '' COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`dict_id`),
  UNIQUE KEY `dict_type` (`dict_type`)
) ENGINE=InnoDB AUTO_INCREMENT=102 DEFAULT CHARSET=utf8mb4 COMMENT='字典类型表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_dict_type`
--

LOCK TABLES `sys_dict_type` WRITE;
/*!40000 ALTER TABLE `sys_dict_type` DISABLE KEYS */;
INSERT INTO `sys_dict_type` VALUES (1,'用户性别','sys_user_sex','0','admin','2023-09-16 10:17:55','',NULL,'用户性别列表'),(2,'菜单状态','sys_show_hide','0','admin','2023-09-16 10:17:55','',NULL,'菜单状态列表'),(3,'系统开关','sys_normal_disable','0','admin','2023-09-16 10:17:55','',NULL,'系统开关列表'),(4,'任务状态','sys_job_status','0','admin','2023-09-16 10:17:55','',NULL,'任务状态列表'),(5,'任务分组','sys_job_group','0','admin','2023-09-16 10:17:55','',NULL,'任务分组列表'),(6,'系统是否','sys_yes_no','0','admin','2023-09-16 10:17:55','',NULL,'系统是否列表'),(7,'通知类型','sys_notice_type','0','admin','2023-09-16 10:17:55','',NULL,'通知类型列表'),(8,'通知状态','sys_notice_status','0','admin','2023-09-16 10:17:55','',NULL,'通知状态列表'),(9,'操作类型','sys_oper_type','0','admin','2023-09-16 10:17:55','',NULL,'操作类型列表'),(10,'系统状态','sys_common_status','0','admin','2023-09-16 10:17:55','',NULL,'登录状态列表'),(100,'数据状态','sys_data_status','0','admin','2023-09-16 11:13:17','',NULL,NULL),(101,'乘车状态','bus_diding_status','0','admin','2023-09-16 11:36:13','',NULL,NULL);
/*!40000 ALTER TABLE `sys_dict_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_job`
--

DROP TABLE IF EXISTS `sys_job`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sys_job` (
  `job_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '任务ID',
  `job_name` varchar(64) NOT NULL DEFAULT '' COMMENT '任务名称',
  `job_group` varchar(64) NOT NULL DEFAULT 'DEFAULT' COMMENT '任务组名',
  `invoke_target` varchar(500) NOT NULL COMMENT '调用目标字符串',
  `cron_expression` varchar(255) DEFAULT '' COMMENT 'cron执行表达式',
  `misfire_policy` varchar(20) DEFAULT '3' COMMENT '计划执行错误策略（1立即执行 2执行一次 3放弃执行）',
  `concurrent` char(1) DEFAULT '1' COMMENT '是否并发执行（0允许 1禁止）',
  `status` char(1) DEFAULT '0' COMMENT '状态（0正常 1暂停）',
  `create_by` varchar(64) DEFAULT '' COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) DEFAULT '' COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) DEFAULT '' COMMENT '备注信息',
  PRIMARY KEY (`job_id`,`job_name`,`job_group`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COMMENT='定时任务调度表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_job`
--

LOCK TABLES `sys_job` WRITE;
/*!40000 ALTER TABLE `sys_job` DISABLE KEYS */;
INSERT INTO `sys_job` VALUES (1,'系统默认（无参）','DEFAULT','ryTask.ryNoParams','0/10 * * * * ?','3','1','1','admin','2023-09-16 10:17:55','',NULL,''),(2,'系统默认（有参）','DEFAULT','ryTask.ryParams(\'ry\')','0/15 * * * * ?','3','1','1','admin','2023-09-16 10:17:55','',NULL,''),(3,'系统默认（多参）','DEFAULT','ryTask.ryMultipleParams(\'ry\', true, 2000L, 316.50D, 100)','0/20 * * * * ?','3','1','1','admin','2023-09-16 10:17:55','',NULL,'');
/*!40000 ALTER TABLE `sys_job` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_job_log`
--

DROP TABLE IF EXISTS `sys_job_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sys_job_log` (
  `job_log_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '任务日志ID',
  `job_name` varchar(64) NOT NULL COMMENT '任务名称',
  `job_group` varchar(64) NOT NULL COMMENT '任务组名',
  `invoke_target` varchar(500) NOT NULL COMMENT '调用目标字符串',
  `job_message` varchar(500) DEFAULT NULL COMMENT '日志信息',
  `status` char(1) DEFAULT '0' COMMENT '执行状态（0正常 1失败）',
  `exception_info` varchar(2000) DEFAULT '' COMMENT '异常信息',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`job_log_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='定时任务调度日志表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_job_log`
--

LOCK TABLES `sys_job_log` WRITE;
/*!40000 ALTER TABLE `sys_job_log` DISABLE KEYS */;
/*!40000 ALTER TABLE `sys_job_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_logininfor`
--

DROP TABLE IF EXISTS `sys_logininfor`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sys_logininfor` (
  `info_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '访问ID',
  `user_name` varchar(50) DEFAULT '' COMMENT '用户账号',
  `ipaddr` varchar(128) DEFAULT '' COMMENT '登录IP地址',
  `login_location` varchar(255) DEFAULT '' COMMENT '登录地点',
  `browser` varchar(50) DEFAULT '' COMMENT '浏览器类型',
  `os` varchar(50) DEFAULT '' COMMENT '操作系统',
  `status` char(1) DEFAULT '0' COMMENT '登录状态（0成功 1失败）',
  `msg` varchar(255) DEFAULT '' COMMENT '提示消息',
  `login_time` datetime DEFAULT NULL COMMENT '访问时间',
  PRIMARY KEY (`info_id`),
  KEY `idx_sys_logininfor_s` (`status`),
  KEY `idx_sys_logininfor_lt` (`login_time`)
) ENGINE=InnoDB AUTO_INCREMENT=138 DEFAULT CHARSET=utf8mb4 COMMENT='系统访问记录';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_logininfor`
--

LOCK TABLES `sys_logininfor` WRITE;
/*!40000 ALTER TABLE `sys_logininfor` DISABLE KEYS */;
INSERT INTO `sys_logininfor` VALUES (100,'admin','127.0.0.1','内网IP','Chrome 11','Windows 10','0','登录成功','2023-09-16 10:27:00'),(101,'admin','127.0.0.1','内网IP','Chrome 11','Windows 10','0','登录成功','2023-09-16 11:10:54'),(102,'admin','127.0.0.1','内网IP','Chrome 11','Windows 10','0','登录成功','2023-09-16 14:48:35'),(103,'admin','127.0.0.1','内网IP','Chrome 11','Windows 10','0','登录成功','2023-09-16 17:10:00'),(104,'admin','127.0.0.1','内网IP','Chrome 11','Windows 10','0','登录成功','2023-09-16 19:44:56'),(105,'admin','127.0.0.1','内网IP','Chrome 11','Windows 10','0','登录成功','2023-09-16 20:55:52'),(106,'admin','127.0.0.1','内网IP','Chrome 11','Windows 10','0','登录成功','2023-09-16 22:30:11'),(107,'admin','127.0.0.1','内网IP','Chrome 9','Windows 8','0','登录成功','2023-09-16 22:55:16'),(108,'admin','127.0.0.1','内网IP','Chrome 9','Windows 8','0','登录成功','2023-09-16 23:42:00'),(109,'admin','127.0.0.1','内网IP','Chrome 9','Windows 8','0','登录成功','2023-09-17 10:04:56'),(110,'admin','127.0.0.1','内网IP','Mobile Safari','iOS 11 (iPhone)','1','验证码错误','2023-09-17 13:16:09'),(111,'admin','127.0.0.1','内网IP','Mobile Safari','iOS 11 (iPhone)','0','登录成功','2023-09-17 13:16:13'),(112,'admin','127.0.0.1','内网IP','Mobile Safari','iOS 11 (iPhone)','0','登录成功','2023-09-17 14:52:01'),(113,'admin','127.0.0.1','内网IP','Mobile Safari','iOS 11 (iPhone)','0','登录成功','2023-09-17 17:07:35'),(114,'admin','127.0.0.1','内网IP','Mobile Safari','iOS 11 (iPhone)','0','退出成功','2023-09-17 18:08:07'),(115,'J01010625','127.0.0.1','内网IP','Mobile Safari','iOS 11 (iPhone)','0','登录成功','2023-09-17 18:08:50'),(116,'admin','127.0.0.1','内网IP','Mobile Safari','iOS 11 (iPhone)','1','验证码已失效','2023-09-17 19:21:52'),(117,'admin','127.0.0.1','内网IP','Mobile Safari','iOS 11 (iPhone)','0','登录成功','2023-09-17 19:21:55'),(118,'admin','127.0.0.1','内网IP','Mobile Safari','iOS 11 (iPhone)','0','退出成功','2023-09-17 19:38:41'),(119,'J01010625','127.0.0.1','内网IP','Mobile Safari','iOS 11 (iPhone)','0','登录成功','2023-09-17 19:38:48'),(120,'J01010625','127.0.0.1','内网IP','Mobile Safari','iOS 11 (iPhone)','0','退出成功','2023-09-17 19:40:32'),(121,'admin','127.0.0.1','内网IP','Mobile Safari','iOS 11 (iPhone)','1','验证码错误','2023-09-17 19:40:36'),(122,'admin','127.0.0.1','内网IP','Mobile Safari','iOS 11 (iPhone)','0','登录成功','2023-09-17 19:40:40'),(123,'admin','127.0.0.1','内网IP','Chrome 11','Windows 10','0','登录成功','2023-09-17 19:58:11'),(124,'admin','127.0.0.1','内网IP','Mobile Safari','iOS 11 (iPhone)','0','退出成功','2023-09-17 20:06:39'),(125,'J01010625','127.0.0.1','内网IP','Mobile Safari','iOS 11 (iPhone)','0','登录成功','2023-09-17 20:06:54'),(126,'J01010625','127.0.0.1','内网IP','Mobile Safari','iOS 11 (iPhone)','0','退出成功','2023-09-17 20:07:42'),(127,'admin','127.0.0.1','内网IP','Mobile Safari','iOS 11 (iPhone)','0','登录成功','2023-09-17 20:07:46'),(128,'admin','192.168.101.16','内网IP','Chrome Mobile','Android 1.x','0','登录成功','2023-09-17 20:22:46'),(129,'admin','192.168.101.16','内网IP','Chrome Mobile','Android 1.x','0','登录成功','2023-09-17 20:23:10'),(130,'admin','192.168.101.27','内网IP','Chrome 9','Windows 8','0','登录成功','2023-09-17 20:30:12'),(131,'admin','192.168.101.16','内网IP','Chrome Mobile','Android 1.x','0','登录成功','2023-09-17 20:30:36'),(132,'admin','192.168.101.16','内网IP','Chrome Mobile','Android 1.x','0','登录成功','2023-09-17 20:55:07'),(133,'admin','192.168.101.27','内网IP','Chrome 9','Windows 8','0','登录成功','2023-09-17 20:55:16'),(134,'admin','192.168.101.16','内网IP','Chrome Mobile','Android 1.x','0','登录成功','2023-09-17 20:59:12'),(135,'admin','192.168.101.16','内网IP','Chrome Mobile','Android 1.x','0','登录成功','2023-09-17 21:01:38'),(136,'admin','192.168.101.16','内网IP','Chrome Mobile','Android 1.x','0','登录成功','2023-09-17 21:01:59'),(137,'admin','192.168.101.16','内网IP','Chrome Mobile','Android 1.x','0','登录成功','2023-09-17 21:03:21');
/*!40000 ALTER TABLE `sys_logininfor` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_menu`
--

DROP TABLE IF EXISTS `sys_menu`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sys_menu` (
  `menu_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '菜单ID',
  `menu_name` varchar(50) NOT NULL COMMENT '菜单名称',
  `parent_id` bigint(20) DEFAULT '0' COMMENT '父菜单ID',
  `order_num` int(4) DEFAULT '0' COMMENT '显示顺序',
  `path` varchar(200) DEFAULT '' COMMENT '路由地址',
  `component` varchar(255) DEFAULT NULL COMMENT '组件路径',
  `query` varchar(255) DEFAULT NULL COMMENT '路由参数',
  `is_frame` int(1) DEFAULT '1' COMMENT '是否为外链（0是 1否）',
  `is_cache` int(1) DEFAULT '0' COMMENT '是否缓存（0缓存 1不缓存）',
  `menu_type` char(1) DEFAULT '' COMMENT '菜单类型（M目录 C菜单 F按钮）',
  `visible` char(1) DEFAULT '0' COMMENT '菜单状态（0显示 1隐藏）',
  `status` char(1) DEFAULT '0' COMMENT '菜单状态（0正常 1停用）',
  `perms` varchar(100) DEFAULT NULL COMMENT '权限标识',
  `icon` varchar(100) DEFAULT '#' COMMENT '菜单图标',
  `create_by` varchar(64) DEFAULT '' COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) DEFAULT '' COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) DEFAULT '' COMMENT '备注',
  PRIMARY KEY (`menu_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2031 DEFAULT CHARSET=utf8mb4 COMMENT='菜单权限表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_menu`
--

LOCK TABLES `sys_menu` WRITE;
/*!40000 ALTER TABLE `sys_menu` DISABLE KEYS */;
INSERT INTO `sys_menu` VALUES (1,'系统管理',0,1,'system',NULL,'',1,0,'M','0','0','','system','admin','2023-09-16 10:17:55','',NULL,'系统管理目录'),(2,'系统监控',0,2,'monitor',NULL,'',1,0,'M','0','0','','monitor','admin','2023-09-16 10:17:55','',NULL,'系统监控目录'),(3,'系统工具',0,3,'tool',NULL,'',1,0,'M','0','0','','tool','admin','2023-09-16 10:17:55','',NULL,'系统工具目录'),(4,'若依官网',0,4,'http://ruoyi.vip',NULL,'',0,0,'M','0','0','','guide','admin','2023-09-16 10:17:55','',NULL,'若依官网地址'),(100,'用户管理',1,1,'user','system/user/index','',1,0,'C','0','0','system:user:list','user','admin','2023-09-16 10:17:55','',NULL,'用户管理菜单'),(101,'角色管理',1,2,'role','system/role/index','',1,0,'C','0','0','system:role:list','peoples','admin','2023-09-16 10:17:55','',NULL,'角色管理菜单'),(102,'菜单管理',1,3,'menu','system/menu/index','',1,0,'C','0','0','system:menu:list','tree-table','admin','2023-09-16 10:17:55','',NULL,'菜单管理菜单'),(103,'部门管理',1,4,'dept','system/dept/index','',1,0,'C','0','0','system:dept:list','tree','admin','2023-09-16 10:17:55','',NULL,'部门管理菜单'),(104,'岗位管理',1,5,'post','system/post/index','',1,0,'C','0','0','system:post:list','post','admin','2023-09-16 10:17:55','',NULL,'岗位管理菜单'),(105,'字典管理',1,6,'dict','system/dict/index','',1,0,'C','0','0','system:dict:list','dict','admin','2023-09-16 10:17:55','',NULL,'字典管理菜单'),(106,'参数设置',1,7,'config','system/config/index','',1,0,'C','0','0','system:config:list','edit','admin','2023-09-16 10:17:55','',NULL,'参数设置菜单'),(107,'通知公告',1,8,'notice','system/notice/index','',1,0,'C','0','0','system:notice:list','message','admin','2023-09-16 10:17:55','',NULL,'通知公告菜单'),(108,'日志管理',1,9,'log','','',1,0,'M','0','0','','log','admin','2023-09-16 10:17:55','',NULL,'日志管理菜单'),(109,'在线用户',2,1,'online','monitor/online/index','',1,0,'C','0','0','monitor:online:list','online','admin','2023-09-16 10:17:55','',NULL,'在线用户菜单'),(110,'定时任务',2,2,'job','monitor/job/index','',1,0,'C','0','0','monitor:job:list','job','admin','2023-09-16 10:17:55','',NULL,'定时任务菜单'),(111,'数据监控',2,3,'druid','monitor/druid/index','',1,0,'C','0','0','monitor:druid:list','druid','admin','2023-09-16 10:17:55','',NULL,'数据监控菜单'),(112,'服务监控',2,4,'server','monitor/server/index','',1,0,'C','0','0','monitor:server:list','server','admin','2023-09-16 10:17:55','',NULL,'服务监控菜单'),(113,'缓存监控',2,5,'cache','monitor/cache/index','',1,0,'C','0','0','monitor:cache:list','redis','admin','2023-09-16 10:17:55','',NULL,'缓存监控菜单'),(114,'缓存列表',2,6,'cacheList','monitor/cache/list','',1,0,'C','0','0','monitor:cache:list','redis-list','admin','2023-09-16 10:17:55','',NULL,'缓存列表菜单'),(115,'表单构建',3,1,'build','tool/build/index','',1,0,'C','0','0','tool:build:list','build','admin','2023-09-16 10:17:55','',NULL,'表单构建菜单'),(116,'代码生成',3,2,'gen','tool/gen/index','',1,0,'C','0','0','tool:gen:list','code','admin','2023-09-16 10:17:55','',NULL,'代码生成菜单'),(117,'系统接口',3,3,'swagger','tool/swagger/index','',1,0,'C','0','0','tool:swagger:list','swagger','admin','2023-09-16 10:17:55','',NULL,'系统接口菜单'),(500,'操作日志',108,1,'operlog','monitor/operlog/index','',1,0,'C','0','0','monitor:operlog:list','form','admin','2023-09-16 10:17:55','',NULL,'操作日志菜单'),(501,'登录日志',108,2,'logininfor','monitor/logininfor/index','',1,0,'C','0','0','monitor:logininfor:list','logininfor','admin','2023-09-16 10:17:55','',NULL,'登录日志菜单'),(1000,'用户查询',100,1,'','','',1,0,'F','0','0','system:user:query','#','admin','2023-09-16 10:17:55','',NULL,''),(1001,'用户新增',100,2,'','','',1,0,'F','0','0','system:user:add','#','admin','2023-09-16 10:17:55','',NULL,''),(1002,'用户修改',100,3,'','','',1,0,'F','0','0','system:user:edit','#','admin','2023-09-16 10:17:55','',NULL,''),(1003,'用户删除',100,4,'','','',1,0,'F','0','0','system:user:remove','#','admin','2023-09-16 10:17:55','',NULL,''),(1004,'用户导出',100,5,'','','',1,0,'F','0','0','system:user:export','#','admin','2023-09-16 10:17:55','',NULL,''),(1005,'用户导入',100,6,'','','',1,0,'F','0','0','system:user:import','#','admin','2023-09-16 10:17:55','',NULL,''),(1006,'重置密码',100,7,'','','',1,0,'F','0','0','system:user:resetPwd','#','admin','2023-09-16 10:17:55','',NULL,''),(1007,'角色查询',101,1,'','','',1,0,'F','0','0','system:role:query','#','admin','2023-09-16 10:17:55','',NULL,''),(1008,'角色新增',101,2,'','','',1,0,'F','0','0','system:role:add','#','admin','2023-09-16 10:17:55','',NULL,''),(1009,'角色修改',101,3,'','','',1,0,'F','0','0','system:role:edit','#','admin','2023-09-16 10:17:55','',NULL,''),(1010,'角色删除',101,4,'','','',1,0,'F','0','0','system:role:remove','#','admin','2023-09-16 10:17:55','',NULL,''),(1011,'角色导出',101,5,'','','',1,0,'F','0','0','system:role:export','#','admin','2023-09-16 10:17:55','',NULL,''),(1012,'菜单查询',102,1,'','','',1,0,'F','0','0','system:menu:query','#','admin','2023-09-16 10:17:55','',NULL,''),(1013,'菜单新增',102,2,'','','',1,0,'F','0','0','system:menu:add','#','admin','2023-09-16 10:17:55','',NULL,''),(1014,'菜单修改',102,3,'','','',1,0,'F','0','0','system:menu:edit','#','admin','2023-09-16 10:17:55','',NULL,''),(1015,'菜单删除',102,4,'','','',1,0,'F','0','0','system:menu:remove','#','admin','2023-09-16 10:17:55','',NULL,''),(1016,'部门查询',103,1,'','','',1,0,'F','0','0','system:dept:query','#','admin','2023-09-16 10:17:55','',NULL,''),(1017,'部门新增',103,2,'','','',1,0,'F','0','0','system:dept:add','#','admin','2023-09-16 10:17:55','',NULL,''),(1018,'部门修改',103,3,'','','',1,0,'F','0','0','system:dept:edit','#','admin','2023-09-16 10:17:55','',NULL,''),(1019,'部门删除',103,4,'','','',1,0,'F','0','0','system:dept:remove','#','admin','2023-09-16 10:17:55','',NULL,''),(1020,'岗位查询',104,1,'','','',1,0,'F','0','0','system:post:query','#','admin','2023-09-16 10:17:55','',NULL,''),(1021,'岗位新增',104,2,'','','',1,0,'F','0','0','system:post:add','#','admin','2023-09-16 10:17:55','',NULL,''),(1022,'岗位修改',104,3,'','','',1,0,'F','0','0','system:post:edit','#','admin','2023-09-16 10:17:55','',NULL,''),(1023,'岗位删除',104,4,'','','',1,0,'F','0','0','system:post:remove','#','admin','2023-09-16 10:17:55','',NULL,''),(1024,'岗位导出',104,5,'','','',1,0,'F','0','0','system:post:export','#','admin','2023-09-16 10:17:55','',NULL,''),(1025,'字典查询',105,1,'#','','',1,0,'F','0','0','system:dict:query','#','admin','2023-09-16 10:17:55','',NULL,''),(1026,'字典新增',105,2,'#','','',1,0,'F','0','0','system:dict:add','#','admin','2023-09-16 10:17:55','',NULL,''),(1027,'字典修改',105,3,'#','','',1,0,'F','0','0','system:dict:edit','#','admin','2023-09-16 10:17:55','',NULL,''),(1028,'字典删除',105,4,'#','','',1,0,'F','0','0','system:dict:remove','#','admin','2023-09-16 10:17:55','',NULL,''),(1029,'字典导出',105,5,'#','','',1,0,'F','0','0','system:dict:export','#','admin','2023-09-16 10:17:55','',NULL,''),(1030,'参数查询',106,1,'#','','',1,0,'F','0','0','system:config:query','#','admin','2023-09-16 10:17:55','',NULL,''),(1031,'参数新增',106,2,'#','','',1,0,'F','0','0','system:config:add','#','admin','2023-09-16 10:17:55','',NULL,''),(1032,'参数修改',106,3,'#','','',1,0,'F','0','0','system:config:edit','#','admin','2023-09-16 10:17:55','',NULL,''),(1033,'参数删除',106,4,'#','','',1,0,'F','0','0','system:config:remove','#','admin','2023-09-16 10:17:55','',NULL,''),(1034,'参数导出',106,5,'#','','',1,0,'F','0','0','system:config:export','#','admin','2023-09-16 10:17:55','',NULL,''),(1035,'公告查询',107,1,'#','','',1,0,'F','0','0','system:notice:query','#','admin','2023-09-16 10:17:55','',NULL,''),(1036,'公告新增',107,2,'#','','',1,0,'F','0','0','system:notice:add','#','admin','2023-09-16 10:17:55','',NULL,''),(1037,'公告修改',107,3,'#','','',1,0,'F','0','0','system:notice:edit','#','admin','2023-09-16 10:17:55','',NULL,''),(1038,'公告删除',107,4,'#','','',1,0,'F','0','0','system:notice:remove','#','admin','2023-09-16 10:17:55','',NULL,''),(1039,'操作查询',500,1,'#','','',1,0,'F','0','0','monitor:operlog:query','#','admin','2023-09-16 10:17:55','',NULL,''),(1040,'操作删除',500,2,'#','','',1,0,'F','0','0','monitor:operlog:remove','#','admin','2023-09-16 10:17:55','',NULL,''),(1041,'日志导出',500,3,'#','','',1,0,'F','0','0','monitor:operlog:export','#','admin','2023-09-16 10:17:55','',NULL,''),(1042,'登录查询',501,1,'#','','',1,0,'F','0','0','monitor:logininfor:query','#','admin','2023-09-16 10:17:55','',NULL,''),(1043,'登录删除',501,2,'#','','',1,0,'F','0','0','monitor:logininfor:remove','#','admin','2023-09-16 10:17:55','',NULL,''),(1044,'日志导出',501,3,'#','','',1,0,'F','0','0','monitor:logininfor:export','#','admin','2023-09-16 10:17:55','',NULL,''),(1045,'账户解锁',501,4,'#','','',1,0,'F','0','0','monitor:logininfor:unlock','#','admin','2023-09-16 10:17:55','',NULL,''),(1046,'在线查询',109,1,'#','','',1,0,'F','0','0','monitor:online:query','#','admin','2023-09-16 10:17:55','',NULL,''),(1047,'批量强退',109,2,'#','','',1,0,'F','0','0','monitor:online:batchLogout','#','admin','2023-09-16 10:17:55','',NULL,''),(1048,'单条强退',109,3,'#','','',1,0,'F','0','0','monitor:online:forceLogout','#','admin','2023-09-16 10:17:55','',NULL,''),(1049,'任务查询',110,1,'#','','',1,0,'F','0','0','monitor:job:query','#','admin','2023-09-16 10:17:55','',NULL,''),(1050,'任务新增',110,2,'#','','',1,0,'F','0','0','monitor:job:add','#','admin','2023-09-16 10:17:55','',NULL,''),(1051,'任务修改',110,3,'#','','',1,0,'F','0','0','monitor:job:edit','#','admin','2023-09-16 10:17:55','',NULL,''),(1052,'任务删除',110,4,'#','','',1,0,'F','0','0','monitor:job:remove','#','admin','2023-09-16 10:17:55','',NULL,''),(1053,'状态修改',110,5,'#','','',1,0,'F','0','0','monitor:job:changeStatus','#','admin','2023-09-16 10:17:55','',NULL,''),(1054,'任务导出',110,6,'#','','',1,0,'F','0','0','monitor:job:export','#','admin','2023-09-16 10:17:55','',NULL,''),(1055,'生成查询',116,1,'#','','',1,0,'F','0','0','tool:gen:query','#','admin','2023-09-16 10:17:55','',NULL,''),(1056,'生成修改',116,2,'#','','',1,0,'F','0','0','tool:gen:edit','#','admin','2023-09-16 10:17:55','',NULL,''),(1057,'生成删除',116,3,'#','','',1,0,'F','0','0','tool:gen:remove','#','admin','2023-09-16 10:17:55','',NULL,''),(1058,'导入代码',116,4,'#','','',1,0,'F','0','0','tool:gen:import','#','admin','2023-09-16 10:17:55','',NULL,''),(1059,'预览代码',116,5,'#','','',1,0,'F','0','0','tool:gen:preview','#','admin','2023-09-16 10:17:55','',NULL,''),(1060,'生成代码',116,6,'#','','',1,0,'F','0','0','tool:gen:code','#','admin','2023-09-16 10:17:55','',NULL,''),(2000,'校车',2030,1,'bus','system/bus/index',NULL,1,0,'C','0','0','system:bus:list','#','admin','2023-09-16 11:17:08','admin','2023-09-16 11:42:44','校车菜单'),(2001,'校车查询',2000,1,'#','',NULL,1,0,'F','0','0','system:bus:query','#','admin','2023-09-16 11:17:08','',NULL,''),(2002,'校车新增',2000,2,'#','',NULL,1,0,'F','0','0','system:bus:add','#','admin','2023-09-16 11:17:08','',NULL,''),(2003,'校车修改',2000,3,'#','',NULL,1,0,'F','0','0','system:bus:edit','#','admin','2023-09-16 11:17:08','',NULL,''),(2004,'校车删除',2000,4,'#','',NULL,1,0,'F','0','0','system:bus:remove','#','admin','2023-09-16 11:17:08','',NULL,''),(2005,'校车导出',2000,5,'#','',NULL,1,0,'F','0','0','system:bus:export','#','admin','2023-09-16 11:17:08','',NULL,''),(2006,'站点',2030,1,'site','system/site/index',NULL,1,0,'C','0','0','system:site:list','#','admin','2023-09-16 11:27:34','admin','2023-09-16 11:42:47','站点菜单'),(2007,'站点查询',2006,1,'#','',NULL,1,0,'F','0','0','system:site:query','#','admin','2023-09-16 11:27:34','',NULL,''),(2008,'站点新增',2006,2,'#','',NULL,1,0,'F','0','0','system:site:add','#','admin','2023-09-16 11:27:34','',NULL,''),(2009,'站点修改',2006,3,'#','',NULL,1,0,'F','0','0','system:site:edit','#','admin','2023-09-16 11:27:34','',NULL,''),(2010,'站点删除',2006,4,'#','',NULL,1,0,'F','0','0','system:site:remove','#','admin','2023-09-16 11:27:34','',NULL,''),(2011,'站点导出',2006,5,'#','',NULL,1,0,'F','0','0','system:site:export','#','admin','2023-09-16 11:27:34','',NULL,''),(2012,'路线',2030,1,'route','system/route/index',NULL,1,0,'C','0','0','system:route:list','#','admin','2023-09-16 11:30:42','admin','2023-09-16 11:42:51','路线菜单'),(2013,'路线查询',2012,1,'#','',NULL,1,0,'F','0','0','system:route:query','#','admin','2023-09-16 11:30:42','',NULL,''),(2014,'路线新增',2012,2,'#','',NULL,1,0,'F','0','0','system:route:add','#','admin','2023-09-16 11:30:42','',NULL,''),(2015,'路线修改',2012,3,'#','',NULL,1,0,'F','0','0','system:route:edit','#','admin','2023-09-16 11:30:42','',NULL,''),(2016,'路线删除',2012,4,'#','',NULL,1,0,'F','0','0','system:route:remove','#','admin','2023-09-16 11:30:42','',NULL,''),(2017,'路线导出',2012,5,'#','',NULL,1,0,'F','0','0','system:route:export','#','admin','2023-09-16 11:30:42','',NULL,''),(2018,'校车行驶记录',2030,1,'record','system/record/index',NULL,1,0,'C','0','0','system:record:list','#','admin','2023-09-16 11:32:43','admin','2023-09-16 11:42:56','校车行驶记录菜单'),(2019,'校车行驶记录查询',2018,1,'#','',NULL,1,0,'F','0','0','system:record:query','#','admin','2023-09-16 11:32:43','',NULL,''),(2020,'校车行驶记录新增',2018,2,'#','',NULL,1,0,'F','0','0','system:record:add','#','admin','2023-09-16 11:32:43','',NULL,''),(2021,'校车行驶记录修改',2018,3,'#','',NULL,1,0,'F','0','0','system:record:edit','#','admin','2023-09-16 11:32:43','',NULL,''),(2022,'校车行驶记录删除',2018,4,'#','',NULL,1,0,'F','0','0','system:record:remove','#','admin','2023-09-16 11:32:43','',NULL,''),(2023,'校车行驶记录导出',2018,5,'#','',NULL,1,0,'F','0','0','system:record:export','#','admin','2023-09-16 11:32:43','',NULL,''),(2024,'乘车预约记录',2030,1,'reservation','system/reservation/index',NULL,1,0,'C','0','0','system:reservation:list','#','admin','2023-09-16 11:38:41','admin','2023-09-16 11:43:00','乘车预约记录菜单'),(2025,'乘车预约记录查询',2024,1,'#','',NULL,1,0,'F','0','0','system:reservation:query','#','admin','2023-09-16 11:38:41','',NULL,''),(2026,'乘车预约记录新增',2024,2,'#','',NULL,1,0,'F','0','0','system:reservation:add','#','admin','2023-09-16 11:38:41','',NULL,''),(2027,'乘车预约记录修改',2024,3,'#','',NULL,1,0,'F','0','0','system:reservation:edit','#','admin','2023-09-16 11:38:41','',NULL,''),(2028,'乘车预约记录删除',2024,4,'#','',NULL,1,0,'F','0','0','system:reservation:remove','#','admin','2023-09-16 11:38:41','',NULL,''),(2029,'乘车预约记录导出',2024,5,'#','',NULL,1,0,'F','0','0','system:reservation:export','#','admin','2023-09-16 11:38:41','',NULL,''),(2030,'校车管理',0,1,'bus',NULL,NULL,1,0,'M','0','0','','cascader','admin','2023-09-16 11:42:22','admin','2023-09-16 11:42:36','');
/*!40000 ALTER TABLE `sys_menu` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_notice`
--

DROP TABLE IF EXISTS `sys_notice`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sys_notice` (
  `notice_id` int(4) NOT NULL AUTO_INCREMENT COMMENT '公告ID',
  `notice_title` varchar(50) NOT NULL COMMENT '公告标题',
  `notice_type` char(1) NOT NULL COMMENT '公告类型（1通知 2公告）',
  `notice_content` longblob COMMENT '公告内容',
  `status` char(1) DEFAULT '0' COMMENT '公告状态（0正常 1关闭）',
  `create_by` varchar(64) DEFAULT '' COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) DEFAULT '' COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(255) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`notice_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COMMENT='通知公告表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_notice`
--

LOCK TABLES `sys_notice` WRITE;
/*!40000 ALTER TABLE `sys_notice` DISABLE KEYS */;
INSERT INTO `sys_notice` VALUES (1,'温馨提醒：2018-07-01 若依新版本发布啦','2',_binary '新版本内容','0','admin','2023-09-16 10:17:55','',NULL,'管理员'),(2,'维护通知：2018-07-01 若依系统凌晨维护','1',_binary '维护内容','0','admin','2023-09-16 10:17:55','',NULL,'管理员');
/*!40000 ALTER TABLE `sys_notice` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_oper_log`
--

DROP TABLE IF EXISTS `sys_oper_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sys_oper_log` (
  `oper_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '日志主键',
  `title` varchar(50) DEFAULT '' COMMENT '模块标题',
  `business_type` int(2) DEFAULT '0' COMMENT '业务类型（0其它 1新增 2修改 3删除）',
  `method` varchar(100) DEFAULT '' COMMENT '方法名称',
  `request_method` varchar(10) DEFAULT '' COMMENT '请求方式',
  `operator_type` int(1) DEFAULT '0' COMMENT '操作类别（0其它 1后台用户 2手机端用户）',
  `oper_name` varchar(50) DEFAULT '' COMMENT '操作人员',
  `dept_name` varchar(50) DEFAULT '' COMMENT '部门名称',
  `oper_url` varchar(255) DEFAULT '' COMMENT '请求URL',
  `oper_ip` varchar(128) DEFAULT '' COMMENT '主机地址',
  `oper_location` varchar(255) DEFAULT '' COMMENT '操作地点',
  `oper_param` varchar(2000) DEFAULT '' COMMENT '请求参数',
  `json_result` varchar(2000) DEFAULT '' COMMENT '返回参数',
  `status` int(1) DEFAULT '0' COMMENT '操作状态（0正常 1异常）',
  `error_msg` varchar(2000) DEFAULT '' COMMENT '错误消息',
  `oper_time` datetime DEFAULT NULL COMMENT '操作时间',
  `cost_time` bigint(20) DEFAULT '0' COMMENT '消耗时间',
  PRIMARY KEY (`oper_id`),
  KEY `idx_sys_oper_log_bt` (`business_type`),
  KEY `idx_sys_oper_log_s` (`status`),
  KEY `idx_sys_oper_log_ot` (`oper_time`)
) ENGINE=InnoDB AUTO_INCREMENT=175 DEFAULT CHARSET=utf8mb4 COMMENT='操作日志记录';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_oper_log`
--

LOCK TABLES `sys_oper_log` WRITE;
/*!40000 ALTER TABLE `sys_oper_log` DISABLE KEYS */;
INSERT INTO `sys_oper_log` VALUES (100,'代码生成',6,'com.ruoyi.generator.controller.GenController.importTableSave()','POST',1,'admin',NULL,'/tool/gen/importTable','127.0.0.1','内网IP','{\"tables\":\"bus_bus\"}','{\"msg\":\"操作成功\",\"code\":200}',0,NULL,'2023-09-16 11:11:02',62),(101,'字典类型',1,'com.ruoyi.web.controller.system.SysDictTypeController.add()','POST',1,'admin',NULL,'/system/dict/type','127.0.0.1','内网IP','{\"createBy\":\"admin\",\"dictName\":\"数据状态\",\"dictType\":\"sys_data_status\",\"params\":{},\"status\":\"0\"}','{\"msg\":\"操作成功\",\"code\":200}',0,NULL,'2023-09-16 11:13:17',7),(102,'字典数据',1,'com.ruoyi.web.controller.system.SysDictDataController.add()','POST',1,'admin',NULL,'/system/dict/data','127.0.0.1','内网IP','{\"createBy\":\"admin\",\"default\":false,\"dictLabel\":\"正常\",\"dictSort\":0,\"dictType\":\"sys_data_status\",\"dictValue\":\"0\",\"listClass\":\"default\",\"params\":{},\"status\":\"0\"}','{\"msg\":\"操作成功\",\"code\":200}',0,NULL,'2023-09-16 11:13:35',7),(103,'字典数据',1,'com.ruoyi.web.controller.system.SysDictDataController.add()','POST',1,'admin',NULL,'/system/dict/data','127.0.0.1','内网IP','{\"createBy\":\"admin\",\"default\":false,\"dictLabel\":\"停用\",\"dictSort\":1,\"dictType\":\"sys_data_status\",\"dictValue\":\"1\",\"listClass\":\"warning\",\"params\":{},\"status\":\"0\"}','{\"msg\":\"操作成功\",\"code\":200}',0,NULL,'2023-09-16 11:13:47',9),(104,'代码生成',2,'com.ruoyi.generator.controller.GenController.editSave()','PUT',1,'admin',NULL,'/tool/gen','127.0.0.1','内网IP','{\"businessName\":\"bus\",\"className\":\"BusBus\",\"columns\":[{\"capJavaField\":\"Id\",\"columnComment\":\"ID\",\"columnId\":1,\"columnName\":\"id\",\"columnType\":\"bigint(20)\",\"createBy\":\"admin\",\"createTime\":\"2023-09-16 11:11:02\",\"dictType\":\"\",\"edit\":false,\"htmlType\":\"input\",\"increment\":true,\"insert\":true,\"isIncrement\":\"1\",\"isInsert\":\"1\",\"isPk\":\"1\",\"javaField\":\"id\",\"javaType\":\"Long\",\"list\":false,\"params\":{},\"pk\":true,\"query\":false,\"queryType\":\"EQ\",\"required\":false,\"sort\":1,\"superColumn\":false,\"tableId\":1,\"updateBy\":\"\",\"usableColumn\":false},{\"capJavaField\":\"BusName\",\"columnComment\":\"校车名\",\"columnId\":2,\"columnName\":\"bus_name\",\"columnType\":\"varchar(64)\",\"createBy\":\"admin\",\"createTime\":\"2023-09-16 11:11:02\",\"dictType\":\"\",\"edit\":true,\"htmlType\":\"input\",\"increment\":false,\"insert\":true,\"isEdit\":\"1\",\"isIncrement\":\"0\",\"isInsert\":\"1\",\"isList\":\"1\",\"isPk\":\"0\",\"isQuery\":\"1\",\"javaField\":\"busName\",\"javaType\":\"String\",\"list\":true,\"params\":{},\"pk\":false,\"query\":true,\"queryType\":\"LIKE\",\"required\":false,\"sort\":2,\"superColumn\":false,\"tableId\":1,\"updateBy\":\"\",\"usableColumn\":false},{\"capJavaField\":\"Plate\",\"columnComment\":\"车牌号\",\"columnId\":3,\"columnName\":\"plate\",\"columnType\":\"varchar(64)\",\"createBy\":\"admin\",\"createTime\":\"2023-09-16 11:11:02\",\"dictType\":\"\",\"edit\":true,\"htmlType\":\"input\",\"increment\":false,\"insert\":true,\"isEdit\":\"1\",\"isIncrement\":\"0\",\"isInsert\":\"1\",\"isList\":\"1\",\"isPk\":\"0\",\"isQuery\":\"1\",\"javaField\":\"plate\",\"javaType\":\"String\",\"list\":true,\"params\":{},\"pk\":false,\"query\":true,\"queryType\":\"EQ\",\"required\":false,\"sort\":3,\"superColumn\":false,\"tableId\":1,\"updateBy\":\"\",\"usableColumn\":false},{\"capJavaField\":\"Driver\",\"columnComment\":\"司机\",\"columnId\":4,\"columnName\":\"driver\",\"columnType\":\"bigint(20)\",\"createBy\":\"admin\",\"createTime\":\"2023-09-16 11:11:02\",\"dictType\":\"\",\"edit\":true,\"htmlType\":\"input\",\"increment\":false,\"insert\":true,\"isEdit\":\"1\",\"isIncrement\":\"0\",\"isInsert\":\"1\",\"isList\":\"1\",\"isPk\":\"0\",\"isQuery\":\"1\",\"javaField\":\"driver\",\"javaType\":\"Long\",\"list\":true,\"params\":{},\"pk\":false,\"query\":true,\"queryType\":\"EQ\",','{\"msg\":\"操作成功\",\"code\":200}',0,NULL,'2023-09-16 11:14:42',22),(105,'代码生成',8,'com.ruoyi.generator.controller.GenController.batchGenCode()','GET',1,'admin',NULL,'/tool/gen/batchGenCode','127.0.0.1','内网IP','{\"tables\":\"bus_bus\"}',NULL,0,NULL,'2023-09-16 11:14:58',75),(106,'校车',1,'com.ruoyi.system.controller.BusBusController.add()','POST',1,'admin',NULL,'/system/bus','127.0.0.1','内网IP','{\"busName\":\"校车11\",\"createTime\":\"2023-09-16 11:18:34\",\"driver\":1,\"id\":100,\"params\":{},\"plate\":\"贵A654321\",\"seating\":30,\"status\":\"0\"}','{\"msg\":\"操作成功\",\"code\":200}',0,NULL,'2023-09-16 11:18:34',15),(107,'代码生成',2,'com.ruoyi.generator.controller.GenController.editSave()','PUT',1,'admin',NULL,'/tool/gen','127.0.0.1','内网IP','{\"businessName\":\"bus\",\"className\":\"BusBus\",\"columns\":[{\"capJavaField\":\"Id\",\"columnComment\":\"ID\",\"columnId\":1,\"columnName\":\"id\",\"columnType\":\"bigint(20)\",\"createBy\":\"admin\",\"createTime\":\"2023-09-16 11:11:02\",\"dictType\":\"\",\"edit\":false,\"htmlType\":\"input\",\"increment\":true,\"insert\":true,\"isIncrement\":\"1\",\"isInsert\":\"1\",\"isPk\":\"1\",\"javaField\":\"id\",\"javaType\":\"Long\",\"list\":false,\"params\":{},\"pk\":true,\"query\":false,\"queryType\":\"EQ\",\"required\":false,\"sort\":1,\"superColumn\":false,\"tableId\":1,\"updateBy\":\"\",\"updateTime\":\"2023-09-16 11:14:42\",\"usableColumn\":false},{\"capJavaField\":\"BusName\",\"columnComment\":\"校车名\",\"columnId\":2,\"columnName\":\"bus_name\",\"columnType\":\"varchar(64)\",\"createBy\":\"admin\",\"createTime\":\"2023-09-16 11:11:02\",\"dictType\":\"\",\"edit\":true,\"htmlType\":\"input\",\"increment\":false,\"insert\":true,\"isEdit\":\"1\",\"isIncrement\":\"0\",\"isInsert\":\"1\",\"isList\":\"1\",\"isPk\":\"0\",\"isQuery\":\"1\",\"isRequired\":\"1\",\"javaField\":\"busName\",\"javaType\":\"String\",\"list\":true,\"params\":{},\"pk\":false,\"query\":true,\"queryType\":\"LIKE\",\"required\":true,\"sort\":2,\"superColumn\":false,\"tableId\":1,\"updateBy\":\"\",\"updateTime\":\"2023-09-16 11:14:42\",\"usableColumn\":false},{\"capJavaField\":\"Plate\",\"columnComment\":\"车牌号\",\"columnId\":3,\"columnName\":\"plate\",\"columnType\":\"varchar(64)\",\"createBy\":\"admin\",\"createTime\":\"2023-09-16 11:11:02\",\"dictType\":\"\",\"edit\":true,\"htmlType\":\"input\",\"increment\":false,\"insert\":true,\"isEdit\":\"1\",\"isIncrement\":\"0\",\"isInsert\":\"1\",\"isList\":\"1\",\"isPk\":\"0\",\"isQuery\":\"1\",\"isRequired\":\"1\",\"javaField\":\"plate\",\"javaType\":\"String\",\"list\":true,\"params\":{},\"pk\":false,\"query\":true,\"queryType\":\"EQ\",\"required\":true,\"sort\":3,\"superColumn\":false,\"tableId\":1,\"updateBy\":\"\",\"updateTime\":\"2023-09-16 11:14:42\",\"usableColumn\":false},{\"capJavaField\":\"Driver\",\"columnComment\":\"司机\",\"columnId\":4,\"columnName\":\"driver\",\"columnType\":\"bigint(20)\",\"createBy\":\"admin\",\"createTime\":\"2023-09-16 11:11:02\",\"dictType\":\"\",\"edit\":true,\"htmlType\":\"input\",\"increment\":false,\"insert\":true,\"isEdit\":\"1\",\"isIncrement\":\"0\",\"isInsert\":\"1\",\"isLi','{\"msg\":\"操作成功\",\"code\":200}',0,NULL,'2023-09-16 11:20:34',33),(108,'代码生成',8,'com.ruoyi.generator.controller.GenController.batchGenCode()','GET',1,'admin',NULL,'/tool/gen/batchGenCode','127.0.0.1','内网IP','{\"tables\":\"bus_bus\"}',NULL,0,NULL,'2023-09-16 11:21:02',46),(109,'代码生成',6,'com.ruoyi.generator.controller.GenController.importTableSave()','POST',1,'admin',NULL,'/tool/gen/importTable','127.0.0.1','内网IP','{\"tables\":\"bus_route_site\"}','{\"msg\":\"操作成功\",\"code\":200}',0,NULL,'2023-09-16 11:26:10',76),(110,'代码生成',2,'com.ruoyi.generator.controller.GenController.editSave()','PUT',1,'admin',NULL,'/tool/gen','127.0.0.1','内网IP','{\"businessName\":\"site\",\"className\":\"BusRouteSite\",\"columns\":[{\"capJavaField\":\"Id\",\"columnComment\":\"ID\",\"columnId\":12,\"columnName\":\"id\",\"columnType\":\"bigint(20)\",\"createBy\":\"admin\",\"createTime\":\"2023-09-16 11:26:10\",\"dictType\":\"\",\"edit\":false,\"htmlType\":\"input\",\"increment\":true,\"insert\":true,\"isIncrement\":\"1\",\"isInsert\":\"1\",\"isPk\":\"1\",\"javaField\":\"id\",\"javaType\":\"Long\",\"list\":false,\"params\":{},\"pk\":true,\"query\":false,\"queryType\":\"EQ\",\"required\":false,\"sort\":1,\"superColumn\":false,\"tableId\":2,\"updateBy\":\"\",\"usableColumn\":false},{\"capJavaField\":\"SiteName\",\"columnComment\":\"站点名\",\"columnId\":13,\"columnName\":\"site_name\",\"columnType\":\"varchar(64)\",\"createBy\":\"admin\",\"createTime\":\"2023-09-16 11:26:10\",\"dictType\":\"\",\"edit\":true,\"htmlType\":\"input\",\"increment\":false,\"insert\":true,\"isEdit\":\"1\",\"isIncrement\":\"0\",\"isInsert\":\"1\",\"isList\":\"1\",\"isPk\":\"0\",\"isQuery\":\"1\",\"isRequired\":\"1\",\"javaField\":\"siteName\",\"javaType\":\"String\",\"list\":true,\"params\":{},\"pk\":false,\"query\":true,\"queryType\":\"LIKE\",\"required\":true,\"sort\":2,\"superColumn\":false,\"tableId\":2,\"updateBy\":\"\",\"usableColumn\":false},{\"capJavaField\":\"Longitude\",\"columnComment\":\"经度\",\"columnId\":14,\"columnName\":\"longitude\",\"columnType\":\"varchar(64)\",\"createBy\":\"admin\",\"createTime\":\"2023-09-16 11:26:10\",\"dictType\":\"\",\"edit\":true,\"htmlType\":\"input\",\"increment\":false,\"insert\":true,\"isEdit\":\"1\",\"isIncrement\":\"0\",\"isInsert\":\"1\",\"isList\":\"1\",\"isPk\":\"0\",\"isQuery\":\"1\",\"isRequired\":\"1\",\"javaField\":\"longitude\",\"javaType\":\"String\",\"list\":true,\"params\":{},\"pk\":false,\"query\":true,\"queryType\":\"EQ\",\"required\":true,\"sort\":3,\"superColumn\":false,\"tableId\":2,\"updateBy\":\"\",\"usableColumn\":false},{\"capJavaField\":\"Latitude\",\"columnComment\":\"纬度\",\"columnId\":15,\"columnName\":\"latitude\",\"columnType\":\"varchar(64)\",\"createBy\":\"admin\",\"createTime\":\"2023-09-16 11:26:10\",\"dictType\":\"\",\"edit\":true,\"htmlType\":\"input\",\"increment\":false,\"insert\":true,\"isEdit\":\"1\",\"isIncrement\":\"0\",\"isInsert\":\"1\",\"isList\":\"1\",\"isPk\":\"0\",\"isQuery\":\"1\",\"isRequired\":\"1\",\"javaField\":\"latitude\",\"j','{\"msg\":\"操作成功\",\"code\":200}',0,NULL,'2023-09-16 11:27:02',30),(111,'代码生成',8,'com.ruoyi.generator.controller.GenController.batchGenCode()','GET',1,'admin',NULL,'/tool/gen/batchGenCode','127.0.0.1','内网IP','{\"tables\":\"bus_route_site\"}',NULL,0,NULL,'2023-09-16 11:27:05',100),(112,'代码生成',6,'com.ruoyi.generator.controller.GenController.importTableSave()','POST',1,'admin',NULL,'/tool/gen/importTable','127.0.0.1','内网IP','{\"tables\":\"bus_route\"}','{\"msg\":\"操作成功\",\"code\":200}',0,NULL,'2023-09-16 11:29:00',37),(113,'代码生成',2,'com.ruoyi.generator.controller.GenController.editSave()','PUT',1,'admin',NULL,'/tool/gen','127.0.0.1','内网IP','{\"businessName\":\"route\",\"className\":\"BusRoute\",\"columns\":[{\"capJavaField\":\"Id\",\"columnComment\":\"ID\",\"columnId\":23,\"columnName\":\"id\",\"columnType\":\"bigint(20)\",\"createBy\":\"admin\",\"createTime\":\"2023-09-16 11:29:00\",\"dictType\":\"\",\"edit\":false,\"htmlType\":\"input\",\"increment\":true,\"insert\":true,\"isIncrement\":\"1\",\"isInsert\":\"1\",\"isPk\":\"1\",\"javaField\":\"id\",\"javaType\":\"Long\",\"list\":false,\"params\":{},\"pk\":true,\"query\":false,\"queryType\":\"EQ\",\"required\":false,\"sort\":1,\"superColumn\":false,\"tableId\":3,\"updateBy\":\"\",\"usableColumn\":false},{\"capJavaField\":\"RouteName\",\"columnComment\":\"路线名\",\"columnId\":24,\"columnName\":\"route_name\",\"columnType\":\"varchar(64)\",\"createBy\":\"admin\",\"createTime\":\"2023-09-16 11:29:00\",\"dictType\":\"\",\"edit\":true,\"htmlType\":\"input\",\"increment\":false,\"insert\":true,\"isEdit\":\"1\",\"isIncrement\":\"0\",\"isInsert\":\"1\",\"isList\":\"1\",\"isPk\":\"0\",\"isQuery\":\"1\",\"isRequired\":\"1\",\"javaField\":\"routeName\",\"javaType\":\"String\",\"list\":true,\"params\":{},\"pk\":false,\"query\":true,\"queryType\":\"LIKE\",\"required\":true,\"sort\":2,\"superColumn\":false,\"tableId\":3,\"updateBy\":\"\",\"usableColumn\":false},{\"capJavaField\":\"StartSiteId\",\"columnComment\":\"起点\",\"columnId\":25,\"columnName\":\"start_site_id\",\"columnType\":\"bigint(20)\",\"createBy\":\"admin\",\"createTime\":\"2023-09-16 11:29:00\",\"dictType\":\"\",\"edit\":true,\"htmlType\":\"input\",\"increment\":false,\"insert\":true,\"isEdit\":\"1\",\"isIncrement\":\"0\",\"isInsert\":\"1\",\"isList\":\"1\",\"isPk\":\"0\",\"isQuery\":\"1\",\"isRequired\":\"1\",\"javaField\":\"startSiteId\",\"javaType\":\"Long\",\"list\":true,\"params\":{},\"pk\":false,\"query\":true,\"queryType\":\"EQ\",\"required\":true,\"sort\":3,\"superColumn\":false,\"tableId\":3,\"updateBy\":\"\",\"usableColumn\":false},{\"capJavaField\":\"EndSiteId\",\"columnComment\":\"终点\",\"columnId\":26,\"columnName\":\"end_site_id\",\"columnType\":\"bigint(20)\",\"createBy\":\"admin\",\"createTime\":\"2023-09-16 11:29:00\",\"dictType\":\"\",\"edit\":true,\"htmlType\":\"input\",\"increment\":false,\"insert\":true,\"isEdit\":\"1\",\"isIncrement\":\"0\",\"isInsert\":\"1\",\"isList\":\"1\",\"isPk\":\"0\",\"isQuery\":\"1\",\"isRequired\":\"1\",\"javaField\":\"endS','{\"msg\":\"操作成功\",\"code\":200}',0,NULL,'2023-09-16 11:30:00',22),(114,'代码生成',8,'com.ruoyi.generator.controller.GenController.batchGenCode()','GET',1,'admin',NULL,'/tool/gen/batchGenCode','127.0.0.1','内网IP','{\"tables\":\"bus_route\"}',NULL,0,NULL,'2023-09-16 11:30:03',41),(115,'代码生成',6,'com.ruoyi.generator.controller.GenController.importTableSave()','POST',1,'admin',NULL,'/tool/gen/importTable','127.0.0.1','内网IP','{\"tables\":\"bus_driving_record\"}','{\"msg\":\"操作成功\",\"code\":200}',0,NULL,'2023-09-16 11:31:40',29),(116,'代码生成',2,'com.ruoyi.generator.controller.GenController.editSave()','PUT',1,'admin',NULL,'/tool/gen','127.0.0.1','内网IP','{\"businessName\":\"record\",\"className\":\"BusDrivingRecord\",\"columns\":[{\"capJavaField\":\"Id\",\"columnComment\":\"ID\",\"columnId\":35,\"columnName\":\"id\",\"columnType\":\"bigint(20)\",\"createBy\":\"admin\",\"createTime\":\"2023-09-16 11:31:40\",\"dictType\":\"\",\"edit\":false,\"htmlType\":\"input\",\"increment\":true,\"insert\":true,\"isIncrement\":\"1\",\"isInsert\":\"1\",\"isPk\":\"1\",\"javaField\":\"id\",\"javaType\":\"Long\",\"list\":false,\"params\":{},\"pk\":true,\"query\":false,\"queryType\":\"EQ\",\"required\":false,\"sort\":1,\"superColumn\":false,\"tableId\":4,\"updateBy\":\"\",\"usableColumn\":false},{\"capJavaField\":\"BusId\",\"columnComment\":\"校车\",\"columnId\":36,\"columnName\":\"bus_id\",\"columnType\":\"bigint(20)\",\"createBy\":\"admin\",\"createTime\":\"2023-09-16 11:31:40\",\"dictType\":\"\",\"edit\":true,\"htmlType\":\"input\",\"increment\":false,\"insert\":true,\"isEdit\":\"1\",\"isIncrement\":\"0\",\"isInsert\":\"1\",\"isList\":\"1\",\"isPk\":\"0\",\"isQuery\":\"1\",\"isRequired\":\"1\",\"javaField\":\"busId\",\"javaType\":\"Long\",\"list\":true,\"params\":{},\"pk\":false,\"query\":true,\"queryType\":\"EQ\",\"required\":true,\"sort\":2,\"superColumn\":false,\"tableId\":4,\"updateBy\":\"\",\"usableColumn\":false},{\"capJavaField\":\"Driver\",\"columnComment\":\"司机\",\"columnId\":37,\"columnName\":\"driver\",\"columnType\":\"bigint(20)\",\"createBy\":\"admin\",\"createTime\":\"2023-09-16 11:31:40\",\"dictType\":\"\",\"edit\":true,\"htmlType\":\"input\",\"increment\":false,\"insert\":true,\"isEdit\":\"1\",\"isIncrement\":\"0\",\"isInsert\":\"1\",\"isList\":\"1\",\"isPk\":\"0\",\"isQuery\":\"1\",\"isRequired\":\"1\",\"javaField\":\"driver\",\"javaType\":\"Long\",\"list\":true,\"params\":{},\"pk\":false,\"query\":true,\"queryType\":\"EQ\",\"required\":true,\"sort\":3,\"superColumn\":false,\"tableId\":4,\"updateBy\":\"\",\"usableColumn\":false},{\"capJavaField\":\"RouteId\",\"columnComment\":\"路线\",\"columnId\":38,\"columnName\":\"route_id\",\"columnType\":\"bigint(20)\",\"createBy\":\"admin\",\"createTime\":\"2023-09-16 11:31:40\",\"dictType\":\"\",\"edit\":true,\"htmlType\":\"input\",\"increment\":false,\"insert\":true,\"isEdit\":\"1\",\"isIncrement\":\"0\",\"isInsert\":\"1\",\"isList\":\"1\",\"isPk\":\"0\",\"isQuery\":\"1\",\"isRequired\":\"1\",\"javaField\":\"routeId\",\"javaType\":\"Long\",\"list\":t','{\"msg\":\"操作成功\",\"code\":200}',0,NULL,'2023-09-16 11:32:14',17),(117,'代码生成',8,'com.ruoyi.generator.controller.GenController.batchGenCode()','GET',1,'admin',NULL,'/tool/gen/batchGenCode','127.0.0.1','内网IP','{\"tables\":\"bus_driving_record\"}',NULL,0,NULL,'2023-09-16 11:32:16',40),(118,'代码生成',6,'com.ruoyi.generator.controller.GenController.importTableSave()','POST',1,'admin',NULL,'/tool/gen/importTable','127.0.0.1','内网IP','{\"tables\":\"bus_record_site\"}','{\"msg\":\"操作成功\",\"code\":200}',0,NULL,'2023-09-16 11:33:53',29),(119,'代码生成',2,'com.ruoyi.generator.controller.GenController.editSave()','PUT',1,'admin',NULL,'/tool/gen','127.0.0.1','内网IP','{\"businessName\":\"recordSite\",\"className\":\"BusRecordSite\",\"columns\":[{\"capJavaField\":\"Id\",\"columnComment\":\"ID\",\"columnId\":46,\"columnName\":\"id\",\"columnType\":\"bigint(20)\",\"createBy\":\"admin\",\"createTime\":\"2023-09-16 11:33:53\",\"dictType\":\"\",\"edit\":false,\"htmlType\":\"input\",\"increment\":true,\"insert\":true,\"isIncrement\":\"1\",\"isInsert\":\"1\",\"isPk\":\"1\",\"javaField\":\"id\",\"javaType\":\"Long\",\"list\":false,\"params\":{},\"pk\":true,\"query\":false,\"queryType\":\"EQ\",\"required\":false,\"sort\":1,\"superColumn\":false,\"tableId\":5,\"updateBy\":\"\",\"usableColumn\":false},{\"capJavaField\":\"RecordId\",\"columnComment\":\"行驶记录\",\"columnId\":47,\"columnName\":\"record_id\",\"columnType\":\"bigint(20)\",\"createBy\":\"admin\",\"createTime\":\"2023-09-16 11:33:53\",\"dictType\":\"\",\"edit\":true,\"htmlType\":\"input\",\"increment\":false,\"insert\":true,\"isEdit\":\"1\",\"isIncrement\":\"0\",\"isInsert\":\"1\",\"isList\":\"1\",\"isPk\":\"0\",\"isQuery\":\"1\",\"isRequired\":\"1\",\"javaField\":\"recordId\",\"javaType\":\"Long\",\"list\":true,\"params\":{},\"pk\":false,\"query\":true,\"queryType\":\"EQ\",\"required\":true,\"sort\":2,\"superColumn\":false,\"tableId\":5,\"updateBy\":\"\",\"usableColumn\":false},{\"capJavaField\":\"RouteId\",\"columnComment\":\"路线\",\"columnId\":48,\"columnName\":\"route_id\",\"columnType\":\"bigint(20)\",\"createBy\":\"admin\",\"createTime\":\"2023-09-16 11:33:53\",\"dictType\":\"\",\"edit\":true,\"htmlType\":\"input\",\"increment\":false,\"insert\":true,\"isEdit\":\"1\",\"isIncrement\":\"0\",\"isInsert\":\"1\",\"isList\":\"1\",\"isPk\":\"0\",\"isQuery\":\"1\",\"isRequired\":\"1\",\"javaField\":\"routeId\",\"javaType\":\"Long\",\"list\":true,\"params\":{},\"pk\":false,\"query\":true,\"queryType\":\"EQ\",\"required\":true,\"sort\":3,\"superColumn\":false,\"tableId\":5,\"updateBy\":\"\",\"usableColumn\":false},{\"capJavaField\":\"SiteId\",\"columnComment\":\"站点\",\"columnId\":49,\"columnName\":\"site_id\",\"columnType\":\"bigint(20)\",\"createBy\":\"admin\",\"createTime\":\"2023-09-16 11:33:53\",\"dictType\":\"\",\"edit\":true,\"htmlType\":\"input\",\"increment\":false,\"insert\":true,\"isEdit\":\"1\",\"isIncrement\":\"0\",\"isInsert\":\"1\",\"isList\":\"1\",\"isPk\":\"0\",\"isQuery\":\"1\",\"isRequired\":\"1\",\"javaField\":\"siteId\",\"javaType\":\"L','{\"msg\":\"操作成功\",\"code\":200}',0,NULL,'2023-09-16 11:34:58',16),(120,'代码生成',8,'com.ruoyi.generator.controller.GenController.batchGenCode()','GET',1,'admin',NULL,'/tool/gen/batchGenCode','127.0.0.1','内网IP','{\"tables\":\"bus_record_site\"}',NULL,0,NULL,'2023-09-16 11:35:02',45),(121,'字典类型',1,'com.ruoyi.web.controller.system.SysDictTypeController.add()','POST',1,'admin',NULL,'/system/dict/type','127.0.0.1','内网IP','{\"createBy\":\"admin\",\"dictName\":\"乘车状态\",\"dictType\":\"bus_diding_status\",\"params\":{},\"status\":\"0\"}','{\"msg\":\"操作成功\",\"code\":200}',0,NULL,'2023-09-16 11:36:13',6),(122,'字典数据',1,'com.ruoyi.web.controller.system.SysDictDataController.add()','POST',1,'admin',NULL,'/system/dict/data','127.0.0.1','内网IP','{\"createBy\":\"admin\",\"default\":false,\"dictLabel\":\"已预约\",\"dictSort\":0,\"dictType\":\"bus_diding_status\",\"dictValue\":\"0\",\"listClass\":\"default\",\"params\":{},\"status\":\"0\"}','{\"msg\":\"操作成功\",\"code\":200}',0,NULL,'2023-09-16 11:36:33',8),(123,'字典数据',1,'com.ruoyi.web.controller.system.SysDictDataController.add()','POST',1,'admin',NULL,'/system/dict/data','127.0.0.1','内网IP','{\"createBy\":\"admin\",\"default\":false,\"dictLabel\":\"已上车\",\"dictSort\":1,\"dictType\":\"bus_diding_status\",\"dictValue\":\"1\",\"listClass\":\"default\",\"params\":{},\"status\":\"0\"}','{\"msg\":\"操作成功\",\"code\":200}',0,NULL,'2023-09-16 11:36:44',9),(124,'字典数据',1,'com.ruoyi.web.controller.system.SysDictDataController.add()','POST',1,'admin',NULL,'/system/dict/data','127.0.0.1','内网IP','{\"createBy\":\"admin\",\"default\":false,\"dictLabel\":\"已下车\",\"dictSort\":2,\"dictType\":\"bus_diding_status\",\"dictValue\":\"2\",\"listClass\":\"default\",\"params\":{},\"status\":\"0\"}','{\"msg\":\"操作成功\",\"code\":200}',0,NULL,'2023-09-16 11:36:53',8),(125,'字典数据',1,'com.ruoyi.web.controller.system.SysDictDataController.add()','POST',1,'admin',NULL,'/system/dict/data','127.0.0.1','内网IP','{\"createBy\":\"admin\",\"default\":false,\"dictLabel\":\"已取消\",\"dictSort\":3,\"dictType\":\"bus_diding_status\",\"dictValue\":\"3\",\"listClass\":\"default\",\"params\":{},\"status\":\"0\"}','{\"msg\":\"操作成功\",\"code\":200}',0,NULL,'2023-09-16 11:37:02',9),(126,'代码生成',6,'com.ruoyi.generator.controller.GenController.importTableSave()','POST',1,'admin',NULL,'/tool/gen/importTable','127.0.0.1','内网IP','{\"tables\":\"bus_reservation\"}','{\"msg\":\"操作成功\",\"code\":200}',0,NULL,'2023-09-16 11:37:23',27),(127,'代码生成',2,'com.ruoyi.generator.controller.GenController.editSave()','PUT',1,'admin',NULL,'/tool/gen','127.0.0.1','内网IP','{\"businessName\":\"reservation\",\"className\":\"BusReservation\",\"columns\":[{\"capJavaField\":\"Id\",\"columnComment\":\"ID\",\"columnId\":59,\"columnName\":\"id\",\"columnType\":\"bigint(20)\",\"createBy\":\"admin\",\"createTime\":\"2023-09-16 11:37:23\",\"dictType\":\"\",\"edit\":false,\"htmlType\":\"input\",\"increment\":true,\"insert\":true,\"isIncrement\":\"1\",\"isInsert\":\"1\",\"isPk\":\"1\",\"javaField\":\"id\",\"javaType\":\"Long\",\"list\":false,\"params\":{},\"pk\":true,\"query\":false,\"queryType\":\"EQ\",\"required\":false,\"sort\":1,\"superColumn\":false,\"tableId\":6,\"updateBy\":\"\",\"usableColumn\":false},{\"capJavaField\":\"UserId\",\"columnComment\":\"预约人\",\"columnId\":60,\"columnName\":\"user_id\",\"columnType\":\"bigint(20)\",\"createBy\":\"admin\",\"createTime\":\"2023-09-16 11:37:23\",\"dictType\":\"\",\"edit\":true,\"htmlType\":\"input\",\"increment\":false,\"insert\":true,\"isEdit\":\"1\",\"isIncrement\":\"0\",\"isInsert\":\"1\",\"isList\":\"1\",\"isPk\":\"0\",\"isQuery\":\"1\",\"isRequired\":\"1\",\"javaField\":\"userId\",\"javaType\":\"Long\",\"list\":true,\"params\":{},\"pk\":false,\"query\":true,\"queryType\":\"EQ\",\"required\":true,\"sort\":2,\"superColumn\":false,\"tableId\":6,\"updateBy\":\"\",\"usableColumn\":false},{\"capJavaField\":\"ReservationTime\",\"columnComment\":\"预约时间\",\"columnId\":61,\"columnName\":\"reservation_time\",\"columnType\":\"datetime\",\"createBy\":\"admin\",\"createTime\":\"2023-09-16 11:37:23\",\"dictType\":\"\",\"edit\":true,\"htmlType\":\"datetime\",\"increment\":false,\"insert\":true,\"isEdit\":\"1\",\"isIncrement\":\"0\",\"isInsert\":\"1\",\"isList\":\"1\",\"isPk\":\"0\",\"isQuery\":\"1\",\"isRequired\":\"1\",\"javaField\":\"reservationTime\",\"javaType\":\"Date\",\"list\":true,\"params\":{},\"pk\":false,\"query\":true,\"queryType\":\"EQ\",\"required\":true,\"sort\":3,\"superColumn\":false,\"tableId\":6,\"updateBy\":\"\",\"usableColumn\":false},{\"capJavaField\":\"InSiteId\",\"columnComment\":\"上车站点\",\"columnId\":62,\"columnName\":\"in_site_id\",\"columnType\":\"bigint(20)\",\"createBy\":\"admin\",\"createTime\":\"2023-09-16 11:37:23\",\"dictType\":\"\",\"edit\":true,\"htmlType\":\"input\",\"increment\":false,\"insert\":true,\"isEdit\":\"1\",\"isIncrement\":\"0\",\"isInsert\":\"1\",\"isList\":\"1\",\"isPk\":\"0\",\"isQuery\":\"1\",\"isRequired\":\"1\",\"java','{\"msg\":\"操作成功\",\"code\":200}',0,NULL,'2023-09-16 11:38:09',17),(128,'代码生成',8,'com.ruoyi.generator.controller.GenController.batchGenCode()','GET',1,'admin',NULL,'/tool/gen/batchGenCode','127.0.0.1','内网IP','{\"tables\":\"bus_reservation\"}',NULL,0,NULL,'2023-09-16 11:38:12',44),(129,'站点',1,'com.ruoyi.system.controller.BusRouteSiteController.add()','POST',1,'admin',NULL,'/system/site','127.0.0.1','内网IP','{\"address\":\"333\",\"createTime\":\"2023-09-16 11:39:30\",\"id\":100,\"latitude\":\"3\",\"longitude\":\"2\",\"params\":{},\"siteName\":\"学校\",\"status\":\"0\"}','{\"msg\":\"操作成功\",\"code\":200}',0,NULL,'2023-09-16 11:39:30',20),(130,'校车行驶记录',1,'com.ruoyi.system.controller.BusDrivingRecordController.add()','POST',1,'admin',NULL,'/system/record','127.0.0.1','内网IP','{\"busId\":1,\"createTime\":\"2023-09-16 11:40:52\",\"departureTime\":\"2023-09-15\",\"driver\":1,\"id\":100,\"params\":{},\"routeId\":1,\"status\":\"0\"}','{\"msg\":\"操作成功\",\"code\":200}',0,NULL,'2023-09-16 11:40:52',10),(131,'乘车预约记录',1,'com.ruoyi.system.controller.BusReservationController.add()','POST',1,'admin',NULL,'/system/reservation','127.0.0.1','内网IP','{\"createTime\":\"2023-09-16 11:41:07\",\"didingStatus\":\"0\",\"id\":100,\"inSiteId\":1,\"outSiteId\":2,\"params\":{},\"reservationTime\":\"2023-09-16\",\"status\":\"0\",\"userId\":1}','{\"msg\":\"操作成功\",\"code\":200}',0,NULL,'2023-09-16 11:41:07',6),(132,'菜单管理',1,'com.ruoyi.web.controller.system.SysMenuController.add()','POST',1,'admin',NULL,'/system/menu','127.0.0.1','内网IP','{\"children\":[],\"createBy\":\"admin\",\"icon\":\"cascader\",\"isCache\":\"0\",\"isFrame\":\"1\",\"menuName\":\"校车管理\",\"menuType\":\"M\",\"orderNum\":1,\"params\":{},\"parentId\":0,\"path\":\"/bus\",\"status\":\"0\",\"visible\":\"0\"}','{\"msg\":\"操作成功\",\"code\":200}',0,NULL,'2023-09-16 11:42:22',10),(133,'菜单管理',2,'com.ruoyi.web.controller.system.SysMenuController.edit()','PUT',1,'admin',NULL,'/system/menu','127.0.0.1','内网IP','{\"children\":[],\"createTime\":\"2023-09-16 11:42:22\",\"icon\":\"cascader\",\"isCache\":\"0\",\"isFrame\":\"1\",\"menuId\":2030,\"menuName\":\"校车管理\",\"menuType\":\"M\",\"orderNum\":1,\"params\":{},\"parentId\":0,\"path\":\"bus\",\"perms\":\"\",\"status\":\"0\",\"updateBy\":\"admin\",\"visible\":\"0\"}','{\"msg\":\"操作成功\",\"code\":200}',0,NULL,'2023-09-16 11:42:36',12),(134,'菜单管理',2,'com.ruoyi.web.controller.system.SysMenuController.edit()','PUT',1,'admin',NULL,'/system/menu','127.0.0.1','内网IP','{\"children\":[],\"component\":\"system/bus/index\",\"createTime\":\"2023-09-16 11:17:08\",\"icon\":\"#\",\"isCache\":\"0\",\"isFrame\":\"1\",\"menuId\":2000,\"menuName\":\"校车\",\"menuType\":\"C\",\"orderNum\":1,\"params\":{},\"parentId\":2030,\"path\":\"bus\",\"perms\":\"system:bus:list\",\"status\":\"0\",\"updateBy\":\"admin\",\"visible\":\"0\"}','{\"msg\":\"操作成功\",\"code\":200}',0,NULL,'2023-09-16 11:42:44',5),(135,'菜单管理',2,'com.ruoyi.web.controller.system.SysMenuController.edit()','PUT',1,'admin',NULL,'/system/menu','127.0.0.1','内网IP','{\"children\":[],\"component\":\"system/site/index\",\"createTime\":\"2023-09-16 11:27:34\",\"icon\":\"#\",\"isCache\":\"0\",\"isFrame\":\"1\",\"menuId\":2006,\"menuName\":\"站点\",\"menuType\":\"C\",\"orderNum\":1,\"params\":{},\"parentId\":2030,\"path\":\"site\",\"perms\":\"system:site:list\",\"status\":\"0\",\"updateBy\":\"admin\",\"visible\":\"0\"}','{\"msg\":\"操作成功\",\"code\":200}',0,NULL,'2023-09-16 11:42:47',7),(136,'菜单管理',2,'com.ruoyi.web.controller.system.SysMenuController.edit()','PUT',1,'admin',NULL,'/system/menu','127.0.0.1','内网IP','{\"children\":[],\"component\":\"system/route/index\",\"createTime\":\"2023-09-16 11:30:42\",\"icon\":\"#\",\"isCache\":\"0\",\"isFrame\":\"1\",\"menuId\":2012,\"menuName\":\"路线\",\"menuType\":\"C\",\"orderNum\":1,\"params\":{},\"parentId\":2030,\"path\":\"route\",\"perms\":\"system:route:list\",\"status\":\"0\",\"updateBy\":\"admin\",\"visible\":\"0\"}','{\"msg\":\"操作成功\",\"code\":200}',0,NULL,'2023-09-16 11:42:51',6),(137,'菜单管理',2,'com.ruoyi.web.controller.system.SysMenuController.edit()','PUT',1,'admin',NULL,'/system/menu','127.0.0.1','内网IP','{\"children\":[],\"component\":\"system/record/index\",\"createTime\":\"2023-09-16 11:32:43\",\"icon\":\"#\",\"isCache\":\"0\",\"isFrame\":\"1\",\"menuId\":2018,\"menuName\":\"校车行驶记录\",\"menuType\":\"C\",\"orderNum\":1,\"params\":{},\"parentId\":2030,\"path\":\"record\",\"perms\":\"system:record:list\",\"status\":\"0\",\"updateBy\":\"admin\",\"visible\":\"0\"}','{\"msg\":\"操作成功\",\"code\":200}',0,NULL,'2023-09-16 11:42:56',6),(138,'菜单管理',2,'com.ruoyi.web.controller.system.SysMenuController.edit()','PUT',1,'admin',NULL,'/system/menu','127.0.0.1','内网IP','{\"children\":[],\"component\":\"system/reservation/index\",\"createTime\":\"2023-09-16 11:38:41\",\"icon\":\"#\",\"isCache\":\"0\",\"isFrame\":\"1\",\"menuId\":2024,\"menuName\":\"乘车预约记录\",\"menuType\":\"C\",\"orderNum\":1,\"params\":{},\"parentId\":2030,\"path\":\"reservation\",\"perms\":\"system:reservation:list\",\"status\":\"0\",\"updateBy\":\"admin\",\"visible\":\"0\"}','{\"msg\":\"操作成功\",\"code\":200}',0,NULL,'2023-09-16 11:43:00',5),(139,'用户管理',1,'com.ruoyi.web.controller.system.SysUserController.add()','POST',1,'admin',NULL,'/system/user','127.0.0.1','内网IP','{\"admin\":false,\"createBy\":\"admin\",\"deptId\":103,\"nickName\":\"教师1\",\"params\":{},\"postIds\":[],\"roleIds\":[],\"sex\":\"0\",\"status\":\"0\",\"userId\":100,\"userName\":\"J01010625\"}','{\"msg\":\"操作成功\",\"code\":200}',0,NULL,'2023-09-16 14:53:20',65),(140,'校车',2,'com.ruoyi.system.controller.BusBusController.edit()','PUT',1,'admin',NULL,'/system/bus','127.0.0.1','内网IP','{\"busName\":\"校车11\",\"createBy\":\"\",\"createTime\":\"2023-09-16 11:18:35\",\"delFlag\":\"0\",\"driver\":100,\"driverName\":\"教师1\",\"id\":100,\"params\":{},\"plate\":\"贵A654321\",\"seating\":30,\"status\":\"0\",\"updateBy\":\"\",\"updateTime\":\"2023-09-16 15:34:55\"}','{\"msg\":\"操作成功\",\"code\":200}',0,NULL,'2023-09-16 15:34:55',13),(141,'站点',2,'com.ruoyi.system.controller.BusRouteSiteController.edit()','PUT',1,'admin',NULL,'/system/site','127.0.0.1','内网IP','{\"address\":\"333\",\"createBy\":\"\",\"createTime\":\"2023-09-16 11:39:30\",\"delFlag\":\"0\",\"id\":100,\"latitude\":\"66°33\' 38\\\" N\",\"longitude\":\"2\",\"params\":{},\"siteName\":\"学校\",\"status\":\"0\",\"updateBy\":\"\",\"updateTime\":\"2023-09-16 15:38:04\"}','{\"msg\":\"操作成功\",\"code\":200}',0,NULL,'2023-09-16 15:38:04',5),(142,'站点',2,'com.ruoyi.system.controller.BusRouteSiteController.edit()','PUT',1,'admin',NULL,'/system/site','127.0.0.1','内网IP','{\"address\":\"白云区党委\",\"createBy\":\"\",\"createTime\":\"2023-09-16 11:39:30\",\"delFlag\":\"0\",\"id\":100,\"latitude\":\"66°33\' 38\\\" N\",\"longitude\":\"66°33\' 38\\\" N\",\"params\":{},\"siteName\":\"白云区党委\",\"status\":\"0\",\"updateBy\":\"\",\"updateTime\":\"2023-09-16 15:41:21\"}','{\"msg\":\"操作成功\",\"code\":200}',0,NULL,'2023-09-16 15:41:21',5),(143,'站点',1,'com.ruoyi.system.controller.BusRouteSiteController.add()','POST',1,'admin',NULL,'/system/site','127.0.0.1','内网IP','{\"address\":\"金阳新世界\",\"createTime\":\"2023-09-16 15:42:28\",\"id\":101,\"latitude\":\"1\",\"longitude\":\"1\",\"params\":{},\"siteName\":\"金阳新世界\",\"status\":\"0\"}','{\"msg\":\"操作成功\",\"code\":200}',0,NULL,'2023-09-16 15:42:28',8),(144,'站点',1,'com.ruoyi.system.controller.BusRouteSiteController.add()','POST',1,'admin',NULL,'/system/site','127.0.0.1','内网IP','{\"address\":\"省教育厅\",\"createTime\":\"2023-09-16 15:44:50\",\"id\":102,\"latitude\":\"1\",\"longitude\":\"1\",\"params\":{},\"siteName\":\"省教育厅\",\"status\":\"0\"}','{\"msg\":\"操作成功\",\"code\":200}',0,NULL,'2023-09-16 15:44:50',3),(145,'站点',1,'com.ruoyi.system.controller.BusRouteSiteController.add()','POST',1,'admin',NULL,'/system/site','127.0.0.1','内网IP','{\"address\":\"金融城\",\"createTime\":\"2023-09-16 15:45:03\",\"id\":103,\"latitude\":\"1\",\"longitude\":\"1\",\"params\":{},\"siteName\":\"金融城\",\"status\":\"0\"}','{\"msg\":\"操作成功\",\"code\":200}',0,NULL,'2023-09-16 15:45:03',5),(146,'站点',1,'com.ruoyi.system.controller.BusRouteSiteController.add()','POST',1,'admin',NULL,'/system/site','127.0.0.1','内网IP','{\"address\":\"金融101大厦（数博大道与观山东路交叉口）\",\"createTime\":\"2023-09-16 15:45:13\",\"id\":104,\"latitude\":\"1\",\"longitude\":\"1\",\"params\":{},\"siteName\":\"金融101大厦（数博大道与观山东路交叉口）\",\"status\":\"0\"}','{\"msg\":\"操作成功\",\"code\":200}',0,NULL,'2023-09-16 15:45:13',7),(147,'站点',1,'com.ruoyi.system.controller.BusRouteSiteController.add()','POST',1,'admin',NULL,'/system/site','127.0.0.1','内网IP','{\"address\":\"兴筑东路口\",\"createTime\":\"2023-09-16 15:45:22\",\"id\":105,\"latitude\":\"1\",\"longitude\":\"1\",\"params\":{},\"siteName\":\"兴筑东路口\",\"status\":\"0\"}','{\"msg\":\"操作成功\",\"code\":200}',0,NULL,'2023-09-16 15:45:22',6),(148,'站点',1,'com.ruoyi.system.controller.BusRouteSiteController.add()','POST',1,'admin',NULL,'/system/site','127.0.0.1','内网IP','{\"address\":\"长岭南路口（北京西路）\",\"createTime\":\"2023-09-16 15:45:30\",\"id\":106,\"latitude\":\"1\",\"longitude\":\"1\",\"params\":{},\"siteName\":\"长岭南路口（北京西路）\",\"status\":\"0\"}','{\"msg\":\"操作成功\",\"code\":200}',0,NULL,'2023-09-16 15:45:30',3),(149,'站点',1,'com.ruoyi.system.controller.BusRouteSiteController.add()','POST',1,'admin',NULL,'/system/site','127.0.0.1','内网IP','{\"address\":\"小湾河大桥\",\"createTime\":\"2023-09-16 15:45:38\",\"id\":107,\"latitude\":\"1\",\"longitude\":\"1\",\"params\":{},\"siteName\":\"小湾河大桥\",\"status\":\"0\"}','{\"msg\":\"操作成功\",\"code\":200}',0,NULL,'2023-09-16 15:45:38',7),(150,'站点',1,'com.ruoyi.system.controller.BusRouteSiteController.add()','POST',1,'admin',NULL,'/system/site','127.0.0.1','内网IP','{\"address\":\"北京西路公交站（金阳南路地铁站B出口旁）\",\"createTime\":\"2023-09-16 15:45:48\",\"id\":108,\"latitude\":\"1\",\"longitude\":\"1\",\"params\":{},\"siteName\":\"北京西路公交站（金阳南路地铁站B出口旁）\",\"status\":\"0\"}','{\"msg\":\"操作成功\",\"code\":200}',0,NULL,'2023-09-16 15:45:48',3),(151,'站点',1,'com.ruoyi.system.controller.BusRouteSiteController.add()','POST',1,'admin',NULL,'/system/site','127.0.0.1','内网IP','{\"address\":\"金清大道口（中铁逸都国际）\",\"createTime\":\"2023-09-16 15:45:58\",\"id\":109,\"latitude\":\"1\",\"longitude\":\"1\",\"params\":{},\"siteName\":\"金清大道口（中铁逸都国际）\",\"status\":\"0\"}','{\"msg\":\"操作成功\",\"code\":200}',0,NULL,'2023-09-16 15:45:58',7),(152,'站点',1,'com.ruoyi.system.controller.BusRouteSiteController.add()','POST',1,'admin',NULL,'/system/site','127.0.0.1','内网IP','{\"address\":\"二铺公交站\",\"createTime\":\"2023-09-16 15:46:07\",\"id\":110,\"latitude\":\"1\",\"longitude\":\"1\",\"params\":{},\"siteName\":\"二铺公交站\",\"status\":\"0\"}','{\"msg\":\"操作成功\",\"code\":200}',0,NULL,'2023-09-16 15:46:07',7),(153,'站点',1,'com.ruoyi.system.controller.BusRouteSiteController.add()','POST',1,'admin',NULL,'/system/site','127.0.0.1','内网IP','{\"address\":\"宾阳大道（南）公交站\",\"createTime\":\"2023-09-16 15:46:16\",\"id\":111,\"latitude\":\"1\",\"longitude\":\"1\",\"params\":{},\"siteName\":\"宾阳大道（南）公交站\",\"status\":\"0\"}','{\"msg\":\"操作成功\",\"code\":200}',0,NULL,'2023-09-16 15:46:16',5),(154,'站点',1,'com.ruoyi.system.controller.BusRouteSiteController.add()','POST',1,'admin',NULL,'/system/site','127.0.0.1','内网IP','{\"address\":\"华丰食品城公交站\",\"createTime\":\"2023-09-16 15:46:24\",\"id\":112,\"latitude\":\"1\",\"longitude\":\"1\",\"params\":{},\"siteName\":\"华丰食品城公交站\",\"status\":\"0\"}','{\"msg\":\"操作成功\",\"code\":200}',0,NULL,'2023-09-16 15:46:24',5),(155,'站点',1,'com.ruoyi.system.controller.BusRouteSiteController.add()','POST',1,'admin',NULL,'/system/site','127.0.0.1','内网IP','{\"address\":\"经宾阳大道（百马大道-兴安大道）\",\"createTime\":\"2023-09-16 15:46:32\",\"id\":113,\"latitude\":\"1\",\"longitude\":\"1\",\"params\":{},\"siteName\":\"经宾阳大道（百马大道-兴安大道）\",\"status\":\"0\"}','{\"msg\":\"操作成功\",\"code\":200}',0,NULL,'2023-09-16 15:46:32',8),(156,'站点',1,'com.ruoyi.system.controller.BusRouteSiteController.add()','POST',1,'admin',NULL,'/system/site','127.0.0.1','内网IP','{\"address\":\"富贵安康小镇（新校区公租房）\",\"createTime\":\"2023-09-16 15:46:51\",\"id\":114,\"latitude\":\"1\",\"longitude\":\"1\",\"params\":{},\"siteName\":\"富贵安康小镇（新校区公租房）\",\"status\":\"0\"}','{\"msg\":\"操作成功\",\"code\":200}',0,NULL,'2023-09-16 15:46:51',5),(157,'路线',1,'com.ruoyi.system.controller.BusRouteController.add()','POST',1,'admin',NULL,'/system/route','127.0.0.1','内网IP','{\"createTime\":\"2023-09-16 15:47:19\",\"endSiteId\":1,\"id\":100,\"params\":{},\"routeName\":\"一号车线路\",\"startSiteId\":1,\"status\":\"0\"}','{\"msg\":\"操作成功\",\"code\":200}',0,NULL,'2023-09-16 15:47:19',7),(158,'路线',2,'com.ruoyi.system.controller.BusRouteController.edit()','PUT',1,'admin',NULL,'/system/route','127.0.0.1','内网IP','{\"createBy\":\"\",\"createTime\":\"2023-09-16 15:47:19\",\"delFlag\":\"0\",\"endSiteId\":114,\"id\":100,\"params\":{},\"routeName\":\"一号车线路\",\"startSiteId\":100,\"status\":\"0\",\"updateBy\":\"\",\"updateTime\":\"2023-09-16 15:49:28\"}','{\"msg\":\"操作成功\",\"code\":200}',0,NULL,'2023-09-16 15:49:28',5),(159,'校车',2,'com.ruoyi.system.controller.BusBusController.edit()','PUT',1,'admin',NULL,'/system/bus','127.0.0.1','内网IP','{\"busName\":\"校车11\",\"createBy\":\"\",\"createTime\":\"2023-09-16 11:18:35\",\"delFlag\":\"0\",\"driver\":100,\"driverName\":\"教师1\",\"id\":100,\"params\":{},\"plate\":\"贵A654321\",\"seating\":30,\"status\":\"0\",\"updateBy\":\"\",\"updateTime\":\"2023-09-16 15:51:42\"}','{\"msg\":\"操作成功\",\"code\":200}',0,NULL,'2023-09-16 15:51:42',14),(160,'校车行驶记录',2,'com.ruoyi.system.controller.BusDrivingRecordController.edit()','PUT',1,'admin',NULL,'/system/record','127.0.0.1','内网IP','{\"busId\":100,\"busName\":\"校车11\",\"createBy\":\"\",\"createTime\":\"2023-09-16 11:40:52\",\"delFlag\":\"0\",\"departureTime\":\"2023-09-15\",\"driver\":100,\"driverName\":\"教师1\",\"id\":100,\"params\":{},\"routeId\":100,\"routeName\":\"一号车线路\",\"status\":\"0\",\"updateBy\":\"\",\"updateTime\":\"2023-09-16 16:24:58\"}','{\"msg\":\"操作成功\",\"code\":200}',0,NULL,'2023-09-16 16:24:58',14),(161,'校车',1,'com.ruoyi.system.controller.BusBusController.add()','POST',1,'admin',NULL,'/system/bus','127.0.0.1','内网IP','{\"busName\":\"校车22\",\"createTime\":\"2023-09-16 17:10:40\",\"driver\":100,\"driverName\":\"教师1\",\"id\":101,\"params\":{},\"plate\":\"贵A123456\",\"seating\":20,\"status\":\"0\"}','{\"msg\":\"操作成功\",\"code\":200}',0,NULL,'2023-09-16 17:10:40',16),(162,'校车行驶记录',1,'com.ruoyi.system.controller.BusDrivingRecordController.add()','POST',1,'admin',NULL,'/system/record','127.0.0.1','内网IP','{\"busId\":101,\"createTime\":\"2023-09-16 17:11:13\",\"departureTime\":\"2023-09-17\",\"driverName\":\"100\",\"params\":{},\"routeId\":100,\"status\":\"0\"}',NULL,1,'\r\n### Error updating database.  Cause: java.sql.SQLException: Field \'driver\' doesn\'t have a default value\r\n### The error may exist in file [D:\\openSource\\RuoYi-Vue\\ruoyi-admin\\target\\classes\\mapper\\system\\BusDrivingRecordMapper.xml]\r\n### The error may involve com.ruoyi.system.mapper.BusDrivingRecordMapper.insertBusDrivingRecord-Inline\r\n### The error occurred while setting parameters\r\n### SQL: insert into bus_driving_record          ( bus_id,                          route_id,             departure_time,             status,                                       create_time )           values ( ?,                          ?,             ?,             ?,                                       ? )\r\n### Cause: java.sql.SQLException: Field \'driver\' doesn\'t have a default value\n; Field \'driver\' doesn\'t have a default value; nested exception is java.sql.SQLException: Field \'driver\' doesn\'t have a default value','2023-09-16 17:11:13',56),(163,'校车行驶记录',1,'com.ruoyi.system.controller.BusDrivingRecordController.add()','POST',1,'admin',NULL,'/system/record','127.0.0.1','内网IP','{\"busId\":101,\"createTime\":\"2023-09-16 17:11:23\",\"departureTime\":\"2023-09-17\",\"driverName\":\"100\",\"params\":{},\"routeId\":100,\"status\":\"0\"}',NULL,1,'\r\n### Error updating database.  Cause: java.sql.SQLException: Field \'driver\' doesn\'t have a default value\r\n### The error may exist in file [D:\\openSource\\RuoYi-Vue\\ruoyi-admin\\target\\classes\\mapper\\system\\BusDrivingRecordMapper.xml]\r\n### The error may involve com.ruoyi.system.mapper.BusDrivingRecordMapper.insertBusDrivingRecord-Inline\r\n### The error occurred while setting parameters\r\n### SQL: insert into bus_driving_record          ( bus_id,                          route_id,             departure_time,             status,                                       create_time )           values ( ?,                          ?,             ?,             ?,                                       ? )\r\n### Cause: java.sql.SQLException: Field \'driver\' doesn\'t have a default value\n; Field \'driver\' doesn\'t have a default value; nested exception is java.sql.SQLException: Field \'driver\' doesn\'t have a default value','2023-09-16 17:11:23',4),(164,'校车行驶记录',2,'com.ruoyi.system.controller.BusDrivingRecordController.edit()','PUT',1,'admin',NULL,'/system/record','127.0.0.1','内网IP','{\"busId\":101,\"busName\":\"校车11\",\"createBy\":\"\",\"createTime\":\"2023-09-16 11:40:52\",\"delFlag\":\"0\",\"departureTime\":\"2023-09-15\",\"driver\":100,\"driverName\":\"教师1\",\"id\":100,\"params\":{},\"routeId\":100,\"routeName\":\"一号车线路\",\"status\":\"0\",\"updateBy\":\"\",\"updateTime\":\"2023-09-16 17:12:09\"}','{\"msg\":\"操作成功\",\"code\":200}',0,NULL,'2023-09-16 17:12:09',7),(165,'校车行驶记录',1,'com.ruoyi.system.controller.BusDrivingRecordController.add()','POST',1,'admin',NULL,'/system/record','127.0.0.1','内网IP','{\"busId\":101,\"createTime\":\"2023-09-16 17:22:39\",\"departureTime\":\"2023-09-22\",\"driver\":100,\"driverName\":\"教师1\",\"id\":101,\"params\":{},\"routeId\":100,\"status\":\"0\"}','{\"msg\":\"操作成功\",\"code\":200}',0,NULL,'2023-09-16 17:22:39',4),(166,'校车行驶记录',2,'com.ruoyi.system.controller.BusDrivingRecordController.edit()','PUT',1,'admin',NULL,'/system/record','127.0.0.1','内网IP','{\"busId\":100,\"busName\":\"校车22\",\"createBy\":\"\",\"createTime\":\"2023-09-16 17:22:39\",\"delFlag\":\"0\",\"departureTime\":\"2023-09-22\",\"driver\":100,\"driverName\":\"教师1\",\"id\":101,\"params\":{},\"routeId\":100,\"routeName\":\"一号车线路\",\"status\":\"0\",\"updateBy\":\"\",\"updateTime\":\"2023-09-16 17:22:47\"}','{\"msg\":\"操作成功\",\"code\":200}',0,NULL,'2023-09-16 17:22:47',6),(167,'路线',2,'com.ruoyi.system.controller.BusRouteController.edit()','PUT',1,'admin',NULL,'/system/route','127.0.0.1','内网IP','{\"createBy\":\"\",\"createTime\":\"2023-09-16 15:47:19\",\"delFlag\":\"0\",\"endSiteId\":114,\"endSiteName\":\"富贵安康小镇（新校区公租房）\",\"id\":100,\"params\":{},\"routeName\":\"一号车线路\",\"startSiteId\":101,\"startSiteName\":\"白云区党委\",\"status\":\"0\",\"updateBy\":\"\",\"updateTime\":\"2023-09-16 20:15:43\"}','{\"msg\":\"操作成功\",\"code\":200}',0,NULL,'2023-09-16 20:15:43',16),(168,'路线',2,'com.ruoyi.system.controller.BusRouteController.edit()','PUT',1,'admin',NULL,'/system/route','127.0.0.1','内网IP','{\"createBy\":\"\",\"createTime\":\"2023-09-16 15:47:19\",\"delFlag\":\"0\",\"endSiteId\":114,\"endSiteName\":\"富贵安康小镇（新校区公租房）\",\"id\":100,\"params\":{},\"routeName\":\"一号车线路\",\"startSiteId\":100,\"startSiteName\":\"金阳新世界\",\"status\":\"0\",\"updateBy\":\"\",\"updateTime\":\"2023-09-16 20:15:47\"}','{\"msg\":\"操作成功\",\"code\":200}',0,NULL,'2023-09-16 20:15:47',6),(169,'路线',2,'com.ruoyi.system.controller.BusRouteController.edit()','PUT',1,'admin',NULL,'/system/route','127.0.0.1','内网IP','{\"createBy\":\"\",\"createTime\":\"2023-09-16 15:47:19\",\"delFlag\":\"0\",\"endSiteId\":114,\"endSiteName\":\"富贵安康小镇（新校区公租房）\",\"forwardRoute\":\"[100,101,102]\",\"id\":100,\"params\":{},\"reverseRoute\":\"[102,101,100]\",\"routeName\":\"一号车线路\",\"startSiteId\":100,\"startSiteName\":\"白云区党委\",\"status\":\"0\",\"updateBy\":\"\",\"updateTime\":\"2023-09-16 21:56:02\"}','{\"msg\":\"操作成功\",\"code\":200}',0,NULL,'2023-09-16 21:56:02',9),(170,'路线',2,'com.ruoyi.system.controller.BusRouteController.edit()','PUT',1,'admin',NULL,'/system/route','127.0.0.1','内网IP','{\"createBy\":\"\",\"createTime\":\"2023-09-16 15:47:19\",\"delFlag\":\"0\",\"endSiteId\":114,\"endSiteName\":\"富贵安康小镇（新校区公租房）\",\"forwardRoute\":\"[100,101,102,103,104,113,114]\",\"id\":100,\"params\":{},\"reverseRoute\":\"[114,113,104,103,102,101,100]\",\"routeName\":\"一号车线路\",\"startSiteId\":100,\"startSiteName\":\"白云区党委\",\"status\":\"0\",\"updateBy\":\"\",\"updateTime\":\"2023-09-16 22:35:59\"}','{\"msg\":\"操作成功\",\"code\":200}',0,NULL,'2023-09-16 22:35:59',6),(171,'路线',2,'com.ruoyi.system.controller.BusRouteController.edit()','PUT',1,'admin',NULL,'/system/route','127.0.0.1','内网IP','{\"createBy\":\"\",\"createTime\":\"2023-09-16 15:47:19\",\"delFlag\":\"0\",\"endSiteId\":114,\"endSiteName\":\"富贵安康小镇（新校区公租房）\",\"forwardRoute\":\"[100,101,102,104,113,114]\",\"id\":100,\"params\":{},\"reverseRoute\":\"[114,113,104,102,101,100]\",\"routeName\":\"一号车线路\",\"startSiteId\":100,\"startSiteName\":\"白云区党委\",\"status\":\"0\",\"updateBy\":\"\",\"updateTime\":\"2023-09-16 22:44:22\"}','{\"msg\":\"操作成功\",\"code\":200}',0,NULL,'2023-09-16 22:44:22',18),(172,'路线',2,'com.ruoyi.system.controller.BusRouteController.edit()','PUT',1,'admin',NULL,'/system/route','127.0.0.1','内网IP','{\"createBy\":\"\",\"createTime\":\"2023-09-16 15:47:19\",\"delFlag\":\"0\",\"endSiteId\":114,\"endSiteName\":\"富贵安康小镇（新校区公租房）\",\"forwardRoute\":\"[100,101,102,103,104,105,106,107,108,109,110,111,112,113,114]\",\"id\":100,\"params\":{},\"reverseRoute\":\"[114,113,112,111,110,109,108,107,106,105,104,103,102,101,100]\",\"routeName\":\"一号车线路\",\"startSiteId\":100,\"startSiteName\":\"白云区党委\",\"status\":\"0\",\"updateBy\":\"\",\"updateTime\":\"2023-09-16 22:45:33\"}','{\"msg\":\"操作成功\",\"code\":200}',0,NULL,'2023-09-16 22:45:33',3),(173,'路线',2,'com.ruoyi.system.controller.BusRouteController.edit()','PUT',1,'admin',NULL,'/system/route','127.0.0.1','内网IP','{\"createBy\":\"\",\"createTime\":\"2023-09-16 15:47:19\",\"delFlag\":\"0\",\"endSiteId\":114,\"endSiteName\":\"富贵安康小镇（新校区公租房）\",\"forwardRoute\":\"[100,101,102,103,104,105,106,107,108,109,110,111,112,113,114]\",\"id\":100,\"params\":{},\"reverseRoute\":\"[114,113,112,111,110,109,108,107,106,105,104,103,102,101,100]\",\"routeName\":\"一号车线路\",\"startSiteId\":100,\"startSiteName\":\"白云区党委\",\"status\":\"0\",\"updateBy\":\"\",\"updateTime\":\"2023-09-16 22:46:43\"}','{\"msg\":\"操作成功\",\"code\":200}',0,NULL,'2023-09-16 22:46:43',3),(174,'用户管理',2,'com.ruoyi.web.controller.system.SysUserController.edit()','PUT',1,'admin',NULL,'/system/user','127.0.0.1','内网IP','{\"admin\":false,\"avatar\":\"\",\"createBy\":\"admin\",\"createTime\":\"2023-09-16 14:53:20\",\"delFlag\":\"0\",\"dept\":{\"ancestors\":\"0,100,101\",\"children\":[],\"deptId\":103,\"deptName\":\"研发部门\",\"leader\":\"若依\",\"orderNum\":1,\"params\":{},\"parentId\":101,\"status\":\"0\"},\"deptId\":103,\"email\":\"\",\"loginIp\":\"\",\"nickName\":\"教师1\",\"params\":{},\"phonenumber\":\"18502820405\",\"postIds\":[],\"roleIds\":[],\"roles\":[],\"sex\":\"0\",\"status\":\"0\",\"updateBy\":\"admin\",\"userId\":100,\"userName\":\"J01010625\"}','{\"msg\":\"操作成功\",\"code\":200}',0,NULL,'2023-09-16 23:08:45',23);
/*!40000 ALTER TABLE `sys_oper_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_post`
--

DROP TABLE IF EXISTS `sys_post`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sys_post` (
  `post_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '岗位ID',
  `post_code` varchar(64) NOT NULL COMMENT '岗位编码',
  `post_name` varchar(50) NOT NULL COMMENT '岗位名称',
  `post_sort` int(4) NOT NULL COMMENT '显示顺序',
  `status` char(1) NOT NULL COMMENT '状态（0正常 1停用）',
  `create_by` varchar(64) DEFAULT '' COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) DEFAULT '' COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`post_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COMMENT='岗位信息表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_post`
--

LOCK TABLES `sys_post` WRITE;
/*!40000 ALTER TABLE `sys_post` DISABLE KEYS */;
INSERT INTO `sys_post` VALUES (1,'ceo','董事长',1,'0','admin','2023-09-16 10:17:55','',NULL,''),(2,'se','项目经理',2,'0','admin','2023-09-16 10:17:55','',NULL,''),(3,'hr','人力资源',3,'0','admin','2023-09-16 10:17:55','',NULL,''),(4,'user','普通员工',4,'0','admin','2023-09-16 10:17:55','',NULL,'');
/*!40000 ALTER TABLE `sys_post` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_role`
--

DROP TABLE IF EXISTS `sys_role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sys_role` (
  `role_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '角色ID',
  `role_name` varchar(30) NOT NULL COMMENT '角色名称',
  `role_key` varchar(100) NOT NULL COMMENT '角色权限字符串',
  `role_sort` int(4) NOT NULL COMMENT '显示顺序',
  `data_scope` char(1) DEFAULT '1' COMMENT '数据范围（1：全部数据权限 2：自定数据权限 3：本部门数据权限 4：本部门及以下数据权限）',
  `menu_check_strictly` tinyint(1) DEFAULT '1' COMMENT '菜单树选择项是否关联显示',
  `dept_check_strictly` tinyint(1) DEFAULT '1' COMMENT '部门树选择项是否关联显示',
  `status` char(1) NOT NULL COMMENT '角色状态（0正常 1停用）',
  `del_flag` char(1) DEFAULT '0' COMMENT '删除标志（0代表存在 2代表删除）',
  `create_by` varchar(64) DEFAULT '' COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) DEFAULT '' COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`role_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COMMENT='角色信息表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_role`
--

LOCK TABLES `sys_role` WRITE;
/*!40000 ALTER TABLE `sys_role` DISABLE KEYS */;
INSERT INTO `sys_role` VALUES (1,'超级管理员','admin',1,'1',1,1,'0','0','admin','2023-09-16 10:17:55','',NULL,'超级管理员'),(2,'普通角色','common',2,'2',1,1,'0','0','admin','2023-09-16 10:17:55','',NULL,'普通角色');
/*!40000 ALTER TABLE `sys_role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_role_dept`
--

DROP TABLE IF EXISTS `sys_role_dept`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sys_role_dept` (
  `role_id` bigint(20) NOT NULL COMMENT '角色ID',
  `dept_id` bigint(20) NOT NULL COMMENT '部门ID',
  PRIMARY KEY (`role_id`,`dept_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='角色和部门关联表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_role_dept`
--

LOCK TABLES `sys_role_dept` WRITE;
/*!40000 ALTER TABLE `sys_role_dept` DISABLE KEYS */;
INSERT INTO `sys_role_dept` VALUES (2,100),(2,101),(2,105);
/*!40000 ALTER TABLE `sys_role_dept` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_role_menu`
--

DROP TABLE IF EXISTS `sys_role_menu`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sys_role_menu` (
  `role_id` bigint(20) NOT NULL COMMENT '角色ID',
  `menu_id` bigint(20) NOT NULL COMMENT '菜单ID',
  PRIMARY KEY (`role_id`,`menu_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='角色和菜单关联表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_role_menu`
--

LOCK TABLES `sys_role_menu` WRITE;
/*!40000 ALTER TABLE `sys_role_menu` DISABLE KEYS */;
INSERT INTO `sys_role_menu` VALUES (2,1),(2,2),(2,3),(2,4),(2,100),(2,101),(2,102),(2,103),(2,104),(2,105),(2,106),(2,107),(2,108),(2,109),(2,110),(2,111),(2,112),(2,113),(2,114),(2,115),(2,116),(2,117),(2,500),(2,501),(2,1000),(2,1001),(2,1002),(2,1003),(2,1004),(2,1005),(2,1006),(2,1007),(2,1008),(2,1009),(2,1010),(2,1011),(2,1012),(2,1013),(2,1014),(2,1015),(2,1016),(2,1017),(2,1018),(2,1019),(2,1020),(2,1021),(2,1022),(2,1023),(2,1024),(2,1025),(2,1026),(2,1027),(2,1028),(2,1029),(2,1030),(2,1031),(2,1032),(2,1033),(2,1034),(2,1035),(2,1036),(2,1037),(2,1038),(2,1039),(2,1040),(2,1041),(2,1042),(2,1043),(2,1044),(2,1045),(2,1046),(2,1047),(2,1048),(2,1049),(2,1050),(2,1051),(2,1052),(2,1053),(2,1054),(2,1055),(2,1056),(2,1057),(2,1058),(2,1059),(2,1060);
/*!40000 ALTER TABLE `sys_role_menu` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_user`
--

DROP TABLE IF EXISTS `sys_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sys_user` (
  `user_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '用户ID',
  `dept_id` bigint(20) DEFAULT NULL COMMENT '部门ID',
  `user_name` varchar(30) NOT NULL COMMENT '用户账号',
  `nick_name` varchar(30) NOT NULL COMMENT '用户昵称',
  `user_type` varchar(2) DEFAULT '00' COMMENT '用户类型（00系统用户）',
  `email` varchar(50) DEFAULT '' COMMENT '用户邮箱',
  `phonenumber` varchar(11) DEFAULT '' COMMENT '手机号码',
  `sex` char(1) DEFAULT '0' COMMENT '用户性别（0男 1女 2未知）',
  `avatar` varchar(100) DEFAULT '' COMMENT '头像地址',
  `password` varchar(100) DEFAULT '' COMMENT '密码',
  `status` char(1) DEFAULT '0' COMMENT '帐号状态（0正常 1停用）',
  `del_flag` char(1) DEFAULT '0' COMMENT '删除标志（0代表存在 2代表删除）',
  `login_ip` varchar(128) DEFAULT '' COMMENT '最后登录IP',
  `login_date` datetime DEFAULT NULL COMMENT '最后登录时间',
  `create_by` varchar(64) DEFAULT '' COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) DEFAULT '' COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=101 DEFAULT CHARSET=utf8mb4 COMMENT='用户信息表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_user`
--

LOCK TABLES `sys_user` WRITE;
/*!40000 ALTER TABLE `sys_user` DISABLE KEYS */;
INSERT INTO `sys_user` VALUES (1,103,'admin','若依','00','ry@163.com','15888888888','1','','$2a$10$7JB720yubVSZvUI0rEqK/.VqGOZTH.ulu33dHOiBE8ByOhJIrdAu2','0','0','192.168.101.16','2023-09-17 21:03:22','admin','2023-09-16 10:17:55','','2023-09-17 21:03:21','管理员'),(2,105,'ry','若依','00','ry@qq.com','15666666666','1','','$2a$10$7JB720yubVSZvUI0rEqK/.VqGOZTH.ulu33dHOiBE8ByOhJIrdAu2','0','0','127.0.0.1','2023-09-16 10:17:55','admin','2023-09-16 10:17:55','',NULL,'测试员'),(100,103,'J01010625','教师1','00','','18502820405','0','','$2a$10$P4zN7gt81W5W0kyPy.lYR.ot26y7NBw3ZFMWnuqRiFttaHd3QXkW6','0','0','127.0.0.1','2023-09-17 20:06:54','admin','2023-09-16 14:53:20','admin','2023-09-17 20:06:54',NULL);
/*!40000 ALTER TABLE `sys_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_user_post`
--

DROP TABLE IF EXISTS `sys_user_post`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sys_user_post` (
  `user_id` bigint(20) NOT NULL COMMENT '用户ID',
  `post_id` bigint(20) NOT NULL COMMENT '岗位ID',
  PRIMARY KEY (`user_id`,`post_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='用户与岗位关联表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_user_post`
--

LOCK TABLES `sys_user_post` WRITE;
/*!40000 ALTER TABLE `sys_user_post` DISABLE KEYS */;
INSERT INTO `sys_user_post` VALUES (1,1),(2,2);
/*!40000 ALTER TABLE `sys_user_post` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_user_role`
--

DROP TABLE IF EXISTS `sys_user_role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sys_user_role` (
  `user_id` bigint(20) NOT NULL COMMENT '用户ID',
  `role_id` bigint(20) NOT NULL COMMENT '角色ID',
  PRIMARY KEY (`user_id`,`role_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='用户和角色关联表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_user_role`
--

LOCK TABLES `sys_user_role` WRITE;
/*!40000 ALTER TABLE `sys_user_role` DISABLE KEYS */;
INSERT INTO `sys_user_role` VALUES (1,1),(2,2);
/*!40000 ALTER TABLE `sys_user_role` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2023-09-17 21:37:55
