package com.ruoyi.system.domain;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 校车行驶站点记录对象 bus_record_site
 * 
 * @author ruoyi
 * @date 2023-09-16
 */
public class BusRecordSite extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** ID */
    private Long id;

    /** 行驶记录 */
    @Excel(name = "行驶记录")
    private Long recordId;

    /** 路线 */
    @Excel(name = "路线")
    private Long routeId;

    /** 站点 */
    @Excel(name = "站点")
    private Long siteId;

    /** 是否到站 */
    @Excel(name = "是否到站")
    private Integer isIn;

    /** 到站时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "到站时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date inTime;

    /** 出站时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "出站时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date outTime;

    /** 状态（0正常 1停用） */
    @Excel(name = "状态", readConverterExp = "0=正常,1=停用")
    private String status;

    /** 删除标志（0代表存在 2代表删除） */
    private String delFlag;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setRecordId(Long recordId) 
    {
        this.recordId = recordId;
    }

    public Long getRecordId() 
    {
        return recordId;
    }
    public void setRouteId(Long routeId) 
    {
        this.routeId = routeId;
    }

    public Long getRouteId() 
    {
        return routeId;
    }
    public void setSiteId(Long siteId) 
    {
        this.siteId = siteId;
    }

    public Long getSiteId() 
    {
        return siteId;
    }
    public void setIsIn(Integer isIn) 
    {
        this.isIn = isIn;
    }

    public Integer getIsIn() 
    {
        return isIn;
    }
    public void setInTime(Date inTime) 
    {
        this.inTime = inTime;
    }

    public Date getInTime() 
    {
        return inTime;
    }
    public void setOutTime(Date outTime) 
    {
        this.outTime = outTime;
    }

    public Date getOutTime() 
    {
        return outTime;
    }
    public void setStatus(String status) 
    {
        this.status = status;
    }

    public String getStatus() 
    {
        return status;
    }
    public void setDelFlag(String delFlag) 
    {
        this.delFlag = delFlag;
    }

    public String getDelFlag() 
    {
        return delFlag;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("recordId", getRecordId())
            .append("routeId", getRouteId())
            .append("siteId", getSiteId())
            .append("isIn", getIsIn())
            .append("inTime", getInTime())
            .append("outTime", getOutTime())
            .append("status", getStatus())
            .append("delFlag", getDelFlag())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .toString();
    }
}
