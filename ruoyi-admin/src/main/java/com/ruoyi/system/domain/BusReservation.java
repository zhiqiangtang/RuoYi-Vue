package com.ruoyi.system.domain;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 乘车预约记录对象 bus_reservation
 * 
 * @author ruoyi
 * @date 2023-09-16
 */
public class BusReservation extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** ID */
    private Long id;

    /**行驶记录*/
    private Long recordId;

    /** 预约人 */
    @Excel(name = "预约人")
    private Long userId;

    /** 预约时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "预约时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date reservationTime;

    /** 上车站点 */
    @Excel(name = "上车站点")
    private Long inSiteId;

    private String inSiteName;

    /** 下车站点 */
    @Excel(name = "下车站点")
    private Long outSiteId;

    private String outSiteName;

    /** 乘车状态（0已预约 1已上车 2已下车 3已取消），未取消的记录在预约时间3小时候后默认调整为已乘坐并已下车 */
    @Excel(name = "乘车状态", readConverterExp = "0=已预约,1=已上车,2=已下车,3=已取消")
    private String didingStatus;

    /** 状态（0正常 1停用） */
    @Excel(name = "状态", readConverterExp = "0=正常,1=停用")
    private String status;

    /** 删除标志（0代表存在 2代表删除） */
    private String delFlag;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setUserId(Long userId) 
    {
        this.userId = userId;
    }

    public Long getUserId() 
    {
        return userId;
    }
    public void setReservationTime(Date reservationTime) 
    {
        this.reservationTime = reservationTime;
    }

    public Date getReservationTime() 
    {
        return reservationTime;
    }
    public void setInSiteId(Long inSiteId) 
    {
        this.inSiteId = inSiteId;
    }

    public Long getInSiteId() 
    {
        return inSiteId;
    }
    public void setOutSiteId(Long outSiteId) 
    {
        this.outSiteId = outSiteId;
    }

    public Long getOutSiteId() 
    {
        return outSiteId;
    }
    public void setDidingStatus(String didingStatus) 
    {
        this.didingStatus = didingStatus;
    }

    public String getDidingStatus() 
    {
        return didingStatus;
    }
    public void setStatus(String status) 
    {
        this.status = status;
    }

    public String getStatus() 
    {
        return status;
    }
    public void setDelFlag(String delFlag) 
    {
        this.delFlag = delFlag;
    }

    public String getDelFlag() 
    {
        return delFlag;
    }

    public Long getRecordId() {
        return recordId;
    }

    public void setRecordId(Long recordId) {
        this.recordId = recordId;
    }

    public String getInSiteName() {
        return inSiteName;
    }

    public void setInSiteName(String inSiteName) {
        this.inSiteName = inSiteName;
    }

    public String getOutSiteName() {
        return outSiteName;
    }

    public void setOutSiteName(String outSiteName) {
        this.outSiteName = outSiteName;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("recordId", getRecordId())
            .append("userId", getUserId())
            .append("reservationTime", getReservationTime())
            .append("inSiteId", getInSiteId())
            .append("outSiteId", getOutSiteId())
            .append("didingStatus", getDidingStatus())
            .append("status", getStatus())
            .append("delFlag", getDelFlag())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .toString();
    }
}
