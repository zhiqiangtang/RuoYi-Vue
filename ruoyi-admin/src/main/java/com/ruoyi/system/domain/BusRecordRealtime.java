package com.ruoyi.system.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 校车行驶记录实时站点记录对象 bus_record_realtime
 * 
 * @author ruoyi
 * @date 2023-09-21
 */
public class BusRecordRealtime extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** ID */
    private Long id;

    /** 行驶记录 */
    @Excel(name = "行驶记录")
    private Long recordId;

    /** 路线 */
    @Excel(name = "路线")
    private Long routeId;

    /** 起始站点 */
    @Excel(name = "起始站点")
    private Long startSiteId;

    /** 起始坐标，车辆实时坐标 */
    @Excel(name = "起始坐标，车辆实时坐标")
    private String startCoordinate;

    /** 结束站点 */
    @Excel(name = "结束站点")
    private Long endSiteId;

    @Excel(name = "查询时间")
    private String searchTime;

    /** 距离 */
    @Excel(name = "距离")
    private String distance;

    /** 时间 */
    @Excel(name = "时间")
    private String inTime;

    /** 其它 */
    @Excel(name = "其它")
    private String otherMsg;

    /** 状态（0正常 1停用） */
    @Excel(name = "状态", readConverterExp = "0=正常,1=停用")
    private String status;

    /** 删除标志（0代表存在 2代表删除） */
    private String delFlag;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setRecordId(Long recordId) 
    {
        this.recordId = recordId;
    }

    public Long getRecordId() 
    {
        return recordId;
    }
    public void setRouteId(Long routeId) 
    {
        this.routeId = routeId;
    }

    public Long getRouteId() 
    {
        return routeId;
    }
    public void setStartSiteId(Long startSiteId) 
    {
        this.startSiteId = startSiteId;
    }

    public Long getStartSiteId() 
    {
        return startSiteId;
    }
    public void setStartCoordinate(String startCoordinate) 
    {
        this.startCoordinate = startCoordinate;
    }

    public String getStartCoordinate() 
    {
        return startCoordinate;
    }
    public void setEndSiteId(Long endSiteId) 
    {
        this.endSiteId = endSiteId;
    }

    public Long getEndSiteId() 
    {
        return endSiteId;
    }
    public void setDistance(String distance) 
    {
        this.distance = distance;
    }

    public String getDistance() 
    {
        return distance;
    }
    public void setInTime(String inTime) 
    {
        this.inTime = inTime;
    }

    public String getInTime() 
    {
        return inTime;
    }
    public void setOtherMsg(String otherMsg) 
    {
        this.otherMsg = otherMsg;
    }

    public String getOtherMsg() 
    {
        return otherMsg;
    }
    public void setStatus(String status) 
    {
        this.status = status;
    }

    public String getStatus() 
    {
        return status;
    }
    public void setDelFlag(String delFlag) 
    {
        this.delFlag = delFlag;
    }

    public String getDelFlag() 
    {
        return delFlag;
    }

    public String getSearchTime() {
        return searchTime;
    }

    public void setSearchTime(String searchTime) {
        this.searchTime = searchTime;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("recordId", getRecordId())
            .append("routeId", getRouteId())
            .append("startSiteId", getStartSiteId())
            .append("startCoordinate", getStartCoordinate())
            .append("endSiteId", getEndSiteId())
            .append("distance", getDistance())
            .append("inTime", getInTime())
            .append("otherMsg", getOtherMsg())
            .append("status", getStatus())
            .append("delFlag", getDelFlag())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .toString();
    }
}
