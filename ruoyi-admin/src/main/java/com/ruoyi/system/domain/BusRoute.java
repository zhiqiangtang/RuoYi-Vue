package com.ruoyi.system.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 路线对象 bus_route
 * 
 * @author ruoyi
 * @date 2023-09-16
 */
public class BusRoute extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** ID */
    private Long id;

    /** 路线名 */
    @Excel(name = "路线名")
    private String routeName;

    /** 起点 */
    @Excel(name = "起点")
    private Long startSiteId;

    private String startSiteName;

    /** 终点 */
    @Excel(name = "终点")
    private Long endSiteId;

    private String endSiteName;

    /** 正向路线，json */
    @Excel(name = "正向路线，json")
    private String forwardRoute;

    private String routeSiteArray;

    /** 反向路线，json */
    @Excel(name = "反向路线，json")
    private String reverseRoute;

    /** 状态（0正常 1停用） */
    @Excel(name = "状态", readConverterExp = "0=正常,1=停用")
    private String status;

    /** 删除标志（0代表存在 2代表删除） */
    private String delFlag;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setRouteName(String routeName) 
    {
        this.routeName = routeName;
    }

    public String getRouteName() 
    {
        return routeName;
    }
    public void setStartSiteId(Long startSiteId) 
    {
        this.startSiteId = startSiteId;
    }

    public Long getStartSiteId() 
    {
        return startSiteId;
    }
    public void setEndSiteId(Long endSiteId) 
    {
        this.endSiteId = endSiteId;
    }

    public Long getEndSiteId() 
    {
        return endSiteId;
    }
    public void setForwardRoute(String forwardRoute) 
    {
        this.forwardRoute = forwardRoute;
    }

    public String getForwardRoute() 
    {
        return forwardRoute;
    }
    public void setReverseRoute(String reverseRoute) 
    {
        this.reverseRoute = reverseRoute;
    }

    public String getReverseRoute() 
    {
        return reverseRoute;
    }
    public void setStatus(String status) 
    {
        this.status = status;
    }

    public String getStatus() 
    {
        return status;
    }


    public void setDelFlag(String delFlag)
    {
        this.delFlag = delFlag;
    }

    public String getDelFlag()
    {
        return delFlag;
    }


    public void setStartSiteName(String startSiteName)
    {
        this.startSiteName = startSiteName;
    }

    public String getStartSiteName()
    {
        return startSiteName;
    }
    public void setEndSiteName(String endSiteName)
    {
        this.endSiteName = endSiteName;
    }

    public String getEndSiteName()
    {
        return endSiteName;
    }

    public String getRouteSiteArray() {
        return routeSiteArray;
    }

    public void setRouteSiteArray(String routeSiteArray) {
        this.routeSiteArray = routeSiteArray;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("routeName", getRouteName())
            .append("startSiteId", getStartSiteId())
            .append("endSiteId", getEndSiteId())
            .append("forwardRoute", getForwardRoute())
            .append("reverseRoute", getReverseRoute())
            .append("status", getStatus())
            .append("delFlag", getDelFlag())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .toString();
    }
}
