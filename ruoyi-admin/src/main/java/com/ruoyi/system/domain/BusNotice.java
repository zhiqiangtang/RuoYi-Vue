package com.ruoyi.system.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 【请填写功能名称】对象 bus_notice
 *
 * @author ruoyi
 * @date 2023-10-08
 */
public class BusNotice extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    private Long id;

    /** 标题名 */
    @Excel(name = "标题名")
    private String titleName;

    /** 类型 1表示通知，0表示公告 */
    @Excel(name = "类型 1表示通知，0表示公告")
    private Long type;

    /** 文章 */
    @Excel(name = "文章")
    private String noticeContent;

    /** 状态 1表示已经发布，0表示下架 */
    @Excel(name = "状态 1表示已经发布，0表示下架")
    private Long status;

    /** 创建者 */
    @Excel(name = "创建者")
    private String createName;

    /** 1表示发布，0下架 */
    @Excel(name = "1表示发布，0下架")
    private Long operate;

    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getId()
    {
        return id;
    }
    public void setTitleName(String titleName)
    {
        this.titleName = titleName;
    }

    public String getTitleName()
    {
        return titleName;
    }
    public void setType(Long type)
    {
        this.type = type;
    }

    public Long getType()
    {
        return type;
    }
    public void setNoticeContent(String noticeContent)
    {
        this.noticeContent = noticeContent;
    }

    public String getNoticeContent()
    {
        return noticeContent;
    }
    public void setStatus(Long status)
    {
        this.status = status;
    }

    public Long getStatus()
    {
        return status;
    }
    public void setCreateName(String createName)
    {
        this.createName = createName;
    }

    public String getCreateName()
    {
        return createName;
    }
    public void setOperate(Long operate)
    {
        this.operate = operate;
    }

    public Long getOperate()
    {
        return operate;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
                .append("id", getId())
                .append("titleName", getTitleName())
                .append("type", getType())
                .append("noticeContent", getNoticeContent())
                .append("status", getStatus())
                .append("createName", getCreateName())
                .append("createTime", getCreateTime())
                .append("operate", getOperate())
                .append("CreateBy",getCreateBy())
                .toString();
    }
}