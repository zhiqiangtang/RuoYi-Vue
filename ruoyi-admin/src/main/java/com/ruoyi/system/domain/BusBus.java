package com.ruoyi.system.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;
import org.springframework.data.annotation.Transient;

/**
 * 校车对象 bus_bus
 * 
 * @author ruoyi
 * @date 2023-09-16
 */
public class BusBus extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** ID */
    private Long id;

    /** 校车名 */
    @Excel(name = "校车名")
    private String busName;

    /** 车牌号 */
    @Excel(name = "车牌号")
    private String plate;

    /** 司机 */
    @Excel(name = "司机")
    private Long driver;

    private String driverName;

    /** 座位数 */
    @Excel(name = "座位数")
    private Integer seating;

    /** 状态（0正常 1停用） */
    @Excel(name = "状态", readConverterExp = "0=正常,1=停用")
    private String status;

    /** 删除标志（0代表存在 2代表删除） */
    private String delFlag;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setBusName(String busName) 
    {
        this.busName = busName;
    }

    public String getBusName() 
    {
        return busName;
    }
    public void setPlate(String plate) 
    {
        this.plate = plate;
    }

    public String getPlate() 
    {
        return plate;
    }
    public void setDriver(Long driver) 
    {
        this.driver = driver;
    }

    public Long getDriver() 
    {
        return driver;
    }
    public void setSeating(Integer seating) 
    {
        this.seating = seating;
    }

    public Integer getSeating() 
    {
        return seating;
    }
    public void setStatus(String status) 
    {
        this.status = status;
    }

    public String getStatus() 
    {
        return status;
    }
    public void setDelFlag(String delFlag) 
    {
        this.delFlag = delFlag;
    }

    public String getDelFlag() 
    {
        return delFlag;
    }

    public String getDriverName(){
        return driverName;
    }
    public void setDriverName(String driverName){
        this.driverName = driverName;
    }


    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("busName", getBusName())
            .append("plate", getPlate())
            .append("driver", getDriver())
            .append("seating", getSeating())
            .append("status", getStatus())
            .append("delFlag", getDelFlag())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .toString();
    }
}
