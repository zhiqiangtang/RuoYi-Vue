package com.ruoyi.system.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.BusNoticeMapper;
import com.ruoyi.system.domain.BusNotice;
import com.ruoyi.system.service.IBusNoticeService;

/**
 * 【请填写功能名称】Service业务层处理
 *
 * @author ruoyi
 * @date 2023-10-08
 */
@Service
public class BusNoticeServiceImpl implements IBusNoticeService
{
    @Autowired
    private BusNoticeMapper busNoticeMapper;

    /**
     * 查询【请填写功能名称】
     *
     * @param id 【请填写功能名称】主键
     * @return 【请填写功能名称】
     */
    @Override
    public BusNotice selectBusNoticeById(Long id)
    {
        return busNoticeMapper.selectBusNoticeById(id);
    }

    /**
     * 查询【请填写功能名称】列表
     *
     * @param busNotice 【请填写功能名称】
     * @return 【请填写功能名称】
     */
    @Override
    public List<BusNotice> selectBusNoticeList(BusNotice busNotice)
    {
        return busNoticeMapper.selectBusNoticeList(busNotice);
    }

    /**
     * 新增【请填写功能名称】
     *
     * @param busNotice 【请填写功能名称】
     * @return 结果
     */
    @Override
    public int insertBusNotice(BusNotice busNotice)
    {
        busNotice.setCreateTime(DateUtils.getNowDate());  //获取创建的时间
        busNotice.setCreateTime(DateUtils.getNowDate());
        System.out.println("---->"+busNotice);
        return busNoticeMapper.insertBusNotice(busNotice);
    }

    /**
     * 修改【请填写功能名称】
     *
     * @param busNotice 【请填写功能名称】
     * @return 结果
     */
    @Override
    public int updateBusNotice(BusNotice busNotice)
    {
        return busNoticeMapper.updateBusNotice(busNotice);
    }

    /**
     * 批量删除【请填写功能名称】
     *
     * @param ids 需要删除的【请填写功能名称】主键
     * @return 结果
     */
    @Override
    public int deleteBusNoticeByIds(Long[] ids)
    {
        return busNoticeMapper.deleteBusNoticeByIds(ids);
    }

    /**
     * 删除【请填写功能名称】信息
     *
     * @param id 【请填写功能名称】主键
     * @return 结果
     */
    @Override
    public int deleteBusNoticeById(Long id)
    {
        return busNoticeMapper.deleteBusNoticeById(id);
    }


    /**
     * 发布公告
     * @param id 根据id 发布公告
     * @return
     */
    @Override
    public int noticesRelease(Integer id) {
        return busNoticeMapper.noticesRelease(id);
    }

    /**
     * 下架公告
     * @param id
     * @return
     */
    @Override
    public int noticesOffShelf(Integer id) {
        return busNoticeMapper.noticesOffShelf(id);
    }

    @Override
    public List<BusNotice> getNotice() {
        return busNoticeMapper.getNotice();
    }

    /**
     * 获取公告详情
     * @param id
     * @return
     */
    @Override
    public List<BusNotice> getNoticeDetails(Integer id) {
        return busNoticeMapper.getNoticeDetails(id);
    }
}
