package com.ruoyi.system.service;

import java.util.List;
import com.ruoyi.system.domain.BusBus;

/**
 * 校车Service接口
 * 
 * @author ruoyi
 * @date 2023-09-16
 */
public interface IBusBusService 
{
    /**
     * 查询校车
     * 
     * @param id 校车主键
     * @return 校车
     */
    public BusBus selectBusBusById(Long id);

    /**
     * 查询校车列表
     * 
     * @param busBus 校车
     * @return 校车集合
     */
    public List<BusBus> selectBusBusList(BusBus busBus);

    /**
     * 新增校车
     * 
     * @param busBus 校车
     * @return 结果
     */
    public int insertBusBus(BusBus busBus);

    /**
     * 修改校车
     * 
     * @param busBus 校车
     * @return 结果
     */
    public int updateBusBus(BusBus busBus);

    /**
     * 批量删除校车
     * 
     * @param ids 需要删除的校车主键集合
     * @return 结果
     */
    public int deleteBusBusByIds(Long[] ids);

    /**
     * 删除校车信息
     * 
     * @param id 校车主键
     * @return 结果
     */
    public int deleteBusBusById(Long id);
}
