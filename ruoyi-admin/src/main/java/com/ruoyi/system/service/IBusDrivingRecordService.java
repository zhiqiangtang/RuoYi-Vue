package com.ruoyi.system.service;

import java.util.List;
import com.ruoyi.system.domain.BusDrivingRecord;

/**
 * 校车行驶记录Service接口
 * 
 * @author ruoyi
 * @date 2023-09-16
 */
public interface IBusDrivingRecordService 
{
    /**
     * 查询校车行驶记录
     * 
     * @param id 校车行驶记录主键
     * @return 校车行驶记录
     */
    public BusDrivingRecord selectBusDrivingRecordById(Long id);

    /**
     * 查询校车行驶记录列表
     * 
     * @param busDrivingRecord 校车行驶记录
     * @return 校车行驶记录集合
     */
    public List<BusDrivingRecord> selectBusDrivingRecordList(BusDrivingRecord busDrivingRecord);

    /**
     * 新增校车行驶记录
     * 
     * @param busDrivingRecord 校车行驶记录
     * @return 结果
     */
    public int insertBusDrivingRecord(BusDrivingRecord busDrivingRecord);

    /**
     * 修改校车行驶记录
     * 
     * @param busDrivingRecord 校车行驶记录
     * @return 结果
     */
    public int updateBusDrivingRecord(BusDrivingRecord busDrivingRecord);

    /**
     * 批量删除校车行驶记录
     * 
     * @param ids 需要删除的校车行驶记录主键集合
     * @return 结果
     */
    public int deleteBusDrivingRecordByIds(Long[] ids);

    /**
     * 删除校车行驶记录信息
     * 
     * @param id 校车行驶记录主键
     * @return 结果
     */
    public int deleteBusDrivingRecordById(Long id);

    /**
     * 查询司机对应日期的行驶记录
     */
    public List<BusDrivingRecord> getRecordByDriverDate(Long userId,String date);
}
