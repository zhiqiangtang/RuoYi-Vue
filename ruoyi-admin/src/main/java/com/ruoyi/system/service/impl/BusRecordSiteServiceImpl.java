package com.ruoyi.system.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.alibaba.fastjson2.JSONArray;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.system.domain.BusRoute;
import com.ruoyi.system.domain.BusRouteSite;
import com.ruoyi.system.service.IBusRouteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.BusRecordSiteMapper;
import com.ruoyi.system.domain.BusRecordSite;
import com.ruoyi.system.service.IBusRecordSiteService;

/**
 * 校车行驶站点记录Service业务层处理
 * 
 * @author ruoyi
 * @date 2023-09-16
 */
@Service
public class BusRecordSiteServiceImpl implements IBusRecordSiteService 
{
    @Autowired
    private BusRecordSiteMapper busRecordSiteMapper;

    @Autowired
    private IBusRouteService iBusRouteService;

    /**
     * 查询校车行驶站点记录
     * 
     * @param id 校车行驶站点记录主键
     * @return 校车行驶站点记录
     */
    @Override
    public BusRecordSite selectBusRecordSiteById(Long id)
    {
        return busRecordSiteMapper.selectBusRecordSiteById(id);
    }

    /**
     * 查询校车行驶站点记录列表
     * 
     * @param busRecordSite 校车行驶站点记录
     * @return 校车行驶站点记录
     */
    @Override
    public List<BusRecordSite> selectBusRecordSiteList(BusRecordSite busRecordSite)
    {
        return busRecordSiteMapper.selectBusRecordSiteList(busRecordSite);
    }

    /**
     * 新增校车行驶站点记录
     * 
     * @param busRecordSite 校车行驶站点记录
     * @return 结果
     */
    @Override
    public int insertBusRecordSite(BusRecordSite busRecordSite)
    {
        busRecordSite.setCreateTime(DateUtils.getNowDate());
        return busRecordSiteMapper.insertBusRecordSite(busRecordSite);
    }

    /**
     * 修改校车行驶站点记录
     * 
     * @param busRecordSite 校车行驶站点记录
     * @return 结果
     */
    @Override
    public int updateBusRecordSite(BusRecordSite busRecordSite)
    {
        busRecordSite.setUpdateTime(DateUtils.getNowDate());
        return busRecordSiteMapper.updateBusRecordSite(busRecordSite);
    }

    /**
     * 批量删除校车行驶站点记录
     * 
     * @param ids 需要删除的校车行驶站点记录主键
     * @return 结果
     */
    @Override
    public int deleteBusRecordSiteByIds(Long[] ids)
    {
        return busRecordSiteMapper.deleteBusRecordSiteByIds(ids);
    }

    /**
     * 删除校车行驶站点记录信息
     * 
     * @param id 校车行驶站点记录主键
     * @return 结果
     */
    @Override
    public int deleteBusRecordSiteById(Long id)
    {
        return busRecordSiteMapper.deleteBusRecordSiteById(id);
    }

    @Override
    public void addRecordSites(Long recordId,Long routeId,int direction) {
        //已存在记录时先删除记录
        BusRecordSite searchSite = new BusRecordSite();
        searchSite.setRecordId(recordId);
        List<BusRecordSite> list = this.busRecordSiteMapper.selectBusRecordSiteList(searchSite);
        for(BusRecordSite site: list){
            this.busRecordSiteMapper.deleteBusRecordSiteById(site.getId());
        }

        BusRoute route = iBusRouteService.selectBusRouteById(routeId);
        String routeSite = route.getForwardRoute();
        if(direction == 1){
            routeSite = route.getReverseRoute();
        }
        JSONArray siteArray = JSONArray.parseArray(routeSite);
        for(Object siteStr: siteArray){
            BusRecordSite recordSite = new BusRecordSite();
            recordSite.setRecordId(recordId);
            recordSite.setRouteId(routeId);
            recordSite.setSiteId(Long.valueOf(siteStr.toString()));
            recordSite.setIsIn(0);
            recordSite.setStatus("0");
            this.insertBusRecordSite(recordSite);
        }
    }

    @Override
    public Map<Long, BusRecordSite> selectBusRecordSiteMap(Long recordId) {
        BusRecordSite busRecordSite = new BusRecordSite();
        busRecordSite.setRecordId(recordId);
        List<BusRecordSite> list = this.selectBusRecordSiteList(busRecordSite);
        Map<Long, BusRecordSite> map = new HashMap<>();
        for(BusRecordSite site: list){
            map.put(site.getSiteId(),site);
        }
        return map;
    }
}
