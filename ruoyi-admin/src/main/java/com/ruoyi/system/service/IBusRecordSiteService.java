package com.ruoyi.system.service;

import java.util.List;
import java.util.Map;

import com.ruoyi.system.domain.BusRecordSite;

/**
 * 校车行驶站点记录Service接口
 * 
 * @author ruoyi
 * @date 2023-09-16
 */
public interface IBusRecordSiteService 
{
    /**
     * 查询校车行驶站点记录
     * 
     * @param id 校车行驶站点记录主键
     * @return 校车行驶站点记录
     */
    public BusRecordSite selectBusRecordSiteById(Long id);

    /**
     * 查询校车行驶站点记录列表
     * 
     * @param busRecordSite 校车行驶站点记录
     * @return 校车行驶站点记录集合
     */
    public List<BusRecordSite> selectBusRecordSiteList(BusRecordSite busRecordSite);

    /**
     * 新增校车行驶站点记录
     * 
     * @param busRecordSite 校车行驶站点记录
     * @return 结果
     */
    public int insertBusRecordSite(BusRecordSite busRecordSite);

    /**
     * 修改校车行驶站点记录
     * 
     * @param busRecordSite 校车行驶站点记录
     * @return 结果
     */
    public int updateBusRecordSite(BusRecordSite busRecordSite);

    /**
     * 批量删除校车行驶站点记录
     * 
     * @param ids 需要删除的校车行驶站点记录主键集合
     * @return 结果
     */
    public int deleteBusRecordSiteByIds(Long[] ids);

    /**
     * 删除校车行驶站点记录信息
     * 
     * @param id 校车行驶站点记录主键
     * @return 结果
     */
    public int deleteBusRecordSiteById(Long id);

    /**
     * 增加行驶记录下所有站点的记录
     */
    public void addRecordSites(Long recordId,Long routeId,int direction);

    public Map<Long,BusRecordSite> selectBusRecordSiteMap(Long recordId);
}
