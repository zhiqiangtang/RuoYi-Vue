package com.ruoyi.system.service;

import java.util.List;
import com.ruoyi.system.domain.BusNotice;

/**
 * 【请填写功能名称】Service接口
 *
 * @author ruoyi
 * @date 2023-10-08
 */
public interface IBusNoticeService
{
    /**
     * 查询【请填写功能名称】
     *
     * @param id 【请填写功能名称】主键
     * @return 【请填写功能名称】
     */
    public BusNotice selectBusNoticeById(Long id);

    /**
     * 查询【请填写功能名称】列表
     *
     * @param busNotice 【请填写功能名称】
     * @return 【请填写功能名称】集合
     */
    public List<BusNotice> selectBusNoticeList(BusNotice busNotice);

    /**
     * 新增【请填写功能名称】
     *
     * @param busNotice 【请填写功能名称】
     * @return 结果
     */
    public int insertBusNotice(BusNotice busNotice);

    /**
     * 修改【请填写功能名称】
     *
     * @param busNotice 【请填写功能名称】
     * @return 结果
     */
    public int updateBusNotice(BusNotice busNotice);

    /**
     * 批量删除【请填写功能名称】
     *
     * @param ids 需要删除的【请填写功能名称】主键集合
     * @return 结果
     */
    public int deleteBusNoticeByIds(Long[] ids);

    /**
     * 删除【请填写功能名称】信息
     *
     * @param id 【请填写功能名称】主键
     * @return 结果
     */
    public int deleteBusNoticeById(Long id);


    int noticesRelease(Integer id);

//    下架
    int noticesOffShelf(Integer id);

    //获取发布公告
    List<BusNotice> getNotice();

    //获取公告详情
    List<BusNotice> getNoticeDetails(Integer id);
}
