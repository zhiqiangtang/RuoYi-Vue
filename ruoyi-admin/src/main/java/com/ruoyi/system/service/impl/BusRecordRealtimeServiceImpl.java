package com.ruoyi.system.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.BusRecordRealtimeMapper;
import com.ruoyi.system.domain.BusRecordRealtime;
import com.ruoyi.system.service.IBusRecordRealtimeService;

/**
 * 校车行驶记录实时站点记录Service业务层处理
 * 
 * @author ruoyi
 * @date 2023-09-21
 */
@Service
public class BusRecordRealtimeServiceImpl implements IBusRecordRealtimeService 
{
    @Autowired
    private BusRecordRealtimeMapper busRecordRealtimeMapper;

    /**
     * 查询校车行驶记录实时站点记录
     * 
     * @param id 校车行驶记录实时站点记录主键
     * @return 校车行驶记录实时站点记录
     */
    @Override
    public BusRecordRealtime selectBusRecordRealtimeById(Long id)
    {
        return busRecordRealtimeMapper.selectBusRecordRealtimeById(id);
    }

    /**
     * 查询校车行驶记录实时站点记录列表
     * 
     * @param busRecordRealtime 校车行驶记录实时站点记录
     * @return 校车行驶记录实时站点记录
     */
    @Override
    public List<BusRecordRealtime> selectBusRecordRealtimeList(BusRecordRealtime busRecordRealtime)
    {
        return busRecordRealtimeMapper.selectBusRecordRealtimeList(busRecordRealtime);
    }

    /**
     * 新增校车行驶记录实时站点记录
     * 
     * @param busRecordRealtime 校车行驶记录实时站点记录
     * @return 结果
     */
    @Override
    public int insertBusRecordRealtime(BusRecordRealtime busRecordRealtime)
    {
        busRecordRealtime.setCreateTime(DateUtils.getNowDate());
        return busRecordRealtimeMapper.insertBusRecordRealtime(busRecordRealtime);
    }

    /**
     * 修改校车行驶记录实时站点记录
     * 
     * @param busRecordRealtime 校车行驶记录实时站点记录
     * @return 结果
     */
    @Override
    public int updateBusRecordRealtime(BusRecordRealtime busRecordRealtime)
    {
        busRecordRealtime.setUpdateTime(DateUtils.getNowDate());
        return busRecordRealtimeMapper.updateBusRecordRealtime(busRecordRealtime);
    }

    /**
     * 批量删除校车行驶记录实时站点记录
     * 
     * @param ids 需要删除的校车行驶记录实时站点记录主键
     * @return 结果
     */
    @Override
    public int deleteBusRecordRealtimeByIds(Long[] ids)
    {
        return busRecordRealtimeMapper.deleteBusRecordRealtimeByIds(ids);
    }

    /**
     * 删除校车行驶记录实时站点记录信息
     * 
     * @param id 校车行驶记录实时站点记录主键
     * @return 结果
     */
    @Override
    public int deleteBusRecordRealtimeById(Long id)
    {
        return busRecordRealtimeMapper.deleteBusRecordRealtimeById(id);
    }

    @Override
    public Map<String, BusRecordRealtime> getRouteReachSite(Long recordId) {
        //先查出最新批次所有站点的记录
        List<BusRecordRealtime> recordRealtimes = this.busRecordRealtimeMapper.selectRealTimeList(recordId);
        Map<String, BusRecordRealtime> recordMap = new HashMap<>();
        for(BusRecordRealtime realtime: recordRealtimes){
            String key = (realtime.getStartSiteId()==null ? "" : realtime.getStartSiteId()) + "-" + realtime.getEndSiteId();
            recordMap.put(key,realtime);
        }
        return recordMap;
    }
}
