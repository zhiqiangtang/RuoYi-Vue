package com.ruoyi.system.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.BusReservationMapper;
import com.ruoyi.system.domain.BusReservation;
import com.ruoyi.system.service.IBusReservationService;

/**
 * 乘车预约记录Service业务层处理
 * 
 * @author ruoyi
 * @date 2023-09-16
 */
@Service
public class BusReservationServiceImpl implements IBusReservationService 
{
    @Autowired
    private BusReservationMapper busReservationMapper;

    /**
     * 查询乘车预约记录
     * 
     * @param id 乘车预约记录主键
     * @return 乘车预约记录
     */
    @Override
    public BusReservation selectBusReservationById(Long id)
    {
        return busReservationMapper.selectBusReservationById(id);
    }

    /**
     * 查询乘车预约记录列表
     * 
     * @param busReservation 乘车预约记录
     * @return 乘车预约记录
     */
    @Override
    public List<BusReservation> selectBusReservationList(BusReservation busReservation)
    {
        return busReservationMapper.selectBusReservationList(busReservation);
    }

    /**
     * 新增乘车预约记录
     * 
     * @param busReservation 乘车预约记录
     * @return 结果
     */
    @Override
    public int insertBusReservation(BusReservation busReservation)
    {
        busReservation.setCreateTime(DateUtils.getNowDate());
        return busReservationMapper.insertBusReservation(busReservation);
    }

    /**
     * 修改乘车预约记录
     * 
     * @param busReservation 乘车预约记录
     * @return 结果
     */
    @Override
    public int updateBusReservation(BusReservation busReservation)
    {
        busReservation.setUpdateTime(DateUtils.getNowDate());
        return busReservationMapper.updateBusReservation(busReservation);
    }

    /**
     * 批量删除乘车预约记录
     * 
     * @param ids 需要删除的乘车预约记录主键
     * @return 结果
     */
    @Override
    public int deleteBusReservationByIds(Long[] ids)
    {
        return busReservationMapper.deleteBusReservationByIds(ids);
    }

    /**
     * 删除乘车预约记录信息
     * 
     * @param id 乘车预约记录主键
     * @return 结果
     */
    @Override
    public int deleteBusReservationById(Long id)
    {
        return busReservationMapper.deleteBusReservationById(id);
    }

    @Override
    public Integer[] getSiteInOutCount(Long recordId,Long siteId) {
        Integer[] count = new Integer[]{0,0};
        BusReservation busReservation = new BusReservation();
        busReservation.setRecordId(recordId);
        busReservation.setInSiteId(siteId);
        List<BusReservation> inList = this.selectBusReservationList(busReservation);
        if(inList.size() > 0){
            count[0] = inList.size();
        }
        busReservation.setInSiteId(null);
        busReservation.setOutSiteId(siteId);
        List<BusReservation> outList = this.selectBusReservationList(busReservation);
        if(outList.size() > 0){
            count[1] = outList.size();
        }
        return count;
    }

}
