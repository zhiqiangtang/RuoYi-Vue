package com.ruoyi.system.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.BusRouteSiteMapper;
import com.ruoyi.system.domain.BusRouteSite;
import com.ruoyi.system.service.IBusRouteSiteService;

/**
 * 站点Service业务层处理
 * 
 * @author ruoyi
 * @date 2023-09-16
 */
@Service
public class BusRouteSiteServiceImpl implements IBusRouteSiteService 
{
    @Autowired
    private BusRouteSiteMapper busRouteSiteMapper;

    /**
     * 查询站点
     * 
     * @param id 站点主键
     * @return 站点
     */
    @Override
    public BusRouteSite selectBusRouteSiteById(Long id)
    {
        return busRouteSiteMapper.selectBusRouteSiteById(id);
    }

    /**
     * 查询站点列表
     * 
     * @param busRouteSite 站点
     * @return 站点
     */
    @Override
    public List<BusRouteSite> selectBusRouteSiteList(BusRouteSite busRouteSite)
    {
        return busRouteSiteMapper.selectBusRouteSiteList(busRouteSite);
    }

    /**
     * 新增站点
     * 
     * @param busRouteSite 站点
     * @return 结果
     */
    @Override
    public int insertBusRouteSite(BusRouteSite busRouteSite)
    {
        busRouteSite.setCreateTime(DateUtils.getNowDate());
        return busRouteSiteMapper.insertBusRouteSite(busRouteSite);
    }

    /**
     * 修改站点
     * 
     * @param busRouteSite 站点
     * @return 结果
     */
    @Override
    public int updateBusRouteSite(BusRouteSite busRouteSite)
    {
        busRouteSite.setUpdateTime(DateUtils.getNowDate());
        return busRouteSiteMapper.updateBusRouteSite(busRouteSite);
    }

    /**
     * 批量删除站点
     * 
     * @param ids 需要删除的站点主键
     * @return 结果
     */
    @Override
    public int deleteBusRouteSiteByIds(Long[] ids)
    {
        return busRouteSiteMapper.deleteBusRouteSiteByIds(ids);
    }

    /**
     * 删除站点信息
     * 
     * @param id 站点主键
     * @return 结果
     */
    @Override
    public int deleteBusRouteSiteById(Long id)
    {
        return busRouteSiteMapper.deleteBusRouteSiteById(id);
    }
}
