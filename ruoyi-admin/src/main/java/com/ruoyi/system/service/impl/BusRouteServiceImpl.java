package com.ruoyi.system.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.BusRouteMapper;
import com.ruoyi.system.domain.BusRoute;
import com.ruoyi.system.service.IBusRouteService;

/**
 * 路线Service业务层处理
 * 
 * @author ruoyi
 * @date 2023-09-16
 */
@Service
public class BusRouteServiceImpl implements IBusRouteService 
{
    @Autowired
    private BusRouteMapper busRouteMapper;

    /**
     * 查询路线
     * 
     * @param id 路线主键
     * @return 路线
     */
    @Override
    public BusRoute selectBusRouteById(Long id)
    {
        return busRouteMapper.selectBusRouteById(id);
    }

    /**
     * 查询路线列表
     * 
     * @param busRoute 路线
     * @return 路线
     */
    @Override
    public List<BusRoute> selectBusRouteList(BusRoute busRoute)
    {
        return busRouteMapper.selectBusRouteList(busRoute);
    }

    /**
     * 新增路线
     * 
     * @param busRoute 路线
     * @return 结果
     */
    @Override
    public int insertBusRoute(BusRoute busRoute)
    {
        busRoute.setCreateTime(DateUtils.getNowDate());
        return busRouteMapper.insertBusRoute(busRoute);
    }

    /**
     * 修改路线
     * 
     * @param busRoute 路线
     * @return 结果
     */
    @Override
    public int updateBusRoute(BusRoute busRoute)
    {
        busRoute.setUpdateTime(DateUtils.getNowDate());
        return busRouteMapper.updateBusRoute(busRoute);
    }

    /**
     * 批量删除路线
     * 
     * @param ids 需要删除的路线主键
     * @return 结果
     */
    @Override
    public int deleteBusRouteByIds(Long[] ids)
    {
        return busRouteMapper.deleteBusRouteByIds(ids);
    }

    /**
     * 删除路线信息
     * 
     * @param id 路线主键
     * @return 结果
     */
    @Override
    public int deleteBusRouteById(Long id)
    {
        return busRouteMapper.deleteBusRouteById(id);
    }
}
