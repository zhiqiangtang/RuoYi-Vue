package com.ruoyi.system.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.system.service.IBusRecordSiteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.BusDrivingRecordMapper;
import com.ruoyi.system.domain.BusDrivingRecord;
import com.ruoyi.system.service.IBusDrivingRecordService;

/**
 * 校车行驶记录Service业务层处理
 * 
 * @author ruoyi
 * @date 2023-09-16
 */
@Service
public class BusDrivingRecordServiceImpl implements IBusDrivingRecordService 
{
    @Autowired
    private BusDrivingRecordMapper busDrivingRecordMapper;

    @Autowired
    private IBusRecordSiteService iBusRecordSiteService;

    /**
     * 查询校车行驶记录
     * 
     * @param id 校车行驶记录主键
     * @return 校车行驶记录
     */
    @Override
    public BusDrivingRecord selectBusDrivingRecordById(Long id)
    {
        return busDrivingRecordMapper.selectBusDrivingRecordById(id);
    }

    /**
     * 查询校车行驶记录列表
     * 
     * @param busDrivingRecord 校车行驶记录
     * @return 校车行驶记录
     */
    @Override
    public List<BusDrivingRecord> selectBusDrivingRecordList(BusDrivingRecord busDrivingRecord)
    {
        return busDrivingRecordMapper.selectBusDrivingRecordList(busDrivingRecord);
    }

    /**
     * 新增校车行驶记录
     * 
     * @param busDrivingRecord 校车行驶记录
     * @return 结果
     */
    @Override
    public int insertBusDrivingRecord(BusDrivingRecord busDrivingRecord)
    {
        busDrivingRecord.setCreateTime(DateUtils.getNowDate());
        int back = busDrivingRecordMapper.insertBusDrivingRecord(busDrivingRecord);
        //产生每个站点的记录
        iBusRecordSiteService.addRecordSites(busDrivingRecord.getId(),busDrivingRecord.getRouteId(),busDrivingRecord.getDirection());
        return back;
    }

    /**
     * 修改校车行驶记录
     * 
     * @param busDrivingRecord 校车行驶记录
     * @return 结果
     */
    @Override
    public int updateBusDrivingRecord(BusDrivingRecord busDrivingRecord)
    {
        busDrivingRecord.setUpdateTime(DateUtils.getNowDate());
        int back = busDrivingRecordMapper.updateBusDrivingRecord(busDrivingRecord);
        //产生每个站点的记录
        iBusRecordSiteService.addRecordSites(busDrivingRecord.getId(),busDrivingRecord.getRouteId(),busDrivingRecord.getDirection());
        return back;
    }

    /**
     * 批量删除校车行驶记录
     * 
     * @param ids 需要删除的校车行驶记录主键
     * @return 结果
     */
    @Override
    public int deleteBusDrivingRecordByIds(Long[] ids)
    {
        return busDrivingRecordMapper.deleteBusDrivingRecordByIds(ids);
    }

    /**
     * 删除校车行驶记录信息
     * 
     * @param id 校车行驶记录主键
     * @return 结果
     */
    @Override
    public int deleteBusDrivingRecordById(Long id)
    {
        return busDrivingRecordMapper.deleteBusDrivingRecordById(id);
    }

    @Override
    public List<BusDrivingRecord> getRecordByDriverDate(Long userId, String date) {
        BusDrivingRecord busDrivingRecord = new BusDrivingRecord();
        busDrivingRecord.setDriver(userId);
        busDrivingRecord.setRecordDate(date);
        return this.selectBusDrivingRecordList(busDrivingRecord);
    }
}
