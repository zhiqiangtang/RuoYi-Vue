package com.ruoyi.system.service;

import java.util.List;
import com.ruoyi.system.domain.BusRouteSite;

/**
 * 站点Service接口
 * 
 * @author ruoyi
 * @date 2023-09-16
 */
public interface IBusRouteSiteService 
{
    /**
     * 查询站点
     * 
     * @param id 站点主键
     * @return 站点
     */
    public BusRouteSite selectBusRouteSiteById(Long id);

    /**
     * 查询站点列表
     * 
     * @param busRouteSite 站点
     * @return 站点集合
     */
    public List<BusRouteSite> selectBusRouteSiteList(BusRouteSite busRouteSite);

    /**
     * 新增站点
     * 
     * @param busRouteSite 站点
     * @return 结果
     */
    public int insertBusRouteSite(BusRouteSite busRouteSite);

    /**
     * 修改站点
     * 
     * @param busRouteSite 站点
     * @return 结果
     */
    public int updateBusRouteSite(BusRouteSite busRouteSite);

    /**
     * 批量删除站点
     * 
     * @param ids 需要删除的站点主键集合
     * @return 结果
     */
    public int deleteBusRouteSiteByIds(Long[] ids);

    /**
     * 删除站点信息
     * 
     * @param id 站点主键
     * @return 结果
     */
    public int deleteBusRouteSiteById(Long id);
}
