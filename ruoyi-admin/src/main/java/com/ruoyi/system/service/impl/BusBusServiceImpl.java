package com.ruoyi.system.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.BusBusMapper;
import com.ruoyi.system.domain.BusBus;
import com.ruoyi.system.service.IBusBusService;

/**
 * 校车Service业务层处理
 * 
 * @author ruoyi
 * @date 2023-09-16
 */
@Service
public class BusBusServiceImpl implements IBusBusService 
{
    @Autowired
    private BusBusMapper busBusMapper;

    /**
     * 查询校车
     * 
     * @param id 校车主键
     * @return 校车
     */
    @Override
    public BusBus selectBusBusById(Long id)
    {
        return busBusMapper.selectBusBusById(id);
    }

    /**
     * 查询校车列表
     * 
     * @param busBus 校车
     * @return 校车
     */
    @Override
    public List<BusBus> selectBusBusList(BusBus busBus)
    {
        return busBusMapper.selectBusBusList(busBus);
    }

    /**
     * 新增校车
     * 
     * @param busBus 校车
     * @return 结果
     */
    @Override
    public int insertBusBus(BusBus busBus)
    {
        busBus.setCreateTime(DateUtils.getNowDate());
        return busBusMapper.insertBusBus(busBus);
    }

    /**
     * 修改校车
     * 
     * @param busBus 校车
     * @return 结果
     */
    @Override
    public int updateBusBus(BusBus busBus)
    {
        busBus.setUpdateTime(DateUtils.getNowDate());
        return busBusMapper.updateBusBus(busBus);
    }

    /**
     * 批量删除校车
     * 
     * @param ids 需要删除的校车主键
     * @return 结果
     */
    @Override
    public int deleteBusBusByIds(Long[] ids)
    {
        return busBusMapper.deleteBusBusByIds(ids);
    }

    /**
     * 删除校车信息
     * 
     * @param id 校车主键
     * @return 结果
     */
    @Override
    public int deleteBusBusById(Long id)
    {
        return busBusMapper.deleteBusBusById(id);
    }
}
