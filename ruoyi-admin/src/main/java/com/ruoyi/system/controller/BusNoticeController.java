package com.ruoyi.system.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.BusNotice;
import com.ruoyi.system.service.IBusNoticeService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 【请填写功能名称】Controller
 *
 * @author ruoyi
 * @date 2023-10-08
 */
@RestController
@RequestMapping("/system/notices")
public class BusNoticeController extends BaseController
{
    @Autowired
    private IBusNoticeService busNoticeService;

    /**
     * 查询【请填写功能名称】列表
     */
    @PreAuthorize("@ss.hasPermi('system:notices:list')")
    @GetMapping("/list")
    public TableDataInfo list(BusNotice busNotice)
    {
        startPage();
        List<BusNotice> list = busNoticeService.selectBusNoticeList(busNotice);
        return getDataTable(list);
    }

    /**
     * 导出【请填写功能名称】列表
     */
    @PreAuthorize("@ss.hasPermi('system:notices:export')")
    @Log(title = "【请填写功能名称】", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, BusNotice busNotice)
    {
        List<BusNotice> list = busNoticeService.selectBusNoticeList(busNotice);
        ExcelUtil<BusNotice> util = new ExcelUtil<BusNotice>(BusNotice.class);
        util.exportExcel(response, list, "【请填写功能名称】数据");
    }

    /**
     * 获取【请填写功能名称】详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:notices:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(busNoticeService.selectBusNoticeById(id));
    }


    /**
     * 新增公告
     */
    @PreAuthorize("@ss.hasPermi('system:notices:add')")
    @Log(title = "新增公告", businessType = BusinessType.INSERT)
    @GetMapping
    public AjaxResult add(BusNotice busNotice)
    {
        return toAjax(busNoticeService.insertBusNotice(busNotice));
    }


    /**
     * 修改功能
     */
    @PreAuthorize("@ss.hasPermi('system:notices:edit')")
    @Log(title = "【修改功能】", businessType = BusinessType.UPDATE)
    @GetMapping("/edit")
    public AjaxResult edit(BusNotice busNotice)
    {
        System.out.println(busNotice);
        return toAjax(busNoticeService.updateBusNotice(busNotice));
    }

    /**
     * 删除
     */
    @PreAuthorize("@ss.hasPermi('system:notices:remove')")
    @Log(title = "【删除功能】", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(busNoticeService.deleteBusNoticeByIds(ids));
    }


    @PreAuthorize("@ss.hasPermi('system:notices:noticesRelease')")
    @Log(title = "【发布广告】", businessType = BusinessType.DELETE)
    @GetMapping("/noticesRelease/{id}")
    public AjaxResult noticesRelease(@PathVariable Integer id)
    {
        return toAjax(busNoticeService.noticesRelease(id));
    }


    @PreAuthorize("@ss.hasPermi('system:notices:noticesOffShelf')")
    @Log(title = "【下架广告】", businessType = BusinessType.DELETE)
    @GetMapping("/noticesOffShelf/{id}")
    public AjaxResult noticesOffShelf(@PathVariable Integer id)
    {
        return toAjax(busNoticeService.noticesOffShelf(id));
    }


    /**
     * 获取发布的公告
     * @param
     * @return
     */
    @PreAuthorize("@ss.hasPermi('system:notices:getNotice')")
    @Log(title = "【获取发布公告】", businessType = BusinessType.DELETE)
    @GetMapping("/getNotice")
    public AjaxResult getNotice()
    {
        List<BusNotice> notice = busNoticeService.getNotice();
        return success(notice);
    }

    /**
     * 根据id获取公告中的详情信息
     * @return
     */
    @PreAuthorize("@ss.hasPermi('system:notices:getNoticeDetails')")
    @Log(title = "【获取公告性情信息】", businessType = BusinessType.DELETE)
    @GetMapping("/getNoticeDetails/{id}")
    public AjaxResult getNoticeDetails(@PathVariable("id") Integer id)
    {
        List<BusNotice> notice = busNoticeService.getNoticeDetails(id);

        return success(notice);
    }
}
