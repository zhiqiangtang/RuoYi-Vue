package com.ruoyi.system.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.BusRecordSite;
import com.ruoyi.system.service.IBusRecordSiteService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 校车行驶站点记录Controller
 * 
 * @author ruoyi
 * @date 2023-09-16
 */
@RestController
@RequestMapping("/system/recordSite")
public class BusRecordSiteController extends BaseController
{
    @Autowired
    private IBusRecordSiteService busRecordSiteService;

    /**
     * 查询校车行驶站点记录列表
     */
    @PreAuthorize("@ss.hasPermi('system:recordSite:list')")
    @GetMapping("/list")
    public TableDataInfo list(BusRecordSite busRecordSite)
    {
        startPage();
        List<BusRecordSite> list = busRecordSiteService.selectBusRecordSiteList(busRecordSite);
        return getDataTable(list);
    }

    /**
     * 导出校车行驶站点记录列表
     */
    @PreAuthorize("@ss.hasPermi('system:recordSite:export')")
    @Log(title = "校车行驶站点记录", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, BusRecordSite busRecordSite)
    {
        List<BusRecordSite> list = busRecordSiteService.selectBusRecordSiteList(busRecordSite);
        ExcelUtil<BusRecordSite> util = new ExcelUtil<BusRecordSite>(BusRecordSite.class);
        util.exportExcel(response, list, "校车行驶站点记录数据");
    }

    /**
     * 获取校车行驶站点记录详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:recordSite:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(busRecordSiteService.selectBusRecordSiteById(id));
    }

    /**
     * 新增校车行驶站点记录
     */
    @PreAuthorize("@ss.hasPermi('system:recordSite:add')")
    @Log(title = "校车行驶站点记录", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody BusRecordSite busRecordSite)
    {
        return toAjax(busRecordSiteService.insertBusRecordSite(busRecordSite));
    }

    /**
     * 修改校车行驶站点记录
     */
    @Log(title = "校车行驶站点记录", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody BusRecordSite busRecordSite)
    {
        return toAjax(busRecordSiteService.updateBusRecordSite(busRecordSite));
    }

    /**
     * 删除校车行驶站点记录
     */
    @PreAuthorize("@ss.hasPermi('system:recordSite:remove')")
    @Log(title = "校车行驶站点记录", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(busRecordSiteService.deleteBusRecordSiteByIds(ids));
    }
}
