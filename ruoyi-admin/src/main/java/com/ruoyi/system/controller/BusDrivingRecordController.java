package com.ruoyi.system.controller;

import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletResponse;

import com.alibaba.fastjson2.JSONArray;
import com.alibaba.fastjson2.JSONObject;
import com.ruoyi.common.core.domain.entity.SysUser;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.common.utils.SecurityUtils;
import com.ruoyi.system.domain.*;
import com.ruoyi.system.service.*;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 校车行驶记录Controller
 * 
 * @author ruoyi
 * @date 2023-09-16
 */
@RestController
@RequestMapping("/system/record")
public class BusDrivingRecordController extends BaseController
{
    @Autowired
    private IBusDrivingRecordService busDrivingRecordService;


    @Autowired
    private IBusBusService iBusBusService;

    @Autowired
    private IBusRouteService iBusRouteService;

    @Autowired
    private ISysUserService iSysUserService;
    @Autowired
    private IBusRouteService busRouteService;
    @Autowired
    private IBusRouteSiteService busRouteSiteService;

    @Autowired
    private IBusReservationService iBusReservationService;

    @Autowired
    private IBusRecordRealtimeService iBusRecordRealtimeService;

    @Autowired
    private IBusRecordSiteService iBusRecordSiteService;


    /**
     * 查询校车行驶记录列表
     */
    @PreAuthorize("@ss.hasPermi('system:record:list')")
    @GetMapping("/list")
    public TableDataInfo list(BusDrivingRecord busDrivingRecord)
    {
        startPage();
        List<BusDrivingRecord> list = busDrivingRecordService.selectBusDrivingRecordList(busDrivingRecord);
        for(BusDrivingRecord record: list){
            record.setBusName(iBusBusService.selectBusBusById(record.getBusId()).getBusName());
            record.setRouteName(iBusRouteService.selectBusRouteById(record.getRouteId()).getRouteName());
            SysUser user = iSysUserService.selectUserById(record.getDriver());
            record.setDriverName(user.getNickName());
            record.setPhonenumber(user.getPhonenumber());
        }
        return getDataTable(list);
    }

    /**
     * 按日期查询在运行线路
     */
    @GetMapping("/appList")
    public TableDataInfo appList(String recordDate)
    {

        Long userId = SecurityUtils.getUserId();
        //司机的开车记录
        List<BusDrivingRecord> list = busDrivingRecordService.getRecordByDriverDate(userId,recordDate);
        if(list.size() > 0){
        }else{
            //乘客看在运行的所有记录
            BusDrivingRecord busDrivingRecord = new BusDrivingRecord();
            busDrivingRecord.setRecordDate(recordDate);
            list = busDrivingRecordService.selectBusDrivingRecordList(busDrivingRecord);
        }
        for(BusDrivingRecord record: list){
            BusBus bus =iBusBusService.selectBusBusById(record.getBusId());
            record.setBusName(bus.getBusName());
            record.setBusNumber(bus.getPlate());
            record.setRouteName(iBusRouteService.selectBusRouteById(record.getRouteId()).getRouteName());
            SysUser user = iSysUserService.selectUserById(record.getDriver());
            record.setDriverName(user.getNickName());
            record.setPhonenumber(user.getPhonenumber());
        }
        return getDataTable(list);
    }

    /**
     * 导出校车行驶记录列表
     */
    @PreAuthorize("@ss.hasPermi('system:record:export')")
    @Log(title = "校车行驶记录", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, BusDrivingRecord busDrivingRecord)
    {
        List<BusDrivingRecord> list = busDrivingRecordService.selectBusDrivingRecordList(busDrivingRecord);
        ExcelUtil<BusDrivingRecord> util = new ExcelUtil<BusDrivingRecord>(BusDrivingRecord.class);
        util.exportExcel(response, list, "校车行驶记录数据");
    }

    /**
     * 获取校车行驶记录详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:record:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        BusDrivingRecord record = busDrivingRecordService.selectBusDrivingRecordById(id);
        record.setBusName(iBusBusService.selectBusBusById(record.getBusId()).getBusName());
        record.setRouteName(iBusRouteService.selectBusRouteById(record.getRouteId()).getRouteName());
        record.setDriverName(iSysUserService.selectUserById(record.getDriver()).getNickName());
        return success(record);
    }

    @GetMapping(value = "/drivingRoute/{id}/{date}")
    public AjaxResult getAppInfo(@PathVariable("id") Long id,@PathVariable("date") String date)
    {
        BusDrivingRecord drivingRecord = busDrivingRecordService.selectBusDrivingRecordById(id);
        BusBus bus =iBusBusService.selectBusBusById(drivingRecord.getBusId());
        drivingRecord.setBusName(bus.getBusName());
        drivingRecord.setBusNumber(bus.getPlate());
        //路线
        BusRoute route = busRouteService.selectBusRouteById(drivingRecord.getRouteId());
        BusRouteSite start = busRouteSiteService.selectBusRouteSiteById(route.getStartSiteId());
        BusRouteSite end = busRouteSiteService.selectBusRouteSiteById(route.getEndSiteId());
        route.setStartSiteName(start.getSiteName());
        route.setEndSiteName(end.getSiteName());

        JSONArray routeSiteArray = JSONArray.parseArray(route.getForwardRoute());
        if(drivingRecord.getDirection() == 1){
            route.setStartSiteName(end.getSiteName());
            route.setEndSiteName(start.getSiteName());
            routeSiteArray = JSONArray.parseArray(route.getReverseRoute());
        }

        Long userId = SecurityUtils.getUserId();

        int nextSiteNum = 0;//车辆到达的下一个站点在路线的位置，0开始

        List<BusDrivingRecord> drivingList = busDrivingRecordService.getRecordByDriverDate(userId,date);
        Map<Long, BusRecordSite> recordSiteMap = iBusRecordSiteService.selectBusRecordSiteMap(drivingRecord.getId());

        JSONArray siteArray = new JSONArray();
        //检查当前登录人是否司机
        if(drivingList.size() > 0){
            drivingRecord.setWasDriver(1);
            //拼站点数据
            for(Object siteIdStr:routeSiteArray){
                Long siteId = Long.valueOf(siteIdStr.toString());
                BusRecordSite recordSite = recordSiteMap.get(siteId);
                BusRouteSite site = busRouteSiteService.selectBusRouteSiteById(siteId);
                JSONObject siteJson = new JSONObject();
                siteJson.put("id",site.getId());
                siteJson.put("title",site.getSiteName());
                siteJson.put("isIn",recordSite.getIsIn());
                siteJson.put("recordSiteId",recordSite.getId());
                if(recordSite.getIsIn() == 0){
                    //查询站点预约人数，区分上车下车
                    Integer[] count = iBusReservationService.getSiteInOutCount(id,siteId);
                    siteJson.put("desc","上车：" + count[0] + "，下车：" + count[1]);
                }else{
                    nextSiteNum++;
                }
                siteJson.put("longitude",site.getLongitude());
                siteJson.put("latitude",site.getLatitude());
                siteArray.add(siteJson);
            }
        }else{
            //乘客
            drivingRecord.setWasDriver(0);
            Map<String,BusRecordRealtime> recorMap = iBusRecordRealtimeService.getRouteReachSite(drivingRecord.getId());
            //拼站点数据
            int totalTime = 0;
            int totalDistance = 0;
            Long prevSiteId = null;
            boolean firstNotIn = true;//是否第一个未到站点
            for(Object siteIdStr:routeSiteArray){
                Long siteId = Long.valueOf(siteIdStr.toString());
                BusRecordSite recordSite = recordSiteMap.get(siteId);
                BusRouteSite site = busRouteSiteService.selectBusRouteSiteById(siteId);
                if(recordSite.getIsIn() == 0){
                    //查询预约的行驶记录下到上车站点的时间
                    String key = prevSiteId + "-" + siteId;
                    if(firstNotIn){
                        key = "-" + siteId;
                    }
                    BusRecordRealtime realtime = recorMap.get(key);
                    if(realtime != null){
                        totalDistance += Integer.valueOf(realtime.getDistance());
                        totalTime += Integer.valueOf(realtime.getInTime());
                    }
                    firstNotIn = false;
                }else{
                    nextSiteNum++;
                }

                JSONObject siteJson = new JSONObject();
                siteJson.put("id",site.getId());
                siteJson.put("title",site.getSiteName());
                siteJson.put("isIn",recordSite.getIsIn());
                siteJson.put("recordSiteId",recordSite.getId());
                siteJson.put("desc", totalTime+" 分钟 （" + (Double.valueOf(totalDistance) / 1000) +"KM）");
                siteJson.put("longitude",site.getLongitude());
                siteJson.put("latitude",site.getLatitude());
                siteArray.add(siteJson);
                prevSiteId = siteId;
            }
        }
        drivingRecord.setPassSite(nextSiteNum);
        route.setRouteSiteArray(siteArray.toString());
        drivingRecord.setRoute(route);
        return success(drivingRecord);
    }
    /**
     * 新增校车行驶记录
     */
    @PreAuthorize("@ss.hasPermi('system:record:add')")
    @Log(title = "校车行驶记录", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody BusDrivingRecord busDrivingRecord)
    {
        busDrivingRecord.setRecordDate(DateUtils.parseDateToStr("yyyy-MM-dd",busDrivingRecord.getDepartureTime()));
        return toAjax(busDrivingRecordService.insertBusDrivingRecord(busDrivingRecord));
    }

    /**
     * 修改校车行驶记录
     */
    @PreAuthorize("@ss.hasPermi('system:record:edit')")
    @Log(title = "校车行驶记录", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody BusDrivingRecord busDrivingRecord)
    {
        busDrivingRecord.setRecordDate(DateUtils.parseDateToStr("yyyy-MM-dd",busDrivingRecord.getDepartureTime()));
        return toAjax(busDrivingRecordService.updateBusDrivingRecord(busDrivingRecord));
    }

    /**
     * 删除校车行驶记录
     */
    @PreAuthorize("@ss.hasPermi('system:record:remove')")
    @Log(title = "校车行驶记录", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(busDrivingRecordService.deleteBusDrivingRecordByIds(ids));
    }
}
