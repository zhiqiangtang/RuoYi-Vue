package com.ruoyi.system.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;

import com.ruoyi.common.core.domain.entity.SysUser;
import com.ruoyi.system.service.ISysUserService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.BusBus;
import com.ruoyi.system.service.IBusBusService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 校车Controller
 * 
 * @author ruoyi
 * @date 2023-09-16
 */
@RestController
@RequestMapping("/system/bus")
public class BusBusController extends BaseController
{
    @Autowired
    private IBusBusService busBusService;

    @Autowired
    private ISysUserService iSysUserService;

    /**
     * 查询校车列表
     */
    @PreAuthorize("@ss.hasPermi('system:bus:list')")
    @GetMapping("/list")
    public TableDataInfo list(BusBus busBus)
    {
        startPage();
        List<BusBus> list = busBusService.selectBusBusList(busBus);
        for(BusBus bus: list){
            SysUser user = iSysUserService.selectUserById(bus.getDriver());
            bus.setDriverName(user.getNickName());
        }
        return getDataTable(list);
    }

    /**
     * 导出校车列表
     */
    @PreAuthorize("@ss.hasPermi('system:bus:export')")
    @Log(title = "校车", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, BusBus busBus)
    {
        List<BusBus> list = busBusService.selectBusBusList(busBus);
        ExcelUtil<BusBus> util = new ExcelUtil<BusBus>(BusBus.class);
        util.exportExcel(response, list, "校车数据");
    }

    /**
     * 获取校车详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:bus:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        BusBus bus = busBusService.selectBusBusById(id);
        SysUser user = iSysUserService.selectUserById(bus.getDriver());
        bus.setDriverName(user.getNickName());
        return success(bus);
    }

    /**
     * 新增校车
     */
    @PreAuthorize("@ss.hasPermi('system:bus:add')")
    @Log(title = "校车", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody BusBus busBus)
    {
        return toAjax(busBusService.insertBusBus(busBus));
    }

    /**
     * 修改校车
     */
    @PreAuthorize("@ss.hasPermi('system:bus:edit')")
    @Log(title = "校车", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody BusBus busBus)
    {
        return toAjax(busBusService.updateBusBus(busBus));
    }

    /**
     * 删除校车
     */
    @PreAuthorize("@ss.hasPermi('system:bus:remove')")
    @Log(title = "校车", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(busBusService.deleteBusBusByIds(ids));
    }
}
