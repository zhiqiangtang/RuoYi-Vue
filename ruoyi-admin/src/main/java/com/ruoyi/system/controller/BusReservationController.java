package com.ruoyi.system.controller;

import java.util.Date;
import java.util.List;
import javax.servlet.http.HttpServletResponse;

import com.ruoyi.common.utils.SecurityUtils;
import com.ruoyi.system.service.IBusRouteSiteService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.BusReservation;
import com.ruoyi.system.service.IBusReservationService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 乘车预约记录Controller
 *
 * @author ruoyi
 * @date 2023-09-16
 */
@RestController
@RequestMapping("/system/reservation")
public class BusReservationController extends BaseController
{
    @Autowired
    private IBusReservationService busReservationService;

    @Autowired
    private IBusRouteSiteService iBusRouteSiteService;

    /**
     * 查询乘车预约记录列表
     */
    @PreAuthorize("@ss.hasPermi('system:reservation:list')")
    @GetMapping("/list")
    public TableDataInfo list(BusReservation busReservation)
    {
        startPage();
        List<BusReservation> list = busReservationService.selectBusReservationList(busReservation);
        return getDataTable(list);
    }

    /**
     * 导出乘车预约记录列表
     */
    @PreAuthorize("@ss.hasPermi('system:reservation:export')")
    @Log(title = "乘车预约记录", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, BusReservation busReservation)
    {
        List<BusReservation> list = busReservationService.selectBusReservationList(busReservation);
        ExcelUtil<BusReservation> util = new ExcelUtil<BusReservation>(BusReservation.class);
        util.exportExcel(response, list, "乘车预约记录数据");
    }

    /**
     * 获取乘车预约记录详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:reservation:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(busReservationService.selectBusReservationById(id));
    }


    /**
     * 新增乘车预约记录
     */
    @PreAuthorize("@ss.hasPermi('system:reservation:add')")
    @Log(title = "乘车预约记录", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody BusReservation busReservation)
    {
        return toAjax(busReservationService.insertBusReservation(busReservation));
    }


    /**
     * 修改乘车预约记录
     */
    @PreAuthorize("@ss.hasPermi('system:reservation:edit')")
    @Log(title = "乘车预约记录", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody BusReservation busReservation)
    {
        return toAjax(busReservationService.updateBusReservation(busReservation));
    }

    /**
     * 删除乘车预约记录
     */
    @PreAuthorize("@ss.hasPermi('system:reservation:remove')")
    @Log(title = "乘车预约记录", businessType = BusinessType.DELETE)
    @GetMapping("/del/{id}")
    public AjaxResult remove(@PathVariable Long id)
    {
        return toAjax(busReservationService.deleteBusReservationById(id));
    }

    /**
     * 批量删除乘车预约记录
     */
    @PreAuthorize("@ss.hasPermi('system:reservation:remove')")
    @Log(title = "乘车预约记录", businessType = BusinessType.DELETE)
	@GetMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(busReservationService.deleteBusReservationByIds(ids));
    }


    @GetMapping(value = "/app/{recordId}")
    public AjaxResult getAppInfo(@PathVariable("recordId") Long recordId)
    {
        //当前登录人
        Long userId = SecurityUtils.getUserId();
        BusReservation busReservation = new BusReservation();
        busReservation.setRecordId(recordId);
        busReservation.setUserId(userId);
        List<BusReservation> list = busReservationService.selectBusReservationList(busReservation);
        if(list.size() > 0){
            BusReservation reservation = list.get(0);
            if(reservation.getInSiteId() != null){
                reservation.setInSiteName(iBusRouteSiteService.selectBusRouteSiteById(reservation.getInSiteId()).getSiteName());
            }
            if(reservation.getOutSiteId() != null){
                reservation.setOutSiteName(iBusRouteSiteService.selectBusRouteSiteById(reservation.getOutSiteId()).getSiteName());
            }
            return success(reservation);
        }
        return success(null);
    }

    /**
     * 添加预约
     * @param busReservation
     * @return
     */
    @PostMapping(value = "/appAdd")
    public AjaxResult appAdd(@RequestBody BusReservation busReservation)
    {
        busReservation.setUserId(SecurityUtils.getUserId());//申请人
        busReservation.setReservationTime(new Date());
        busReservation.setDidingStatus("0");//已预约
        busReservation.setStatus("0");
        return toAjax(busReservationService.insertBusReservation(busReservation));
    }

    /**
     * 修改预约
     * @param busReservation
     * @return
     */
    @PostMapping(value = "/appEdit")
    public AjaxResult appEdit(@RequestBody BusReservation busReservation)
    {

        return toAjax(busReservationService.updateBusReservation(busReservation));
    }
}
