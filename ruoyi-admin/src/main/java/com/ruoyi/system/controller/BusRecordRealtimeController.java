package com.ruoyi.system.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;

import com.ruoyi.system.domain.BusDrivingRecord;
import com.ruoyi.system.service.IBusDrivingRecordService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.BusRecordRealtime;
import com.ruoyi.system.service.IBusRecordRealtimeService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 校车行驶记录实时站点记录Controller
 * 
 * @author ruoyi
 * @date 2023-09-21
 */
@RestController
@RequestMapping("/system/realtime")
public class BusRecordRealtimeController extends BaseController
{
    @Autowired
    private IBusRecordRealtimeService busRecordRealtimeService;

    @Autowired
    private IBusDrivingRecordService iBusDrivingRecordService;

    /**
     * 查询校车行驶记录实时站点记录列表
     */
    @PreAuthorize("@ss.hasPermi('system:realtime:list')")
    @GetMapping("/list")
    public TableDataInfo list(BusRecordRealtime busRecordRealtime)
    {
        startPage();
        List<BusRecordRealtime> list = busRecordRealtimeService.selectBusRecordRealtimeList(busRecordRealtime);
        return getDataTable(list);
    }

    /**
     * 导出校车行驶记录实时站点记录列表
     */
    @PreAuthorize("@ss.hasPermi('system:realtime:export')")
    @Log(title = "校车行驶记录实时站点记录", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, BusRecordRealtime busRecordRealtime)
    {
        List<BusRecordRealtime> list = busRecordRealtimeService.selectBusRecordRealtimeList(busRecordRealtime);
        ExcelUtil<BusRecordRealtime> util = new ExcelUtil<BusRecordRealtime>(BusRecordRealtime.class);
        util.exportExcel(response, list, "校车行驶记录实时站点记录数据");
    }

    /**
     * 获取校车行驶记录实时站点记录详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:realtime:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(busRecordRealtimeService.selectBusRecordRealtimeById(id));
    }

    /**
     * 新增校车行驶记录实时站点记录
     */
    @Log(title = "校车行驶记录实时站点记录", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody BusRecordRealtime busRecordRealtime)
    {
        return toAjax(busRecordRealtimeService.insertBusRecordRealtime(busRecordRealtime));
    }

    /**
     * 修改校车行驶记录实时站点记录
     */
    @PreAuthorize("@ss.hasPermi('system:realtime:edit')")
    @Log(title = "校车行驶记录实时站点记录", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody BusRecordRealtime busRecordRealtime)
    {
        return toAjax(busRecordRealtimeService.updateBusRecordRealtime(busRecordRealtime));
    }

    /**
     * 删除校车行驶记录实时站点记录
     */
    @PreAuthorize("@ss.hasPermi('system:realtime:remove')")
    @Log(title = "校车行驶记录实时站点记录", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(busRecordRealtimeService.deleteBusRecordRealtimeByIds(ids));
    }
}
