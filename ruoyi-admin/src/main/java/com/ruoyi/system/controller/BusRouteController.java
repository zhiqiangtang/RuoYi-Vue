package com.ruoyi.system.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;

import com.alibaba.fastjson2.JSONArray;
import com.alibaba.fastjson2.JSONObject;
import com.ruoyi.system.domain.BusRouteSite;
import com.ruoyi.system.service.IBusRouteSiteService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.BusRoute;
import com.ruoyi.system.service.IBusRouteService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 路线Controller
 * 
 * @author ruoyi
 * @date 2023-09-16
 */
@RestController
@RequestMapping("/system/route")
public class BusRouteController extends BaseController
{
    @Autowired
    private IBusRouteService busRouteService;
    @Autowired
    private IBusRouteSiteService busRouteSiteService;

    /**
     * 查询路线列表
     */
    @PreAuthorize("@ss.hasPermi('system:route:list')")
    @GetMapping("/list")
    public TableDataInfo list(BusRoute busRoute)
    {
        startPage();
        List<BusRoute> list = busRouteService.selectBusRouteList(busRoute);
        for(BusRoute route: list){
            BusRouteSite start = busRouteSiteService.selectBusRouteSiteById(route.getStartSiteId());
            BusRouteSite end = busRouteSiteService.selectBusRouteSiteById(route.getEndSiteId());
            route.setStartSiteName(start.getSiteName());
            route.setEndSiteName(end.getSiteName());
        }
        return getDataTable(list);
    }

    /**
     * 导出路线列表
     */
    @PreAuthorize("@ss.hasPermi('system:route:export')")
    @Log(title = "路线", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, BusRoute busRoute)
    {
        List<BusRoute> list = busRouteService.selectBusRouteList(busRoute);
        ExcelUtil<BusRoute> util = new ExcelUtil<BusRoute>(BusRoute.class);
        util.exportExcel(response, list, "路线数据");
    }

    /**
     * 获取路线详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:route:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        BusRoute route = busRouteService.selectBusRouteById(id);
        BusRouteSite start = busRouteSiteService.selectBusRouteSiteById(route.getStartSiteId());
        BusRouteSite end = busRouteSiteService.selectBusRouteSiteById(route.getEndSiteId());
        route.setStartSiteName(start.getSiteName());
        route.setEndSiteName(end.getSiteName());
        return success(route);
    }


    /**
     * 新增路线
     */
    @PreAuthorize("@ss.hasPermi('system:route:add')")
    @Log(title = "路线", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody BusRoute busRoute)
    {
        return toAjax(busRouteService.insertBusRoute(busRoute));
    }

    /**
     * 修改路线
     */
    @PreAuthorize("@ss.hasPermi('system:route:edit')")
    @Log(title = "路线", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody BusRoute busRoute)
    {
        return toAjax(busRouteService.updateBusRoute(busRoute));
    }

    /**
     * 删除路线
     */
    @PreAuthorize("@ss.hasPermi('system:route:remove')")
    @Log(title = "路线", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(busRouteService.deleteBusRouteByIds(ids));
    }
}
