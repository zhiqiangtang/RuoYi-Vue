package com.ruoyi.system.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.BusRouteSite;
import com.ruoyi.system.service.IBusRouteSiteService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 站点Controller
 *
 * @author ruoyi
 * @date 2023-09-16
 */
@RestController
@RequestMapping("/system/site")
public class BusRouteSiteController extends BaseController
{
    @Autowired
    private IBusRouteSiteService busRouteSiteService;


    /**
     * 查询站点列表
     */
    @PreAuthorize("@ss.hasPermi('system:site:list')")
    @GetMapping("/list")
    public TableDataInfo list(BusRouteSite busRouteSite)
    {
        startPage();
        List<BusRouteSite> list = busRouteSiteService.selectBusRouteSiteList(busRouteSite);
        return getDataTable(list);
    }

    /**
     * 导出站点列表
     */
    @PreAuthorize("@ss.hasPermi('system:site:export')")
    @Log(title = "站点", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, BusRouteSite busRouteSite)
    {
        List<BusRouteSite> list = busRouteSiteService.selectBusRouteSiteList(busRouteSite);
        ExcelUtil<BusRouteSite> util = new ExcelUtil<BusRouteSite>(BusRouteSite.class);
        util.exportExcel(response, list, "站点数据");
    }

    /**
     * 获取站点详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:site:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(busRouteSiteService.selectBusRouteSiteById(id));
    }

    /**
     * 新增站点
     */
    @PreAuthorize("@ss.hasPermi('system:site:add')")
    @Log(title = "站点", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody BusRouteSite busRouteSite)
    {
        return toAjax(busRouteSiteService.insertBusRouteSite(busRouteSite));
    }

    /**
     * 修改站点
     */
    @PreAuthorize("@ss.hasPermi('system:site:edit')")
    @Log(title = "站点", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody BusRouteSite busRouteSite)
    {
        return toAjax(busRouteSiteService.updateBusRouteSite(busRouteSite));
    }

    /**
     * 删除站点
     */
    @PreAuthorize("@ss.hasPermi('system:site:remove')")
    @Log(title = "站点", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(busRouteSiteService.deleteBusRouteSiteByIds(ids));
    }
}
