package com.ruoyi.system.mapper;

import java.util.List;
import com.ruoyi.system.domain.BusRecordRealtime;

/**
 * 校车行驶记录实时站点记录Mapper接口
 * 
 * @author ruoyi
 * @date 2023-09-21
 */
public interface BusRecordRealtimeMapper 
{
    /**
     * 查询校车行驶记录实时站点记录
     * 
     * @param id 校车行驶记录实时站点记录主键
     * @return 校车行驶记录实时站点记录
     */
    public BusRecordRealtime selectBusRecordRealtimeById(Long id);
    public List<BusRecordRealtime> selectRealTimeList(Long recordId);

    /**
     * 查询校车行驶记录实时站点记录列表
     * 
     * @param busRecordRealtime 校车行驶记录实时站点记录
     * @return 校车行驶记录实时站点记录集合
     */
    public List<BusRecordRealtime> selectBusRecordRealtimeList(BusRecordRealtime busRecordRealtime);

    /**
     * 新增校车行驶记录实时站点记录
     * 
     * @param busRecordRealtime 校车行驶记录实时站点记录
     * @return 结果
     */
    public int insertBusRecordRealtime(BusRecordRealtime busRecordRealtime);

    /**
     * 修改校车行驶记录实时站点记录
     * 
     * @param busRecordRealtime 校车行驶记录实时站点记录
     * @return 结果
     */
    public int updateBusRecordRealtime(BusRecordRealtime busRecordRealtime);

    /**
     * 删除校车行驶记录实时站点记录
     * 
     * @param id 校车行驶记录实时站点记录主键
     * @return 结果
     */
    public int deleteBusRecordRealtimeById(Long id);

    /**
     * 批量删除校车行驶记录实时站点记录
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteBusRecordRealtimeByIds(Long[] ids);
}
