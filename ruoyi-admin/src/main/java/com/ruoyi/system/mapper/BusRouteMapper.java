package com.ruoyi.system.mapper;

import java.util.List;
import com.ruoyi.system.domain.BusRoute;

/**
 * 路线Mapper接口
 * 
 * @author ruoyi
 * @date 2023-09-16
 */
public interface BusRouteMapper 
{
    /**
     * 查询路线
     * 
     * @param id 路线主键
     * @return 路线
     */
    public BusRoute selectBusRouteById(Long id);

    /**
     * 查询路线列表
     * 
     * @param busRoute 路线
     * @return 路线集合
     */
    public List<BusRoute> selectBusRouteList(BusRoute busRoute);

    /**
     * 新增路线
     * 
     * @param busRoute 路线
     * @return 结果
     */
    public int insertBusRoute(BusRoute busRoute);

    /**
     * 修改路线
     * 
     * @param busRoute 路线
     * @return 结果
     */
    public int updateBusRoute(BusRoute busRoute);

    /**
     * 删除路线
     * 
     * @param id 路线主键
     * @return 结果
     */
    public int deleteBusRouteById(Long id);

    /**
     * 批量删除路线
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteBusRouteByIds(Long[] ids);
}
