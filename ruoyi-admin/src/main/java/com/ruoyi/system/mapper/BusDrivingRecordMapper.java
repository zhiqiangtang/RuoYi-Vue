package com.ruoyi.system.mapper;

import java.util.List;
import com.ruoyi.system.domain.BusDrivingRecord;

/**
 * 校车行驶记录Mapper接口
 * 
 * @author ruoyi
 * @date 2023-09-16
 */
public interface BusDrivingRecordMapper 
{
    /**
     * 查询校车行驶记录
     * 
     * @param id 校车行驶记录主键
     * @return 校车行驶记录
     */
    public BusDrivingRecord selectBusDrivingRecordById(Long id);

    /**
     * 查询校车行驶记录列表
     * 
     * @param busDrivingRecord 校车行驶记录
     * @return 校车行驶记录集合
     */
    public List<BusDrivingRecord> selectBusDrivingRecordList(BusDrivingRecord busDrivingRecord);

    /**
     * 新增校车行驶记录
     * 
     * @param busDrivingRecord 校车行驶记录
     * @return 结果
     */
    public int insertBusDrivingRecord(BusDrivingRecord busDrivingRecord);

    /**
     * 修改校车行驶记录
     * 
     * @param busDrivingRecord 校车行驶记录
     * @return 结果
     */
    public int updateBusDrivingRecord(BusDrivingRecord busDrivingRecord);

    /**
     * 删除校车行驶记录
     * 
     * @param id 校车行驶记录主键
     * @return 结果
     */
    public int deleteBusDrivingRecordById(Long id);

    /**
     * 批量删除校车行驶记录
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteBusDrivingRecordByIds(Long[] ids);
}
