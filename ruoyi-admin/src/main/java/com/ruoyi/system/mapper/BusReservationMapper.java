package com.ruoyi.system.mapper;

import java.util.List;
import com.ruoyi.system.domain.BusReservation;

/**
 * 乘车预约记录Mapper接口
 * 
 * @author ruoyi
 * @date 2023-09-16
 */
public interface BusReservationMapper 
{
    /**
     * 查询乘车预约记录
     * 
     * @param id 乘车预约记录主键
     * @return 乘车预约记录
     */
    public BusReservation selectBusReservationById(Long id);

    /**
     * 查询乘车预约记录列表
     * 
     * @param busReservation 乘车预约记录
     * @return 乘车预约记录集合
     */
    public List<BusReservation> selectBusReservationList(BusReservation busReservation);

    /**
     * 新增乘车预约记录
     * 
     * @param busReservation 乘车预约记录
     * @return 结果
     */
    public int insertBusReservation(BusReservation busReservation);

    /**
     * 修改乘车预约记录
     * 
     * @param busReservation 乘车预约记录
     * @return 结果
     */
    public int updateBusReservation(BusReservation busReservation);

    /**
     * 删除乘车预约记录
     * 
     * @param id 乘车预约记录主键
     * @return 结果
     */
    public int deleteBusReservationById(Long id);

    /**
     * 批量删除乘车预约记录
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteBusReservationByIds(Long[] ids);
}
