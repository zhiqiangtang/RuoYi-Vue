package com.ruoyi.uam.controller;

import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.uam.domain.ViewTeacher;
import com.ruoyi.uam.service.ViewTeacherService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @description: 描述
 * @author: 朋朋
 * @date: 2023/12/8/16:42
 */
@RestController
@RequestMapping("/teacher")
public class ViewTeacherController {

    @Autowired
    private ViewTeacherService viewTeacherService;

    @GetMapping("/userJobNumber")
    public AjaxResult userJobNumber(@RequestParam String jobNumber){
        ViewTeacher viewTeachers = viewTeacherService.selectTeacherInfo(jobNumber);
        return AjaxResult.success("查询成功",viewTeachers);
    }
}
