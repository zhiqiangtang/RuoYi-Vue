package com.ruoyi.uam.controller;

import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.uam.domain.ViewDept;
import com.ruoyi.uam.service.ViewDeptService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @description: 描述
 * @author: 朋朋
 * @date: 2023/12/8/16:13
 */
@RestController
@RequestMapping("/dept")
public class ViewDeptController {

    @Autowired
    private ViewDeptService viewDeptService;

    @GetMapping("/list")
    public AjaxResult list(){
        List<ViewDept> list = viewDeptService.list();
        return AjaxResult.success("查询成功",list);
    }
}
