package com.ruoyi.web.controller.sfginfo;


import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.sfgsystem.entity.Family;
import com.ruoyi.sfgsystem.service.FamilyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


@RestController
@RequestMapping("/family")
public class FamilyController extends BaseController {

    @Autowired(required = false)
    FamilyService familyService;

    @GetMapping("/selectFamilyData")
    public TableDataInfo selectPage(Family family){

        startPage();  //分页
        System.out.println();

        List<Family> familieDatas=familyService.selectFamilyData(family);
        for (int i = 0; i < familieDatas.size(); i++) {
            System.out.println(familieDatas.get(i).getFamilyName());
        }
        return getDataTable(familieDatas);
    }
//    数据新增和修改数据
    @PostMapping("/insertFamilyData")
    public AjaxResult insertFamilyData(@RequestBody Family family){
        int familyData=familyService.insertFamilyData(family);
        return success(familyData);
    }

//    删除数据
    @DeleteMapping("/deleteBeachIds/{ids}")
    public AjaxResult deleteBeachIds(@PathVariable("ids") List<Integer> ids){
       int familyIds= familyService.deleteBeachIds(ids);
       return success(familyIds);
    }

//    验证人编号
    @PostMapping("/checkBehaviourCodeRepeat/{value}")
    public AjaxResult verifierNumber(@PathVariable("value") Integer value){
        boolean nom=familyService.verifierNumber(value);
        return success(nom);
    }

//    导出数据
    @PostMapping("/exportData")
    public void export(HttpServletResponse response,Family family){
        List<Family> list = familyService.selectFamilyData(family);
        ExcelUtil<Family> util = new ExcelUtil<>(Family.class);
        util.exportExcel(response,list,"家庭基本信息表");
    }

}
