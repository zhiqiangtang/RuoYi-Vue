package com.ruoyi.web.controller.sfginfo;


import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.sfgsystem.entity.Education;
import com.ruoyi.sfgsystem.service.EducationService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.*;

@RestController
@RequestMapping("/education")
public class EducationController extends BaseController
{
    @Autowired
    private EducationService educationService;


    @GetMapping("/query")
    public TableDataInfo query(HttpServletResponse response,Education education){
//        数据的总条数
        startPage();
        List<Education> educations = educationService.selectData(education);

        return getDataTable(educations);
    }

//新增数据或修改数据
    @PostMapping("/insertOrUpdateData")
    public void insertOrUpdateData(@RequestBody Education education){
        educationService.insertOrUpdateData(education);
    }

    //批量删除或单个删除
    @PostMapping("/deleteBeachIds/{ids}")
    public void deleteBeachIds(@PathVariable("ids") List<Integer> ids){
        educationService.deleteBeachDate(ids);
    }

    //导出数据
    @PostMapping("/exportData")
    public void export(HttpServletResponse response, Education education){

        List<Education> list = educationService.selectData(education);

        ExcelUtil<Education> util = new ExcelUtil<>(Education.class);

        util.exportExcel(response,list,"教育基本信息表");

    }
}
