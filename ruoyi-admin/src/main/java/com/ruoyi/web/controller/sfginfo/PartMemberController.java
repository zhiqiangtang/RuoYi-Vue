package com.ruoyi.web.controller.sfginfo;

import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.sfgsystem.entity.ExportPartMember;
import com.ruoyi.sfgsystem.entity.PartMember;
import com.ruoyi.sfgsystem.service.PartMemberService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;


import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CopyOnWriteArrayList;

@RestController
@RequestMapping("/PartMember")
public class PartMemberController extends BaseController {
    @Autowired
    private PartMemberService partMemberService;
//    分页查询
    @GetMapping("/selectPartMemberDataPage")
    public TableDataInfo selectPage(PartMember partMember){
//        数据的记录
        startPage();
//        分页查询的数据
        List<PartMember> partMembersData=partMemberService.selectPartMemberPage(partMember);
        return getDataTable(partMembersData);
    }

    /**
     * 新增或修改
     * @param partMember
     * @return
     */
    @PostMapping("/insertOrUpdataPartMemberData")
    public AjaxResult insertOrUpdata(@RequestBody PartMember partMember){
        System.out.println("partMember:"+partMember.getPartMemberId()+"sda:"+partMember.getPartMemberPersonId());
        System.out.println("小宋:==="+partMember.getPartMemberPhone());
        int partMemberId= partMemberService.insertOrUpdata(partMember);
        return success(partMemberId);
    }

    /**
     * 批量删除或单个删除
     * @param ids 要删除的数据
     * @return 返回的结果
     */
    @DeleteMapping("/deleteBeachPartMemberData/{ids}")
    public AjaxResult deleteBeachIdsOrRemoveId(@PathVariable("ids") List<Integer> ids){
        int removeId=partMemberService.deleteBeachIdsOrRemoveId(ids);
        return success(removeId);
    }

    /*人编号的验证*/
    @PostMapping("/checkBehaviourCodeRepeat/{value}")
    public AjaxResult verifierNumber(@PathVariable("value") Integer value){
        boolean list =partMemberService.verifierNumber(value);
        return success();

    }

//    数据的导出
    @PostMapping("/exportData")
    public void export(HttpServletResponse response,PartMember partMember){
        List<PartMember> list = partMemberService.selectPartMemberPage(partMember);

        ExcelUtil<PartMember> util = new ExcelUtil<>(PartMember.class);
        util.exportExcel(response,list,"党员基本信息表");
    }

}
