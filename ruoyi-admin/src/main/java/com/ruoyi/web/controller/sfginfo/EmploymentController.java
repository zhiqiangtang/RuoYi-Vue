package com.ruoyi.web.controller.sfginfo;

import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.sfgsystem.entity.Employment;
import com.ruoyi.sfgsystem.service.EmploymentService;
import io.swagger.models.auth.In;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CopyOnWriteArrayList;

/**就业信息*/
@RestController
@RequestMapping("/employment")
public class EmploymentController extends BaseController {

    @Autowired
    EmploymentService employmentService;

    @GetMapping("/slectEmploymentDataPage")
    public TableDataInfo selectPage(Employment employment){
//        数据的条数记录
        startPage();
//        分页查询
        List<Employment> employmentDatas=employmentService.selectEmploymentPageData(employment);

        return getDataTable(employmentDatas);
    }
//    新增数据和修改数据
    @PostMapping("/insertOrUpdateEmploymentData")
    public Integer insertOrUpdateData(@RequestBody Employment employment){
        System.out.println("新增的数据:---->"+employment.getEmploymentPhone());
        return  employmentService.insertOrUpdateData(employment);
    }

    //人编号的验证
    @GetMapping("/checkBehaviourCodeRepeat/{employmentPersonId}")
    public boolean verifierNumber(@PathVariable("employmentPersonId") String employmentPersonId){
        boolean ver = employmentService.verifierNumber(employmentPersonId);
        return ver;
    }

    //批量删除或单个数据删除
    @DeleteMapping("/deleteBeachIds/{ids}")
    public Integer deleteBeachIds(@PathVariable("ids") List<Integer> ids){
        return employmentService.deleteBeachIds(ids);
    }

    /*导出数据*/
    @PostMapping("/exportData")
    public void export(HttpServletResponse response ,Employment employment){
        List<Employment> list = employmentService.selectEmploymentPageData(employment);
        ExcelUtil<Employment> util = new ExcelUtil<>(Employment.class);
        util.exportExcel(response,list,"就业基本信息");
    }


}
