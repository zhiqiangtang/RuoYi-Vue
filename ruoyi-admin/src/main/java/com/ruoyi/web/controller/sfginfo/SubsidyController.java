package com.ruoyi.web.controller.sfginfo;


import com.github.pagehelper.PageInfo;
import com.ruoyi.common.constant.HttpStatus;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.utils.poi.ExcelUtil;

import com.ruoyi.sfgsystem.entity.ExportSubsidy;

import com.ruoyi.sfgsystem.entity.Subsidy;
import com.ruoyi.sfgsystem.service.SubsidyService;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.*;

@RestController
@RequestMapping("/subsidy")
public class SubsidyController extends BaseController {
    @Autowired(required = false)
    private SubsidyService subsidyService;
    /* 分页查询*/

    @GetMapping("/selectPage")
    public TableDataInfo selectPage(Subsidy subsidy){


        /**数据的记录数*/
        startPage();
        /*分页查询*/
        List<Subsidy> subsidyData = subsidyService.selectSubsidyPage(subsidy);
        return getDataTable(subsidyData);
    }

    /**新增数据或修改数据*/
    @PostMapping("/insertUpdateSubsidyData")
    public AjaxResult insertOrUpsate(@RequestBody Subsidy subsidy){
        Integer integer = subsidyService.insertOrUpdateData(subsidy);

        /**新增数据或修改数据*/
        return success(integer) ;
    }

    /**
     * 批量删除数据或单个删除
     * @param ids
     * @return
     */
    @DeleteMapping("/deleteBeachIds/{ids}")
    public AjaxResult deleteBeachIds(@PathVariable("ids")List<Integer> ids){
        Integer integer = subsidyService.deleteBeachIds(ids);
        return success(integer);
    }

    /*人编号的验证*/
    @GetMapping("/checkBehaviourCodeRepeat/{value}")
    public AjaxResult verifierNumber(@PathVariable("value") String value){ ;
        boolean vern= subsidyService.verifierNumber(value);
        return success(vern);
    }

    //    导出功能
    @PostMapping("/export")
    public void export(HttpServletResponse response,Subsidy subsidy){
        List<Subsidy> list =subsidyService.selectSubsidyPage(subsidy);
        ExcelUtil<Subsidy> util = new ExcelUtil<>(Subsidy.class);

        util.exportExcel(response,list,"就业信息","就业信息");
    }

}
