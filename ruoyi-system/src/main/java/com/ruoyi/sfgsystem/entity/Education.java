package com.ruoyi.sfgsystem.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.common.annotation.Excel;

import java.util.Date;
import java.util.Objects;

//教育信息
public class Education {

    @Excel(name = "序号")
    private Integer educationId;

    @Excel(name = "姓名")
    private String familyName;

    @Excel(name = "人编号")
    private Integer educationPersonId;               //人编号

    @Excel(name = "学校")
    private String educationSchoolName;              //学校名

    @Excel(name = "教育程度")
    private String educationEducationalLevel;        //教育程度

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Excel(name = "入学时间")
    private Date educationAdmissionTime;             //入学时间

    @Excel(name = "现在年级")
    private String educationCurrentGrade;            //现在年级

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Excel(name = "毕业时间")
    private Date educationGraduationTime;            //毕业时间

    @Excel(name = "是否辍学")
    private String educationQuitSchool;              //是否辍学

    private Family family;

    public Education() {
    }


    public Education(Integer educationId, Integer educationPersonId,
                     String educationSchoolName,
                     String educationEducationalLevel,
                     Date educationAdmissionTime,
                     String educationCurrentGrade,
                     Date educationGraduationTime,
                     String educationQuitSchool) {
        this.educationId = educationId;
        this.educationPersonId = educationPersonId;
        this.educationSchoolName = educationSchoolName;
        this.educationEducationalLevel = educationEducationalLevel;
        this.educationAdmissionTime = educationAdmissionTime;
        this.educationCurrentGrade = educationCurrentGrade;
        this.educationGraduationTime = educationGraduationTime;
        this.educationQuitSchool = educationQuitSchool;
    }

    public Integer getEducationId() {
        return educationId;
    }

    public void setEducationId(Integer educationId) {
        this.educationId = educationId;
    }

    public Integer getEducationPersonId() {
        return educationPersonId;
    }

    public void setEducation_person_id(Integer education_person_id) {
        this.educationPersonId = education_person_id;
    }

    public String getEducationSchoolName() {
        return educationSchoolName;
    }

    public void setEducationSchoolName(String educationSchoolName) {
        this.educationSchoolName = educationSchoolName;
    }

    public String getEducationEducationalLevel() {
        return educationEducationalLevel;
    }

    public void setEducationEducationalLevel(String educationEducationalLevel) {
        this.educationEducationalLevel = educationEducationalLevel;
    }

    public Date getEducationAdmissionTime() {
        return educationAdmissionTime;
    }

    public void setEducationAdmissionTime(Date educationAdmissionTime) {
        this.educationAdmissionTime = educationAdmissionTime;
    }

    public String getEducationCurrentGrade() {
        return educationCurrentGrade;
    }

    public void setEducationCurrentGrade(String educationCurrentGrade) {
        this.educationCurrentGrade = educationCurrentGrade;
    }

    public Date getEducationGraduationTime() {
        return educationGraduationTime;
    }

    public void setEducationGraduationTime(Date educationGraduationTime) {
        this.educationGraduationTime = educationGraduationTime;
    }

    public String getEducationQuitSchool() {
        return educationQuitSchool;
    }

    public void setEducationQuitSchool(String educationQuitSchool) {
        this.educationQuitSchool = educationQuitSchool;
    }

    public Family getFamily() {
        return family;
    }

    public void setFamily(Family family) {
        this.family = family;
    }

    public void setEducationPersonId(Integer educationPersonId) {
        this.educationPersonId = educationPersonId;
    }

    public String getFamilyName() {
        return familyName;
    }

    public void setFamilyName(String familyName) {
        this.familyName = familyName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Education education = (Education) o;
        return Objects.equals(educationId, education.educationId)
                && Objects.equals(educationPersonId, education.educationPersonId)
                && Objects.equals(educationSchoolName, education.educationSchoolName)
                && Objects.equals(educationEducationalLevel,
                education.educationEducationalLevel) && Objects.equals(educationAdmissionTime,
                education.educationAdmissionTime) && Objects.equals(educationCurrentGrade,
                education.educationCurrentGrade) && Objects.equals(educationGraduationTime,
                education.educationGraduationTime) && Objects.equals(educationQuitSchool,
                education.educationQuitSchool);
    }

    @Override
    public int hashCode() {
        return Objects.hash(educationId, educationPersonId, educationSchoolName,
                educationEducationalLevel, educationAdmissionTime, educationCurrentGrade,
                educationGraduationTime, educationQuitSchool);
    }

    @Override
    public String toString() {
        return "Education{" +
                "educationId=" + educationId +
                ", education_person_id=" + educationPersonId +
                ", educationSchoolName='" + educationSchoolName + '\'' +
                ", educationEducationalLevel='" + educationEducationalLevel + '\'' +
                ", educationAdmissionTime=" + educationAdmissionTime +
                ", educationCurrentGrade='" + educationCurrentGrade + '\'' +
                ", educationGraduationTime=" + educationGraduationTime +
                ", educationQuitSchool='" + educationQuitSchool + '\'' +
                '}';
    }
}