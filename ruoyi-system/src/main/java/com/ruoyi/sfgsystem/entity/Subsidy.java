package com.ruoyi.sfgsystem.entity;

import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;
import org.springframework.beans.factory.parsing.BeanEntry;


import java.util.Date;

//村民补助
public class Subsidy extends BaseEntity {
    @Excel(name = "序号")
    private Integer subsidyId;//主键
    @Excel(name = "人编号")
    private Integer subsidyPersonId;                    //人编号
    @Excel(name = "残疾补贴")
    private Integer subsidyDisabilityAllowance;         //残疾补贴
    @Excel(name = "高龄补贴")
    private Integer subsidyOldAgeAllowance;            //高龄补助
    @Excel(name = "孤儿救助")
    private Integer subsidyOrphanAllowance;            //孤儿救助
    @Excel(name = "临时救助")
    private Integer subsidyTemporaryRescue;            //临时救助
    @Excel(name = "低保金")
    private Integer subsidySubsistenceAllowance;       //低保金
    @Excel(name = "特困供养金")
    private Integer subsidyEmergencyAllowance;         //特困供养金
    @Excel(name = "退役军人补助")
    private Integer subsidyRetiredServicemen;          //退役军人补助
    private Integer subsidyAccountId;                  //户编号
    @Excel(name = "低保补助人数")
    private Integer subsidyNumber;                       //人数
    @Excel(name = "问题是否解决")
    private String subsidySolve;                        //问题是否解决
    @Excel(name = "困难等级")
    private String subsidyTypeOrGrade;                  //类型和等级
    @Excel(name = "军人退役时间")
    private Date subsidyTime;//退役时间

    @Excel(name = "姓名")
    private String familyName;

    @Excel(name = "姓名")
    private Family family;

    public Subsidy() {
    }

    public Subsidy(Family family) {

        this.familyName = family.getFamilyName();
    }

    public Subsidy(Integer subsidyId, Integer subsidyPersonId,
                   Integer subsidyDisabilityAllowance,
                   Integer subsidyOldAgeAllowance,
                   Integer subsidyOrphanAllowance,
                   Integer subsidyTemporaryRescue,
                   Integer subsidySubsistenceAllowance,
                   Integer subsidyEmergencyAllowance,
                   Integer subsidyRetiredServicemen,
                   Integer subsidyAccountId, Integer subsidyNumber,
                   String subsidySolve, String subsidyTypeOrGrade,
                   Date subsidyTime, String familyName, Family family) {
        this.subsidyId = subsidyId;
        this.subsidyPersonId = subsidyPersonId;
        this.subsidyDisabilityAllowance = subsidyDisabilityAllowance;
        this.subsidyOldAgeAllowance = subsidyOldAgeAllowance;
        this.subsidyOrphanAllowance = subsidyOrphanAllowance;
        this.subsidyTemporaryRescue = subsidyTemporaryRescue;
        this.subsidySubsistenceAllowance = subsidySubsistenceAllowance;
        this.subsidyEmergencyAllowance = subsidyEmergencyAllowance;
        this.subsidyRetiredServicemen = subsidyRetiredServicemen;
        this.subsidyAccountId = subsidyAccountId;
        this.subsidyNumber = subsidyNumber;
        this.subsidySolve = subsidySolve;
        this.subsidyTypeOrGrade = subsidyTypeOrGrade;
        this.subsidyTime = subsidyTime;
        this.familyName = familyName;
        this.family = family;
    }

    public Integer getSubsidyId() {
        return subsidyId;
    }

    public void setSubsidyId(Integer subsidyId) {
        this.subsidyId = subsidyId;
    }

    public Integer getSubsidyPersonId() {
        return subsidyPersonId;
    }

    public void setSubsidyPersonId(Integer subsidyPersonId) {
        this.subsidyPersonId = subsidyPersonId;
    }

    public Integer getSubsidyDisabilityAllowance() {
        return subsidyDisabilityAllowance;
    }

    public void setSubsidyDisabilityAllowance(Integer subsidyDisabilityAllowance) {
        this.subsidyDisabilityAllowance = subsidyDisabilityAllowance;
    }

    public Integer getSubsidyOldAgeAllowance() {
        return subsidyOldAgeAllowance;
    }

    public void setSubsidyOldAgeAllowance(Integer subsidyOldAgeAllowance) {
        this.subsidyOldAgeAllowance = subsidyOldAgeAllowance;
    }

    public Integer getSubsidyOrphanAllowance() {
        return subsidyOrphanAllowance;
    }

    public void setSubsidyOrphanAllowance(Integer subsidyOrphanAllowance) {
        this.subsidyOrphanAllowance = subsidyOrphanAllowance;
    }

    public Integer getSubsidyTemporaryRescue() {
        return subsidyTemporaryRescue;
    }

    public void setSubsidyTemporaryRescue(Integer subsidyTemporaryRescue) {
        this.subsidyTemporaryRescue = subsidyTemporaryRescue;
    }

    public Integer getSubsidySubsistenceAllowance() {
        return subsidySubsistenceAllowance;
    }

    public void setSubsidySubsistenceAllowance(Integer subsidySubsistenceAllowance) {
        this.subsidySubsistenceAllowance = subsidySubsistenceAllowance;
    }

    public Integer getSubsidyEmergencyAllowance() {
        return subsidyEmergencyAllowance;
    }

    public void setSubsidyEmergencyAllowance(Integer subsidyEmergencyAllowance) {
        this.subsidyEmergencyAllowance = subsidyEmergencyAllowance;
    }

    public Integer getSubsidyRetiredServicemen() {
        return subsidyRetiredServicemen;
    }

    public void setSubsidyRetiredServicemen(Integer subsidyRetiredServicemen) {
        this.subsidyRetiredServicemen = subsidyRetiredServicemen;
    }

    public Integer getSubsidyAccountId() {
        return subsidyAccountId;
    }

    public void setSubsidyAccountId(Integer subsidyAccountId) {
        this.subsidyAccountId = subsidyAccountId;
    }

    public Integer getSubsidyNumber() {
        return subsidyNumber;
    }

    public void setSubsidyNumber(Integer subsidyNumber) {
        this.subsidyNumber = subsidyNumber;
    }

    public String getSubsidySolve() {
        return subsidySolve;
    }

    public void setSubsidySolve(String subsidySolve) {
        this.subsidySolve = subsidySolve;
    }

    public String getSubsidyTypeOrGrade() {
        return subsidyTypeOrGrade;
    }

    public void setSubsidyTypeOrGrade(String subsidyTypeOrGrade) {
        this.subsidyTypeOrGrade = subsidyTypeOrGrade;
    }

    public Date getSubsidyTime() {
        return subsidyTime;
    }

    public void setSubsidyTime(Date subsidyTime) {
        this.subsidyTime = subsidyTime;
    }

    public String getFamilyName() {
        return familyName;
    }

    public void setFamilyName(String familyName) {
        this.familyName = familyName;
    }

    public Family getFamily() {
        return family;
    }

    public void setFamily(Family family) {
        this.family = family;
    }
}
