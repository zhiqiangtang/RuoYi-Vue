package com.ruoyi.sfgsystem.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

import java.util.Date;

import java.util.Objects;
public class Family extends BaseEntity {


    private Integer familyId;  //主键

    @Excel(name = "人编号")
    private Integer familyPersonId; //人编号

    @Excel(name = "户编号")
    private Integer familyAccountId; //户编号

    @Excel(name = "姓名")
    private String familyName;   //性名

    @Excel(name = "年龄")
    private Integer familyAge;   //年龄

    @Excel(name = "性别")
    private String familySex;  //性别

    @Excel(name = "出生日期")
    private Date familyBirthTime;  //出生日期

    @Excel(name = "身份证")
    private String familyNumberId; //身份证

    @Excel(name = "户主关系")
    private String familyHouseholderRelation; //户主关系

    @Excel(name = "是否党员")
    private String familyOrPartymember; //是否党员

    @Excel(name = "入党时间")
    private Date familyTheparty;  //入党时间

    @Excel(name = "家庭人数")
    private Integer familyCount;  //家庭人数

    @Excel(name = "组名")
    private String familyGroupName; //组名

    @Excel(name = "村名")
    private String familyVillageName; //村名

    public Family() {
    }

    public Family(Integer familyId, Integer familyPersonId,
                  Integer familyAccountId, String familyName,
                  Integer familyAge, String familySex,
                  Date familyBirthTime, String familyNumberId,
                  String familyHouseholderRelation,
                  String familyOrPartymember, Date familyTheparty,
                  Integer familyCount, String familyGroupName,
                  String familyVillageName,Family family) {
        this.familyId = familyId;
        this.familyPersonId = familyPersonId;
        this.familyAccountId = familyAccountId;
        this.familyName = familyName;
        this.familyAge = familyAge;
        this.familySex = familySex;
        this.familyBirthTime = familyBirthTime;
        this.familyNumberId = familyNumberId;
        this.familyHouseholderRelation = familyHouseholderRelation;
        this.familyOrPartymember = familyOrPartymember;
        this.familyTheparty = familyTheparty;
        this.familyCount = familyCount;
        this.familyGroupName = familyGroupName;
        this.familyVillageName = familyVillageName;
    }

    public Integer getFamilyId() {
        return familyId;
    }

    public void setFamilyId(Integer familyId) {
        this.familyId = familyId;
    }

    public Integer getFamilyPersonId() {
        return familyPersonId;
    }

    public void setFamilyPersonId(Integer familyPersonId) {
        this.familyPersonId = familyPersonId;
    }

    public Integer getFamilyAccountId() {
        return familyAccountId;
    }

    public void setFamilyAccountId(Integer familyAccountId) {
        this.familyAccountId = familyAccountId;
    }

    public String getFamilyName() {
        return familyName;
    }

    public void setFamilyName(String familyName) {
        this.familyName = familyName;
    }

    public Integer getFamilyAge() {
        return familyAge;
    }

    public void setFamilyAge(Integer familyAge) {
        this.familyAge = familyAge;
    }

    public String getFamilySex() {
        return familySex;
    }

    public void setFamilySex(String familySex) {
        this.familySex = familySex;
    }

    public Date getFamilyBirthTime() {
        return familyBirthTime;
    }

    public void setFamilyBirthTime(Date familyBirthTime) {
        this.familyBirthTime = familyBirthTime;
    }

    public String getFamilyNumberId() {
        return familyNumberId;
    }

    public void setFamilyNumberId(String familyNumberId) {
        this.familyNumberId = familyNumberId;
    }

    public String getFamilyHouseholderRelation() {
        return familyHouseholderRelation;
    }

    public void setFamilyHouseholderRelation(String familyHouseholderRelation) {
        this.familyHouseholderRelation = familyHouseholderRelation;
    }

    public String getFamilyOrPartymember() {
        return familyOrPartymember;
    }

    public void setFamilyOrPartymember(String familyOrPartymember) {
        this.familyOrPartymember = familyOrPartymember;
    }

    public Date getFamilyTheparty() {
        return familyTheparty;
    }

    public void setFamilyTheparty(Date familyTheparty) {
        this.familyTheparty = familyTheparty;
    }

    public Integer getFamilyCount() {
        return familyCount;
    }

    public void setFamilyCount(Integer familyCount) {
        this.familyCount = familyCount;
    }

    public String getFamilyGroupName() {
        return familyGroupName;
    }

    public void setFamilyGroupName(String familyGroupName) {
        this.familyGroupName = familyGroupName;
    }

    public String getFamilyVillageName() {
        return familyVillageName;
    }

    public void setFamilyVillageName(String familyVillageName) {
        this.familyVillageName = familyVillageName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Family family = (Family) o;
        return Objects.equals(familyId, family.familyId) && Objects.equals(familyPersonId, family.familyPersonId) && Objects.equals(familyAccountId, family.familyAccountId) && Objects.equals(familyName, family.familyName) && Objects.equals(familyAge, family.familyAge) && Objects.equals(familySex, family.familySex) && Objects.equals(familyBirthTime, family.familyBirthTime) && Objects.equals(familyNumberId, family.familyNumberId) && Objects.equals(familyHouseholderRelation, family.familyHouseholderRelation) && Objects.equals(familyOrPartymember, family.familyOrPartymember) && Objects.equals(familyTheparty, family.familyTheparty) && Objects.equals(familyCount, family.familyCount) && Objects.equals(familyGroupName, family.familyGroupName) && Objects.equals(familyVillageName, family.familyVillageName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(familyId, familyPersonId, familyAccountId, familyName, familyAge, familySex, familyBirthTime, familyNumberId, familyHouseholderRelation, familyOrPartymember, familyTheparty, familyCount, familyGroupName, familyVillageName);
    }

    @Override
    public String toString() {
        return "Family{" +
                "familyId=" + familyId +
                ", familyPersonId=" + familyPersonId +
                ", familyAccountId=" + familyAccountId +
                ", familyName='" + familyName + '\'' +
                ", familyAge=" + familyAge +
                ", familySex='" + familySex + '\'' +
                ", familyBirthTime=" + familyBirthTime +
                ", familyNumberId='" + familyNumberId + '\'' +
                ", familyHouseholderRelation='" + familyHouseholderRelation + '\'' +
                ", familyOrPartymember='" + familyOrPartymember + '\'' +
                ", familyTheparty=" + familyTheparty +
                ", familyCount=" + familyCount +
                ", familyGroupName='" + familyGroupName + '\'' +
                ", familyVillageName='" + familyVillageName + '\'' +
                '}';
    }
}
