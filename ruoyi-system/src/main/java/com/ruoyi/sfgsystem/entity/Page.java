package com.ruoyi.sfgsystem.entity;

public class Page {
    private Integer currentPage;  //当前页数
    private Integer showPageCount;  //每页显示的数据条数
    private String familyName;             //姓名
    private Integer subsidyPersonId;        //人编号

    public Integer getCurrentPage() {
        return currentPage;
    }

    public void setCurrentPage(Integer currentPage) {
        this.currentPage = currentPage;
    }

    public Integer getShowPageCount() {
        return showPageCount;
    }

    public void setShowPageCount(Integer showPageCount) {
        this.showPageCount = showPageCount;
    }

    public String getFamilyName() {
        return familyName;
    }

    public void setFamilyName(String familyName) {
        this.familyName = familyName;
    }

    public Integer getSubsidyPersonId() {
        return subsidyPersonId;
    }

    public void setSubsidyPersonId(Integer subsidyPersonId) {
        this.subsidyPersonId = subsidyPersonId;
    }
}
