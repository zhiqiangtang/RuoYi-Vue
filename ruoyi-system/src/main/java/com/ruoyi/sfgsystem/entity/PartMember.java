package com.ruoyi.sfgsystem.entity;

import com.ruoyi.common.annotation.Excel;

import java.util.Date;

public class PartMember {

    @Excel(name = "序号")
    private Integer partMemberId;

    @Excel(name = "人编号")
    private String partMemberPersonId;  //人编号

    @Excel(name = "入党时间")
    private Date partMemberTheparty;  //入党时间

    @Excel(name = "手机号")
    private String partMemberPhone;  //手机号

    @Excel(name = "民族")
    private String partMemberNationality;  //民族
    private Family family;
    private String familyName;
//    手机号   出生日期 民族 人编号
    /*根据人编号去查找家庭的基本信息*/

    public PartMember() {
    }

    public PartMember(Integer partMemberId, String partMemberPersonId,
                      Date partMemberTheparty, String partMemberPhone,
                      String partMemberNationality, Family family) {
        this.partMemberId = partMemberId;
        this.partMemberPersonId = partMemberPersonId;
        this.partMemberTheparty = partMemberTheparty;
        this.partMemberPhone = partMemberPhone;
        this.partMemberNationality = partMemberNationality;
        this.family = family;
    }

    public Integer getPartMemberId() {
        return partMemberId;
    }

    public void setPartMemberId(Integer partMemberId) {
        this.partMemberId = partMemberId;
    }

    public String getPartMemberPersonId() {
        return partMemberPersonId;
    }

    public void setPartMemberPersonId(String partMemberPersonId) {
        this.partMemberPersonId = partMemberPersonId;
    }

    public Date getPartMemberTheparty() {
        return partMemberTheparty;
    }

    public void setPartMemberTheparty(Date partMemberTheparty) {
        this.partMemberTheparty = partMemberTheparty;
    }

    public String getPartMemberPhone() {
        return partMemberPhone;
    }

    public void setPartMemberPhone(String partMemberPhone) {
        this.partMemberPhone = partMemberPhone;
    }

    public String getPartMemberNationality() {
        return partMemberNationality;
    }

    public void setPartMemberNationality(String partMemberNationality) {
        this.partMemberNationality = partMemberNationality;
    }

    public Family getFamily() {
        return family;
    }

    public void setFamily(Family family) {
        this.family = family;
    }

    public String getFamilyName() {
        return familyName;
    }

    public void setFamilyName(String familyName) {
        this.familyName = familyName;
    }
}
