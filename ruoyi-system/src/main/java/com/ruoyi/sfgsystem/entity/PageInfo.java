package com.ruoyi.sfgsystem.entity;

public class PageInfo {
    private Integer currentPage;  //当前页数
    private Integer showPageCount;  //每页显示的数据条数
    private Integer sumPage;      //总的页数
    private Integer msgCount;     //总的数据条数
    private String familyName;             //姓名
    private Integer subsidyPersonId;        //人编号

    public Integer getCurrentPage() {
        return currentPage;
    }

    public void setCurrentPage(Integer currentPage) {
        this.currentPage = currentPage;
    }

    public Integer getShowPageCount() {
        return showPageCount;
    }

    public void setShowPageCount(Integer showPageCount) {
        this.showPageCount = showPageCount;
    }

    public Integer getSumPage() {
        return sumPage;
    }

    public void setSumPage(Integer sumPage) {
        this.sumPage = sumPage;
    }

    public Integer getMsgCount() {
        return msgCount;
    }

    public void setMsgCount(Integer msgCount) {
        this.msgCount = msgCount;
    }

    public String getFamilyName() {
        return familyName;
    }

    public void setFamilyName(String familyName) {
        this.familyName = familyName;
    }

    public Integer getSubsidyPersonId() {
        return subsidyPersonId;
    }

    public void setFamilyPersonId(Integer familyPersonId) {
        this.subsidyPersonId = subsidyPersonId;
    }

    // 计算总的页数
    public  int getSumpage(int sumCount,int meiye){
        this.sumPage = sumCount/meiye;
        return sumPage;
    }

}
