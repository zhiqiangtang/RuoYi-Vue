package com.ruoyi.sfgsystem.entity;

import com.ruoyi.common.annotation.Excel;

import java.util.Date;

public class ExportPartMember {

    @Excel(name = "姓名")
    private String familyName;   //性名

    @Excel(name = "年龄")
    private Integer familyAge;   //年龄

    @Excel(name = "性别")
    private String familySex;  //性别

    @Excel(name = "人编号")
    private String partMemberPersonId;  //人编号

    @Excel(name = "户编号")
    private Integer familyAccountId; //户编号

    @Excel(name = "身份证")
    private String familyNumberId; //身份证

    @Excel(name = "民族")
    private String partMemberNationality;  //民族

    @Excel(name = "出生日期")
    private Date familyBirthTime;  //出生日期

    @Excel(name = "入党时间")
    private Date partMemberTheparty;  //入党时间

    @Excel(name = "手机号")
    private String partMemberPhone;  //手机号

    public String getFamilyName() {
        return familyName;
    }

    public void setFamilyName(String familyName) {
        this.familyName = familyName;
    }

    public Integer getFamilyAge() {
        return familyAge;
    }

    public void setFamilyAge(Integer familyAge) {
        this.familyAge = familyAge;
    }

    public String getFamilySex() {
        return familySex;
    }

    public void setFamilySex(String familySex) {
        this.familySex = familySex;
    }

    public String getPartMemberPersonId() {
        return partMemberPersonId;
    }

    public void setPartMemberPersonId(String partMemberPersonId) {
        this.partMemberPersonId = partMemberPersonId;
    }

    public Integer getFamilyAccountId() {
        return familyAccountId;
    }

    public void setFamilyAccountId(Integer familyAccountId) {
        this.familyAccountId = familyAccountId;
    }

    public String getFamilyNumberId() {
        return familyNumberId;
    }

    public void setFamilyNumberId(String familyNumberId) {
        this.familyNumberId = familyNumberId;
    }

    public String getPartMemberNationality() {
        return partMemberNationality;
    }

    public void setPartMemberNationality(String partMemberNationality) {
        this.partMemberNationality = partMemberNationality;
    }

    public Date getFamilyBirthTime() {
        return familyBirthTime;
    }

    public void setFamilyBirthTime(Date familyBirthTime) {
        this.familyBirthTime = familyBirthTime;
    }

    public Date getPartMemberTheparty() {
        return partMemberTheparty;
    }

    public void setPartMemberTheparty(Date partMemberTheparty) {
        this.partMemberTheparty = partMemberTheparty;
    }

    public String getPartMemberPhone() {
        return partMemberPhone;
    }

    public void setPartMemberPhone(String partMemberPhone) {
        this.partMemberPhone = partMemberPhone;
    }
}
