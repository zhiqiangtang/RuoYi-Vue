package com.ruoyi.sfgsystem.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.common.annotation.Excel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

/**
 * 数据的导出
 */

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ExportSubsidy {


    @Excel(name = "序号")
    private Integer subsidyId;//主键
    @Excel(name = "姓名")
    private String familyName;   //性名
    @Excel(name ="人编号")
    private Integer subsidyPersonId;                    //人编号
    @Excel(name = "残疾补贴")
    private Integer subsidyDisabilityAllowance;         //残疾补贴
    @Excel(name = "高龄补贴")
    private Integer subsidyOldAgeAllowance;            //高龄补助
    @Excel(name = "孤儿救助")
    private Integer subsidyOrphanAllowance;            //孤儿救助
    @Excel(name = "临时救助")
    private Integer subsidyTemporaryRescue;            //临时救助
    @Excel(name = "低保金")
    private Integer subsidySubsistenceAllowance;       //低保金
    @Excel(name = "特困供养金")
    private Integer subsidyEmergencyAllowance;         //特困供养金
    @Excel(name = "退役军人补助")
    private Integer subsidyRetiredServicemen;          //退役军人补助
    @Excel(name = "户编号")
    private Integer subsidyAccountId;                  //户编号
    @Excel(name = "低保补助人数")
    private Integer subsidyNumber;                       //人数
    @Excel(name = "问题是否解决")
    private String subsidySolve;                        //问题是否解决
    @Excel(name = "困难等级")
    private String subsidyTypeOrGrade;//类型和等级

    @Excel(name = "军人退役时间")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date subsidyTime;//退役时间



}
