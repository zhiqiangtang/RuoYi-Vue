package com.ruoyi.sfgsystem.entity;

import com.ruoyi.common.annotation.Excel;

import java.util.Date;

//就业信息
public class Employment {
    @Excel(name = "序号")
    private Integer employmentId;

    @Excel(name = "人编号")
    private Integer employmentPersonId;      //人编号

    @Excel(name = "工作城市")
    private String employmentOrWorking;     //工作城市

    @Excel(name = "援助岗位")
    private String employmenteAidpost;      //援助岗位

    @Excel(name = "护林员")
    private String employmenteNurse;      //护林员

    @Excel(name = "清洁员")
    private String employmenteCleaner;      //清洁员

    @Excel(name ="管水员")
    private String employmentePlumber;      //管水员

    @Excel(name = "村干部")
    private String employmenteVillageCadre;  //村干部

    @Excel(name = "薪资")
    private String employmentSalary;           //薪资

    @Excel(name = "工作单位")
    private String employmentWorkUnit;        //工作单位

    @Excel(name = "工作时间")
    private Date employmentWorkTime;        //工作时间

    @Excel(name = "联系电话")
    private String employmentPhone;

    private String familyName;

    private Family family;                      //家庭基本信息

    public Employment() {
    }

    public Employment(Integer employmentId, Integer employmentPersonId,
                      String employmentOrWorking, String employmenteAidpost,
                      String employmenteNurse, String employmenteCleaner,
                      String employmentePlumber, String employmenteVillageCadre,
                      String employmentSalary, String employmentWorkUnit,
                      Date employmentWorkTime, String employmentPhone,
                      Family family) {
        this.employmentId = employmentId;
        this.employmentPersonId = employmentPersonId;
        this.employmentOrWorking = employmentOrWorking;
        this.employmenteAidpost = employmenteAidpost;
        this.employmenteNurse = employmenteNurse;
        this.employmenteCleaner = employmenteCleaner;
        this.employmentePlumber = employmentePlumber;
        this.employmenteVillageCadre = employmenteVillageCadre;
        this.employmentSalary = employmentSalary;
        this.employmentWorkUnit = employmentWorkUnit;
        this.employmentWorkTime = employmentWorkTime;
        this.employmentPhone = employmentPhone;
        this.family = family;
    }

    public Integer getEmploymentId() {
        return employmentId;
    }

    public void setEmploymentId(Integer employmentId) {
        this.employmentId = employmentId;
    }

    public Integer getEmploymentPersonId() {
        return employmentPersonId;
    }

    public void setEmploymentPersonId(Integer employmentPersonId) {
        this.employmentPersonId = employmentPersonId;
    }

    public String getEmploymentOrWorking() {
        return employmentOrWorking;
    }

    public void setEmploymentOrWorking(String employmentOrEorking) {
        this.employmentOrWorking = employmentOrEorking;
    }

    public String getEmploymenteAidpost() {
        return employmenteAidpost;
    }

    public void setEmploymenteAidpost(String employmenteAidpost) {
        this.employmenteAidpost = employmenteAidpost;
    }

    public String getEmploymenteNurse() {
        return employmenteNurse;
    }

    public void setEmploymenteNurse(String employmenteNurse) {
        this.employmenteNurse = employmenteNurse;
    }

    public String getEmploymenteCleaner() {
        return employmenteCleaner;
    }

    public void setEmploymenteCleaner(String employmenteCleaner) {
        this.employmenteCleaner = employmenteCleaner;
    }

    public String getEmploymentePlumber() {
        return employmentePlumber;
    }

    public void setEmploymentePlumber(String employmentePlumber) {
        this.employmentePlumber = employmentePlumber;
    }

    public String getEmploymenteVillageCadre() {
        return employmenteVillageCadre;
    }

    public void setEmploymenteVillageCadre(String employmenteVillageCadre) {
        this.employmenteVillageCadre = employmenteVillageCadre;
    }

    public String getEmploymentSalary() {
        return employmentSalary;
    }

    public void setEmploymentSalary(String employmentSalary) {
        this.employmentSalary = employmentSalary;
    }

    public String getEmploymentWorkUnit() {
        return employmentWorkUnit;
    }

    public void setEmploymentWorkUnit(String employmentWorkUnit) {
        this.employmentWorkUnit = employmentWorkUnit;
    }

    public Date getEmploymentWorkTime() {
        return employmentWorkTime;
    }

    public void setEmploymentWorkTime(Date employmentWorkTime) {
        this.employmentWorkTime = employmentWorkTime;
    }

    public Family getFamily() {
        return family;
    }

    public void setFamily(Family family) {
        this.family = family;
    }

    public String getEmploymentPhone() {
        return employmentPhone;
    }

    public void setEmploymentPhone(String employmentPhone) {
        this.employmentPhone = employmentPhone;
    }

    public String getFamilyName() {
        return familyName;
    }

    public void setFamilyName(String familyName) {
        this.familyName = familyName;
    }
}
