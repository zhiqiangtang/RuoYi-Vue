package com.ruoyi.sfgsystem.mapper;

import com.ruoyi.sfgsystem.entity.PageInfo;
import com.ruoyi.sfgsystem.entity.PartMember;

import java.util.List;

public interface PartMemberMapper {
    /**
     * 计算总的数据条数
     * @return 返回一个int类型的数据条数
     */
    Integer dataTotal();

    /**
     * 分页查询
     *  掺入当前页数，和每页的条数
     * @return 返回一个 PartMember类型deList集合
     */
    List<PartMember> selectPartMemberPage(PartMember partMember);

    /**
     * 新增数据
     * @param partMembers 参数是 PartMember
     * @return 返回int 类型
     */
    int insertPartMemberData(PartMember partMembers);

    /**修改数据*/
    int updataPartMemberData(PartMember partMembers);

    /**批量删除或单个删除*/
    Integer deleteBeachIdsOrRemoveId(List<Integer> ids);

    /**人编号的验证*/
    List<PartMember> verifierNumber();


}
