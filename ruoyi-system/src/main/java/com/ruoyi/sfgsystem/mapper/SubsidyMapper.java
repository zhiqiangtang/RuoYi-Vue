package com.ruoyi.sfgsystem.mapper;

import com.ruoyi.sfgsystem.entity.Family;
import com.ruoyi.sfgsystem.entity.PageInfo;
import com.ruoyi.sfgsystem.entity.Subsidy;

import java.util.List;

public interface SubsidyMapper {
    /*数据的记录数*/
    Integer selectTotal();

    /*分页查询*/
    List<Subsidy> selectSubsidyPage(Subsidy subsidy);

    /*新增数据*/
    Integer insertData(Subsidy subsidy);
    /*修改数据*/
    Integer updateData(Subsidy subsidy);

    /*批量删除或单个删除*/
    Integer deleteBeachId(List<Integer> ids);

    /*人编号的验证*/
    List<Subsidy> selectVerifierNumber();
}
