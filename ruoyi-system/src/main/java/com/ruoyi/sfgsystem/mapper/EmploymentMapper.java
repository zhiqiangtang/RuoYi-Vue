package com.ruoyi.sfgsystem.mapper;

import com.ruoyi.sfgsystem.entity.Employment;
import com.ruoyi.sfgsystem.entity.PageInfo;

import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

public interface EmploymentMapper {
    /**数据的记录*/
    public Integer dataTotal();

    /**分页查询*/
    List<Employment> selectEmploymentPageData(Employment employment);
    /**新增数据*/
    Integer insertData(Employment employment);

/**修改数据*/
    Integer updateData(Employment employment);

    CopyOnWriteArrayList<Employment> verifierNumber();

    Integer deleteBeachIds(List<Integer> ids);
}
