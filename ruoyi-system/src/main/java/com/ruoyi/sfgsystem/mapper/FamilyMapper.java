package com.ruoyi.sfgsystem.mapper;

import com.ruoyi.sfgsystem.entity.Family;
import com.ruoyi.sfgsystem.entity.PageInfo;

import java.util.List;

public interface FamilyMapper {
//    数据的条数
    Integer dataTotal();

//    分页查询
    List<Family> selectFailyData(Family family);

//    数据的插入
    int insertFamilyData(Family family);

    int updataFaamilyData(Family family);

    Integer deleteBeachIds(List<Integer> ids);

    List<Family> verifierNumber();

}




