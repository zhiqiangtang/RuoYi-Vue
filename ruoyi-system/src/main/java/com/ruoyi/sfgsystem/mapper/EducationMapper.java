package com.ruoyi.sfgsystem.mapper;


import com.ruoyi.sfgsystem.entity.Education;
import com.ruoyi.sfgsystem.entity.PageInfo;

import java.util.List;

public interface EducationMapper {

    Integer  statistics(String familyName,String educationSchoolName);

    /**
     *
     * 分页查询
     */
    List<Education> selectData(Education education);

    /**
     * 数据的总条数
     * @return
     */
    int dataTotals();

    /*新增数据*/
    void insertData(Education education);

    void updateData(Education education);

    /*批量删除或单个删除*/
    void deleteBeachDate(List<Integer> ids);
}
