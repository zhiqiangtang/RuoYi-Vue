package com.ruoyi.sfgsystem.service;


import com.ruoyi.sfgsystem.entity.Family;
import org.springframework.web.bind.annotation.RequestParam;


import java.util.List;
public interface FamilyService {

    Integer dataTotal();

//    分页的数据查询
    List<Family> selectFamilyData(Family family);

//   新增数据
    Integer insertFamilyData(Family family);

    Integer deleteBeachIds(List<Integer> ids);

//    人编号验证
    boolean verifierNumber(Integer value);
}
