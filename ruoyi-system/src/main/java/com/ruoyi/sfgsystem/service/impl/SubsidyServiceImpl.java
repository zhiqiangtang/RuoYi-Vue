package com.ruoyi.sfgsystem.service.impl;

import com.ruoyi.common.annotation.DataScope;
import com.ruoyi.sfgsystem.entity.ExportSubsidy;
import com.ruoyi.sfgsystem.entity.Family;
import com.ruoyi.sfgsystem.entity.PageInfo;
import com.ruoyi.sfgsystem.entity.Subsidy;
import com.ruoyi.sfgsystem.mapper.SubsidyMapper;
import com.ruoyi.sfgsystem.service.SubsidyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;



import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.atomic.AtomicBoolean;

@Service
public class SubsidyServiceImpl implements SubsidyService {
    @Autowired
    private SubsidyMapper subsidyMapper;



    /**
     * 数据的记录数
     * @return 返回的结果
     */
    @Override
    public Integer selectTotal() {
        return subsidyMapper.selectTotal();
    }

    /**
     * 分页查询
     *  当前页数
     *  每页的数据
     * @return 返回分页查询的结果
     */
    @Override
    @DataScope(subsidyAlias="ss" ,familyAlias ="sfm" )
    public List<Subsidy> selectSubsidyPage(Subsidy subsidy) {
        /*PageInfo pageInfo = new PageInfo();
        currentPage = (currentPage - 1) * showPageCount;   // 当前页数
        pageInfo.setCurrentPage(currentPage);
        pageInfo.setShowPageCount(showPageCount);*/
        return subsidyMapper.selectSubsidyPage(subsidy);
    }

    /**
     * 新增或修改数据
     * @param subsidy 要新增的数据或修改的数据
     * @return 返回成功的结果
     */
    @Override
    public Integer insertOrUpdateData(Subsidy subsidy) {

        if (subsidy.getSubsidyId() == null){
            return subsidyMapper.insertData(subsidy);   //新增数据
        }else {
            //修改数据
            return subsidyMapper.updateData(subsidy);
        }
    }

    /**
     * 批量删除或单个删除
     * @param ids 要删除的数据
     * @return 返回的结果
     */
    @Override
    public Integer deleteBeachIds(List<Integer> ids) {
        return subsidyMapper.deleteBeachId(ids);
    }

   /* @Override
    public List<ExportSubsidy> selectExport(Integer currentPage, Integer showPageCount) {

        currentPage = (currentPage -1) * showPageCount;
        PageInfo pageInfo = new PageInfo();
        pageInfo.setCurrentPage(currentPage);
        pageInfo.setShowPageCount(showPageCount);

        List<Subsidy> list = subsidyMapper.selectSubsidyPage(pageInfo);

        List<ExportSubsidy> list1 = Collections.synchronizedList(new ArrayList<>());


        for (int i = 0; i < list.size(); i++) {
            ExportSubsidy es = new ExportSubsidy();
            es.setSubsidySolve(list.get(i).getSubsidySolve());
            es.setSubsidyDisabilityAllowance(list.get(i).getSubsidyDisabilityAllowance());
//            es.getSubsidyDisabilityAllowance(list.get(i).getSubsidyDisabilityAllowance());
            es.setSubsidyId(list.get(i).getSubsidyId());

            es.setSubsidyNumber(list.get(i).getSubsidyNumber());

            es.setSubsidyOldAgeAllowance(list.get(i).getSubsidyOldAgeAllowance());

            es.setFamilyName(list.get(i).getFamily().getFamilyName());

            es.setSubsidyAccountId(list.get(i).getSubsidyAccountId());

            es.setSubsidyEmergencyAllowance(list.get(i).getSubsidyEmergencyAllowance());

            es.setSubsidyRetiredServicemen(list.get(i).getSubsidyRetiredServicemen());

            es.setSubsidyTypeOrGrade(list.get(i).getSubsidyTypeOrGrade());

            es.setSubsidyOrphanAllowance(list.get(i).getSubsidyOrphanAllowance());

            es.setSubsidyTemporaryRescue(list.get(i).getSubsidyTemporaryRescue());

            es.setSubsidyPersonId(list.get(i).getSubsidyPersonId());

            es.setSubsidySubsistenceAllowance(list.get(i).getSubsidySubsistenceAllowance());

            es.setSubsidyTime(list.get(i).getSubsidyTime());
            System.out.println("姓名----》"+list.get(i).getFamily().getFamilyName());
            list1.add(es);
        }

        return list1;
    }*/

    /**
     * 人编号的验证是否重复
     * @param value 要验证的数据
     * @return 返回结果 重复返回 true 不重复返回false
     */
    @Override
    public boolean verifierNumber(String value) {
        boolean flag= false;   //false表示不相等 true向相等
        List<Subsidy> list = subsidyMapper.selectVerifierNumber();
        List<Integer> copy = Collections.synchronizedList(new ArrayList<>());
        for (int i = 0; i < list.size(); i++) {
            if (list.get(i).getSubsidyPersonId().toString().equals(value)||list.get(i).getSubsidyPersonId().toString()==""){
                flag = true;
            }
        }
        return flag;
    }
}
