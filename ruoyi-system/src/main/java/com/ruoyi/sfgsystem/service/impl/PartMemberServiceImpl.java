package com.ruoyi.sfgsystem.service.impl;

import com.ruoyi.sfgsystem.entity.ExportPartMember;
import com.ruoyi.sfgsystem.entity.PageInfo;
import com.ruoyi.sfgsystem.entity.PartMember;
import com.ruoyi.sfgsystem.mapper.PartMemberMapper;
import com.ruoyi.sfgsystem.service.PartMemberService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Service
public class PartMemberServiceImpl implements PartMemberService {

    @Autowired(required = false)
    private PartMemberMapper partMemberMapper;
    /**
     * 记录总的数据条数
     * @return 返回 int类型的数
     */
    @Override
    public Integer dataTotal() {
        Integer partMemberTotal= partMemberMapper.dataTotal();
        return partMemberTotal;
    }

    /**
     * 分页查询
     * 所在当前页
     * t 每页的数据总数
     * @return 返回一页PartMember类型的List集合
     */
    @Override
    public List<PartMember> selectPartMemberPage(PartMember partMember) {

        List<PartMember> partMembersData = partMemberMapper.selectPartMemberPage(partMember);
        return partMembersData;
    }

    /**
     * 新增数据或修改数据
     * @param partMembers
     * @return
     */
    @Override
    public Integer insertOrUpdata(PartMember partMembers) {

            Integer dataId= null;
        if (partMembers.getPartMemberId() == null){
            dataId=partMemberMapper.insertPartMemberData(partMembers);
            //新增数据
            return dataId;

        }
        else {
            //修改数据
            dataId =  partMemberMapper.updataPartMemberData(partMembers);
            return dataId;
        }
    }

    /**
     * 批量删除或单个删除
     * @param ids 要批量的数据或单个的数据
     * @return 返回结果
     */
    @Override
    public Integer deleteBeachIdsOrRemoveId(List<Integer> ids) {
        Integer removeId= partMemberMapper.deleteBeachIdsOrRemoveId(ids);
        return removeId;
    }

    /**
     * 人编号的验证
     * @return 返回结果
     */
    @Override
    public boolean verifierNumber(Integer value) {
        boolean flag=false;  //表示人编号不重复
        List<PartMember> listData = partMemberMapper.verifierNumber();
        for (int i = 0; i < listData.size(); i++) {
            if (value.toString().equals(listData.get(i).getPartMemberPersonId())){
                flag = true;  //表示人编号重复
                return flag;
            }
        }
        return flag;
    }

    /**
     * 党员基本信息的导出
     * @param currentPage 导出当前页的数据
     * @param showPageCount 导出没有导出多少条数据
     * @return 返回的结果
     */
/*
    @Override
    public List<ExportPartMember> selectExport(Integer currentPage, Integer showPageCount) {
        currentPage = (currentPage - 1)* showPageCount;
        PageInfo pageInfo = new PageInfo();
        pageInfo.setCurrentPage(currentPage);
        pageInfo.setShowPageCount(showPageCount);


        List<PartMember> partMembers = partMemberMapper.selectPartMemberPage(pageInfo);
        List<ExportPartMember> list = Collections.synchronizedList(new ArrayList<>());
        for (int i = 0; i < partMembers.size(); i++) {
            ExportPartMember exportPartMember = new ExportPartMember();

            exportPartMember.setPartMemberPersonId(partMembers.get(i).getPartMemberPersonId());

            exportPartMember.setPartMemberPhone(partMembers.get(i).getPartMemberPhone());

            exportPartMember.setPartMemberTheparty(partMembers.get(i).getPartMemberTheparty());

            exportPartMember.setPartMemberNationality(partMembers.get(i).getPartMemberNationality());
            exportPartMember.setFamilyName(partMembers.get(i).getFamily().getFamilyName());

            exportPartMember.setFamilyAge(partMembers.get(i).getFamily().getFamilyAge());

            exportPartMember.setFamilySex(partMembers.get(i).getFamily().getFamilySex());

            exportPartMember.setFamilyNumberId(partMembers.get(i).getFamily().getFamilyNumberId());

            exportPartMember.setFamilyBirthTime(partMembers.get(i).getFamily().getFamilyBirthTime());

            exportPartMember.setFamilyAccountId(partMembers.get(i).getFamily().getFamilyAccountId());
            list.add(exportPartMember);
        }

        return list;
    }*/

}
