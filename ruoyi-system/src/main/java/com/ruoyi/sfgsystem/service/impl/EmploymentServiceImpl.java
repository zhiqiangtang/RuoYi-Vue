package com.ruoyi.sfgsystem.service.impl;

import com.ruoyi.sfgsystem.entity.Employment;
import com.ruoyi.sfgsystem.entity.PageInfo;
import com.ruoyi.sfgsystem.mapper.EmploymentMapper;
import com.ruoyi.sfgsystem.service.EmploymentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;
@Service
public class EmploymentServiceImpl implements EmploymentService {

    @Autowired(required = false)
    private EmploymentMapper employmentMapper;

    /**
     * 数据的记录
     * @return 返回的结果
     */
    @Override
    public Integer dataTotal() {

        return employmentMapper.dataTotal();
    }

    /**
     * 分页查询
     * 前页数
     *   每页的数据
     * @return 返回的记过
     */
    @Override
    public List<Employment> selectEmploymentPageData(Employment employment) {
        return employmentMapper.selectEmploymentPageData(employment);
    }

    /**
     * 新增或修改数据
     * @param employment 参数
     * @return 返回的结果 1 表示成功 0 表示失败
     */
    @Override
    public Integer insertOrUpdateData(Employment employment) {
        //新增数据
        if (employment.getEmploymentId() == null){
            return employmentMapper.insertData(employment);
        }else {
//            修改数据
            return employmentMapper.updateData(employment);
        }
    }

    /**
     * 人编号的验证
     * @return 返回结果
     */
    @Override
    public boolean verifierNumber(String employmentPersonId) {
        boolean flag= false;   //false表示不相等 true向相等
        CopyOnWriteArrayList<Integer> list = new CopyOnWriteArrayList<>(new ArrayList<>());
        List<Employment> ver = employmentMapper.verifierNumber();
        for (int i = 0; i < ver.size(); i++) {
            list.add(ver.get(i).getEmploymentPersonId());
        }
        for (Integer s : list) {
            if (s.toString().equals(employmentPersonId)){
                flag = true;
                 //表示重复
            }
        }
        return flag;
    }

    /**
     * 批量删除或单个删除
     * @param ids 要删除的数据
     * @return 返回成功的结果
     */
    @Override
    public Integer deleteBeachIds(List<Integer> ids) {
        return employmentMapper.deleteBeachIds(ids);
    }
}
