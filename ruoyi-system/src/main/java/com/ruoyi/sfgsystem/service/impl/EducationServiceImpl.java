package com.ruoyi.sfgsystem.service.impl;

import com.ruoyi.sfgsystem.entity.Education;
import com.ruoyi.sfgsystem.entity.PageInfo;
import com.ruoyi.sfgsystem.mapper.EducationMapper;
import com.ruoyi.sfgsystem.service.EducationService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class EducationServiceImpl implements EducationService {

    @Resource
    private EducationMapper educationMapper;

    @Override
    public List<Education> selectData(Education education) {

        return educationMapper.selectData(education);
    }

    /**
     * 新增或修改数据
     * @param education 要新增或修改的数据
     */
    @Override
    public void insertOrUpdateData(Education education) {
        if (education.getEducationId() == null){
            /*新增数据*/
            educationMapper.insertData(education);
        }else {
            /*修改数据*/
            educationMapper.updateData(education);
        }
    }

    @Override
    public void deleteBeachDate(List<Integer> ids) {
       educationMapper.deleteBeachDate(ids);
    }

}
