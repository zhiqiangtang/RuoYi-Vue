package com.ruoyi.sfgsystem.service;

import com.ruoyi.sfgsystem.entity.Employment;

import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

/***就业信息*/
public interface EmploymentService {
    /**
     * 数据记录
     * @return 返回
     */
    Integer dataTotal();

    /**
     * 分页查询
     * @param currentPage 当前页数
     * @param showPageCount  每页的数据
     * @return 返回的结果
     */
    List<Employment> selectEmploymentPageData(Employment employment);

    /**
     * 新增或修改数据
     * @param employment 参数
     * @return 返回的结果
     */
    Integer insertOrUpdateData(Employment employment);

    /**
     * 人编号验证
     * @return 返回结果为数组
     */
    boolean verifierNumber(String employmentPersonId);

    /**
     * 批量删除或单个删除
     * @param ids 要删除的数据
     * @return 返回成功的结果
     */
    Integer deleteBeachIds(List<Integer> ids);
}
