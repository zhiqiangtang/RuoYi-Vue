package com.ruoyi.sfgsystem.service;

import com.ruoyi.sfgsystem.entity.ExportPartMember;
import com.ruoyi.sfgsystem.entity.PartMember;

import java.util.List;

public interface PartMemberService {
//    数据的记录
    Integer dataTotal();

//    分页查询
    List<PartMember> selectPartMemberPage(PartMember partMember);

    /**
     * 新增数据或修改数据
     * @param partMember
     * @return
     */
    Integer insertOrUpdata(PartMember partMember);

    /**
     * 批量删除或单个删除
     * @param ids 批量的数据或单个的数据
     * @return 如果成功返回一个int 类型的数据
     */
    Integer deleteBeachIdsOrRemoveId(List<Integer> ids);

    /**
     * 人编号的验证
     * @return 返回的结果
     */
    boolean verifierNumber(Integer value);

    /**
     * 党员基本信息的导出
     * @param currentPage 导出当前页的数据
     * @param showPageCount 导出没有导出多少条数据
     * @return 返回结果
     */
/*
    List<ExportPartMember> selectExport(Integer currentPage, Integer showPageCount);
*/
}
