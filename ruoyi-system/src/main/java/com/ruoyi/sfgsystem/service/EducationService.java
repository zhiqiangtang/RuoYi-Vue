package com.ruoyi.sfgsystem.service;

import com.ruoyi.sfgsystem.entity.Education;
import com.ruoyi.sfgsystem.entity.PageInfo;

import java.util.List;

/**
 * 这是教育信息
 */
public interface EducationService {

    List<Education> selectData(Education education);

    /**
     * 新增数据或修改数据
     * @param education 要新增或修改的数据
     */
    void insertOrUpdateData(Education education);

    void deleteBeachDate(List<Integer> ids);
}

