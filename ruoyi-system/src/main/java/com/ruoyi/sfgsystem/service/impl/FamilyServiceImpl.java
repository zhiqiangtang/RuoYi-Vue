package com.ruoyi.sfgsystem.service.impl;

import com.ruoyi.common.annotation.DataScope;
import com.ruoyi.sfgsystem.entity.Family;
import com.ruoyi.sfgsystem.entity.PageInfo;
import com.ruoyi.sfgsystem.mapper.FamilyMapper;
import com.ruoyi.sfgsystem.service.FamilyService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
@Service
public class FamilyServiceImpl implements FamilyService {

    @Resource
    private FamilyMapper familyMapper;

    /**
     * 数据的总数
     * @return
     */
    @Override
    public Integer dataTotal() {
        return familyMapper.dataTotal();
    }

    @Override
    @DataScope(userAlias = "n")
    public List<Family> selectFamilyData(Family family) {
        /*PageInfo pageInfo = new PageInfo();
        int page = (currentPage - 1) * showPageCount;
        pageInfo.setCurrentPage(page);
        pageInfo.setShowPageCount(showPageCount);*/
        return familyMapper.selectFailyData(family);
    }

    /**
     * 新增数据
     * @param family
     * @return
     */
    @Override
    public Integer insertFamilyData(Family family) {
        Integer  familyData=null;
       if (family.getFamilyId() == null){
//           当familyId没有值是声明没有数据存在，需要添加数据
           familyData= familyMapper.insertFamilyData(family);
       }
       //相反有familyId说明有数据，进行数据更新
       else {
           familyData=familyMapper.updataFaamilyData(family);
       }
        return familyData;
    }

    @Override
    public Integer deleteBeachIds(List<Integer> ids) {
        Integer familyIds=familyMapper.deleteBeachIds(ids);
        return familyIds;
    }

    @Override
    public boolean verifierNumber(Integer value) {
        boolean flag = false;
        List<Family> nom=familyMapper.verifierNumber();
        for (int i = 0; i < nom.size(); i++) {
            if (value.equals(nom.get(i).getFamilyPersonId())){
                flag = true;
                return flag;  //表示人编号重复true false 表示不重复
            }
        }
            return flag;

    }


}
