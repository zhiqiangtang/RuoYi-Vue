package com.ruoyi.sfgsystem.service;

import com.ruoyi.sfgsystem.entity.ExportSubsidy;
import com.ruoyi.sfgsystem.entity.Subsidy;

import java.util.List;

public interface SubsidyService {
    /**数据的记录*/
    Integer selectTotal();

    /**
     * 分页查询
     *  当前页数
     *  每页的数据
     * @return 返回的结果
     */
    List<Subsidy> selectSubsidyPage(Subsidy subsidy);

    /**
     * 新增数据或修改数据
     * @param subsidy 要新增的数据或修改的数据
     * @return 返回成功的结果
     */
    Integer insertOrUpdateData(Subsidy subsidy);

    /**
     * 批量删除或单个删除
     * @param ids 要删除的数据
     * @return 返回删除成功的结果 1 表是成功 0 表示失败
     */
    Integer deleteBeachIds(List<Integer> ids);

  /*  List<ExportSubsidy> selectExport(Integer currentPage, Integer showPageCount);
  */  /**
     * 验证人编号
     * @param value 要验证的数据
     * @return 返回boolean 值
     */
    boolean verifierNumber(String value);
}
