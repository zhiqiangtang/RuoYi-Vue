package com.ruoyi.uam.domain;

import lombok.Data;

/**
 * @description: 人事部门
 * @author: 朋朋
 * @date: 2023/12/8/15:44
 */
@Data
public class ViewDept {

   private String 部门id;

   private String 部门编号;

   private String 上级部门id;

   private String 部门名称;

   private Integer 部门性质id;

   private String 部门性质名称;

   private Integer 是否虚拟部门;

   private String 排序;

   private String 状态;

   private String 建立年月;

   private String 失效日期;

   private String 部门简称;
}
