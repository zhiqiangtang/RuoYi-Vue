package com.ruoyi.uam.domain;

import lombok.Data;

/**
 * @description: 描述
 * @author: 朋朋
 * @date: 2023/12/8/16:26
 */
@Data
public class ViewTeacher {

    private String id;

    private String 教工号;

    private String 姓名;

    private String 性别;

    private String 出生日期;

    private String 身份证号;

    private String 政治面貌码;

    private String 教职工类别码;

    private String  部门ID;
}
