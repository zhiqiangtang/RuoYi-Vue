package com.ruoyi.uam.mapper;

import com.ruoyi.uam.domain.ViewDept;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @description: 描述
 * @author: 朋朋
 * @date: 2023/12/8/15:53
 */
@Component
public interface ViewDeptMapper {

    public List<ViewDept> list();
}
