package com.ruoyi.uam.mapper;

import com.ruoyi.uam.domain.ViewTeacher;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @description: 描述
 * @author: 朋朋
 * @date: 2023/12/8/16:35
 */
@Component
public interface ViewTeacherMapper {

    /**
     * 查询教职工信息
     */
    ViewTeacher  selectTeacherInfo(String jobNumber);
}
