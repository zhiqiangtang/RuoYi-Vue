package com.ruoyi.uam.service.impl;

import com.ruoyi.common.annotation.DataSource;
import com.ruoyi.common.enums.DataSourceType;
import com.ruoyi.uam.domain.ViewDept;
import com.ruoyi.uam.mapper.ViewDeptMapper;
import com.ruoyi.uam.service.ViewDeptService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @description: 描述
 * @author: 朋朋
 * @date: 2023/12/8/16:02
 */
@Service
@DataSource(value = DataSourceType.SLAVE)  // 指定从slave数据源查询数据
public class ViewDeptServiceImpl implements ViewDeptService {

    @Autowired
    private ViewDeptMapper viewDeptMapper;

    @Override
    public List<ViewDept> list() {
        return viewDeptMapper.list();
    }
}
