package com.ruoyi.uam.service;

import com.ruoyi.uam.domain.ViewTeacher;

import java.util.List;

/**
 * @description: 描述
 * @author: 朋朋
 * @date: 2023/12/8/16:40
 */
public interface ViewTeacherService {

    /**
     * 查询教职工信息
     */
    ViewTeacher selectTeacherInfo(String jobNumber);
}
