package com.ruoyi.uam.service.impl;

import com.ruoyi.common.annotation.DataSource;
import com.ruoyi.common.enums.DataSourceType;
import com.ruoyi.uam.domain.ViewTeacher;
import com.ruoyi.uam.mapper.ViewTeacherMapper;
import com.ruoyi.uam.service.ViewTeacherService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @description: 描述
 * @author: 朋朋
 * @date: 2023/12/8/16:41
 */
@Service
@DataSource(value = DataSourceType.SLAVE)  // 指定从slave数据源查询数据
public class ViewTeacherServiceImpl implements ViewTeacherService {

    @Autowired
    private ViewTeacherMapper viewTeacherMapper;

    @Override
    public ViewTeacher selectTeacherInfo(String jobNumber) {
        return viewTeacherMapper.selectTeacherInfo(jobNumber);
    }
}
