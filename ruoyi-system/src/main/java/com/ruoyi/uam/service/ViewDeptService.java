package com.ruoyi.uam.service;

import com.ruoyi.uam.domain.ViewDept;

import java.util.List;

/**
 * @description: 描述
 * @author: 朋朋
 * @date: 2023/12/8/16:02
 */

public interface ViewDeptService {

    public List<ViewDept> list();
}
